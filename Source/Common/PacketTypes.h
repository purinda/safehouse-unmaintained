/* 
 * File:   PacketTypes.h
 * Author: purinda
 *
 * Created on 3 July 2012, 5:12 PM
 */

#ifndef PACKETTYPES_H
#define	PACKETTYPES_H

#ifdef	__cplusplus
extern "C"
{
#endif
	
// DEVICE TYPES
const uint8_t DT_RESERVED = 0;
const uint8_t DT_BULB = 1;
const uint8_t DT_BULB_DIMMABLE = 2;
const uint8_t DT_BULB_PIR = 3;
const uint8_t DT_APP_CTRL_DHT = 4;
const uint8_t DT_DHT = 5;
const uint8_t DT_PIR = 6;
const uint8_t DT_TEST = 100;

// PACKET TYPES
const uint8_t PT_RESERVED = 0;
const uint8_t PT_DEVICE_CHECKING_PACKET = 1;
const uint8_t PT_DEVICE_DATA_PACKET = 2;
const uint8_t PT_COMMAND_PACKET = 3;
const uint8_t PT_COMMAND_STATUS_PACKET = 4;

// COMMAND TYPES
const uint8_t CT_RESERVED = 0;
const uint8_t CT_SWITCH = 1;
const uint8_t CT_VARIABLE_SWITCH = 2;
const uint8_t CT_TEMPERATURE = 10;
const uint8_t CT_HUMIDITY = 11;
const uint8_t CT_CURRENT = 12;
const uint8_t CT_PRESENCE = 13;


// Dummy Packet (used for packet identification)
struct structDummy
{
	uint8_t packetType;
} __attribute__((packed));

/**********************************************/
// Device Checking Packet
struct structDCP
{
	uint8_t packetType;
	uint16_t id;		//2 bytes
} __attribute__((packed));

// Device Data Packet
struct structDDP
{
	uint8_t packetType;			//1 bytes
	uint16_t id;				//2 bytes
	uint8_t deviceType;			//1 bytes
	uint64_t address;			//8 bytes
} __attribute__((packed));

// Command Packet
struct structCP
{
	uint8_t packetType;
	uint16_t id;				//2 bytes
	uint8_t commandType;
	float value;
} __attribute__((packed));

// Command Status Packet
struct structCSP
{
	uint8_t packetType;			//1 bytes
	uint16_t id;				//2 bytes
	uint8_t commandType;		//1 bytes
	float value;				//4 bytes
	uint64_t address;			//8 bytes
} __attribute__((packed));


typedef struct structDummy PacketDummy;
typedef struct structDCP PacketDCP;
typedef struct structDDP PacketDDP;
typedef struct structCP PacketCP;
typedef struct structCSP PacketCSP;


#ifdef	__cplusplus
}
#endif

#endif	/* PACKETTYPES_H */

