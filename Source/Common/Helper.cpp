/*
 * File:   Helper.cpp
 * Author: purinda
 *
 * Created on 13 April 2012, 10:28 PM
 */

#include "Helper.hpp"

Helper::Helper()
{
}

Helper::~Helper()
{
}

void Helper::charToUint8_t(char* chars, uint8_t* uinits, unsigned short count)
{
	for(unsigned int i = 0; i < count; i++)
	{
		uinits[i] = (uint8_t)chars[i];
	}
}

void Helper::Uint8_tToChar(uint8_t* uinits, char* chars, unsigned short count)
{
	for(unsigned int i = 0; i < count; i++)
	{
		chars[i] = (char)uinits[i];
	}
}



uint16_t Helper::freemem(uint16_t* biggest)
{
	char* brkval;
	char* cp;
	unsigned freeSpace;
	struct __freelist* fp1, *fp2;

	brkval = __brkval;
	if (brkval == 0)
	{
		brkval = __malloc_heap_start;
	}
	cp = __malloc_heap_end;
	if (cp == 0)
	{
		cp = ((char*)AVR_STACK_POINTER_REG) - __malloc_margin;
	}
	if (cp <= brkval)
	{
		return 0;
	}

	freeSpace = cp - brkval;

	for (*biggest = 0, fp1 = __flp, fp2 = 0;
			fp1;
			fp2 = fp1, fp1 = fp1->nx)
	{
		if (fp1->sz > *biggest)
		{
			*biggest = fp1->sz;
		}
		freeSpace += fp1->sz;
	}

	return freeSpace;
}

void Helper::freeMem(char* label)
{
	uint16_t biggest;
	if (label != NULL)
	{
		Serial.print(label);
	}
	Serial.print(freemem(&biggest));
}


