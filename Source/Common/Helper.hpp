/*
 * File:   Helper.hpp
 * Author: purinda
 *
 * Created on 13 April 2012, 10:28 PM
 */

#ifndef HELPER_HPP
#define	HELPER_HPP

#include <Arduino.h>
#include <stdint.h>

//Code to print out the free memory
struct __freelist
{
	size_t sz;
	struct __freelist* nx;
};

extern char* const __brkval;
extern struct __freelist* __flp;



class Helper
{
	public:
		Helper();
		virtual ~Helper();
		static void charToUint8_t(char*, uint8_t*, unsigned short);
		static void Uint8_tToChar(uint8_t*, char*, unsigned short);
		static void freeMem(char*);

	private:
		static uint16_t freemem(uint16_t* );
};

#endif	/* HELPER_HPP */

