/*
 * File:   MsgParser.hpp
 * Author: purinda
 *
 *
 * Created on 14 April 2012, 7:43 PM
 */

#ifndef MESSAGING_HPP
#define	MESSAGING_HPP

#include <Arduino.h>
#include <stdint.h>
#include <../Common/Helper.hpp>
#include <../Common/Nrf24.hpp>
#include <../Common/PacketTypes.h>

class Messaging
{
	public:
		Messaging();
		// HC functions
//		static void dig(uint64_t address);
//		static void request(uint64_t address, uint8_t ct, uint16_t instruction);

		// CM functions
		static void beacon(PacketDDP* data);
		static void reply(uint8_t ct, float returnVal, uint64_t address, uint16_t packetId);

	private:
		static void processCommand();

};

#endif	/* MSGPARSER_HPP */

