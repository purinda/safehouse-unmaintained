/*
 * File:   Nrf24.hpp
 * Author: purinda
 *
 * Created on 10 April 2012, 11:36 PM
 *
 * Pins
 * ------------
 * Hardware SPI:
 * MISO -> 12
 * MOSI -> 11
 * SCK -> 13
 *
 * Configurable:
 * CE -> 8
 * CSN -> 7
 */

#ifndef NRF24_HPP
#define	NRF24_HPP


#include <../Common/Scheduler.hpp>
#include <config.hpp>
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>


class Nrf24
{
	public:
		// Singleton pattern implemented as per
		// http://stackoverflow.com/questions/1008019/c-singleton-design-pattern
		static Nrf24& getInstance()
		{
			static Nrf24 instance;
			return instance;
		}

		void initialize(uint8_t, uint8_t);
		void setToAddress(uint64_t);
		void setFromAddress(uint64_t);

		bool send(void*);

		// Low level MIRF access, take this out later
		// Set up nRF24L01 radio on SPI bus plus pins 9 & 10
		RF24* radio;
		// Public variables
		static const short PAYLOAD = 16;
//		static const short PAYLOAD = 8;
		static const uint8_t ADDRESSWIDTH = 5;
		uint64_t my_address_;
		uint64_t receiver_address_;

	private:
		Nrf24();
		Nrf24(Nrf24 const&);
		void operator=(Nrf24 const&);
};

#endif	/* NRF24_HPP */

