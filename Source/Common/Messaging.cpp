/*
 * File:   MsgParser.cpp
 * Author: purinda
 *
 * Created on 14 April 2012, 7:43 PM
 */

#include <string.h>
#include <stdio.h>

#include "Messaging.hpp"
/*
DEV TYPE #	TYPE	DESCRIPTION
000	 Reserved	Reserved device type, dummy module implementation
001	 Bulb           Implements on and off
002	 Bulb (Dimmable)PWM IOs will be used, 0-255 range is used to control intensity.
003	 Bulb + PIR	Bulb with ability to capture human movements.
004	 Appliance
         Controller
         + DHT11 	Appliance controller used to switch on/off connected devices connected to it, and monitor atmospheric temperature and humidity at the same time.
005	 DHT11          monitor atmospheric temperature and humidity
*/

Messaging::Messaging()
{
}

// CM functions
void Messaging::beacon(PacketDDP* data)
{
	// #2 - Device Data Packet ID
	data->packetType = PT_DEVICE_DATA_PACKET;
	uint8_t* ptr_request_packet = (uint8_t*)data;
	Nrf24::getInstance().send(ptr_request_packet);
}

void Messaging::reply(uint8_t ct, float returnVal, uint64_t address, uint16_t packetId)
{
	PacketCSP rep;

	// Packet to be sent to the CM
	rep.packetType = PT_COMMAND_STATUS_PACKET;
	rep.id = packetId;
	rep.commandType = ct;
	rep.value = returnVal;
	rep.address = address;
	
	// Send it!
	uint8_t* ptr_reply_packet = (uint8_t*)&rep;
	Nrf24::getInstance().send(ptr_reply_packet);

}
