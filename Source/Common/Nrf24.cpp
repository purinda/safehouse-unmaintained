/*
 * File:   Nrf24.cpp
 * Author: purinda
 *
 * Created on 10 April 2012, 11:40 PM
 */

#include "Nrf24.hpp"
#include "Helper.hpp"
#include "../Common/Messaging.hpp"

Nrf24::Nrf24()
{

}

void Nrf24::initialize(uint8_t _ce, uint8_t _csn)
{
	radio = new RF24(_ce, _csn);
	radio->begin();
	// optionally, increase the delay between retries & # of retries
	radio->setRetries(15, 15);
	// optionally, reduce the payload size.  seems to
	// improve reliability
	radio->setPayloadSize(PAYLOAD);		
	radio->setDataRate(RF24_250KBPS);
	radio->startListening();	
}

void Nrf24::setToAddress(uint64_t addr)
{
	radio->stopListening();	
	radio->openWritingPipe(addr);
	this->receiver_address_ = addr;
	radio->startListening();
}

void Nrf24::setFromAddress(uint64_t addr)
{
	radio->stopListening();	
	radio->openReadingPipe(1, addr);	
	this->my_address_ = addr;
	radio->startListening();
}

bool Nrf24::send(void* msg)
{
    // First, stop listening so we can talk.
    radio->stopListening();	
	radio->write(msg, PAYLOAD);
    // Now, continue listening
    radio->startListening();
	
	return true;
}


