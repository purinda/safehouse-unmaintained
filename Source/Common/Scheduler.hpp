/*
 * File:   Scheduler.hpp
 * Author: purinda
 *
 * Created on 10 April 2012, 11:40 PM
 */

#ifndef SCHEDULER_HPP
#define	SCHEDULER_HPP

#include <Arduino.h>

#define QUEUE_MAX 32
#define SCHEDULE_MAX 32

#define INIT_TIMER_COUNT 6
#define RESET_TIMER2 TCNT2 = INIT_TIMER_COUNT

class ITask;
class Scheduler;

typedef struct ScheduleItemStruct
{
	ITask* task;
	unsigned long milliseconds;
}
ScheduleItem;


class ITask
{
	public:
		virtual void initialize() = 0;
		virtual void run(Scheduler* scheduler) = 0;

};

class Scheduler
{
	public:
		Scheduler();

		void initialize();
		void processMessages();

		void queue(ITask* task);
		void clearQueue();

		void schedule(ITask*, int);
		void clearSchedule();

	private:
		ITask* taskQueue_[QUEUE_MAX];
		ScheduleItem taskSchedule_[SCHEDULE_MAX];
};

#endif	/* SCHEDULER_HPP */
