/*
 * File:   Scheduler.cpp
 * Author: purinda
 *
 * Created on 10 April 2012, 11:40 PM
 */

#include "Scheduler.hpp"

unsigned long ulCounter__ = 0;

ISR(TIMER2_OVF_vect)
{
	RESET_TIMER2;
	ulCounter__++;
};

Scheduler::Scheduler()
{
	clearQueue();
	clearSchedule();
}

void Scheduler::initialize()
{
	//Timer2 Settings: Timer Prescaler /64,
	TCCR2A |= (1<<CS22);
	TCCR2A &= ~((1<<CS21) | (1<<CS20));

	// Use normal mode
	TCCR2A &= ~((1<<WGM21) | (1<<WGM20));

	// Use internal clock - external clock not used in Arduino
	ASSR |= (0<<AS2);

	//Timer2 Overflow Interrupt Enable
	TIMSK2 |= (1<<TOIE2) | (0<<OCIE2A);

	// Reset timer
	RESET_TIMER2;
	sei();

}

void Scheduler::processMessages()
{
	// Check the schedule
	for( int i=0; i<SCHEDULE_MAX; i++)
	{
		if(taskSchedule_[i].task &&
				taskSchedule_[i].milliseconds <= ulCounter__)
		{
			queue(taskSchedule_[i].task);
			taskSchedule_[i].task = 0;
		}
	}

	// Clear the queue
	for( int i=0; i<QUEUE_MAX; i++)
	{
		if(taskQueue_[i] )
		{
			taskQueue_[i]->run(this);
			taskQueue_[i] = 0;
		}
	}
}

void Scheduler::queue(ITask* task)
{
	for( int i=0; i<QUEUE_MAX; i++)
	{
		if(!taskQueue_[i] )
		{
			taskQueue_[i] = task;
			return;
		}
	}
}

void Scheduler::clearQueue()
{
	for( int i=0; i<QUEUE_MAX; i++)
	{
		taskQueue_[i] = 0;
	}
}

void Scheduler::schedule(ITask* task, int milliseconds)
{
	for( int i=0; i<SCHEDULE_MAX; i++)
	{
		if(!taskSchedule_[i].task)
		{
			taskSchedule_[i].task = task;
			taskSchedule_[i].milliseconds = ulCounter__ + milliseconds;
			return;
		}
	}
}

void Scheduler::clearSchedule()
{
	for( int i=0; i<SCHEDULE_MAX; i++)
	{
		taskSchedule_[i].task = 0;
	}
}
