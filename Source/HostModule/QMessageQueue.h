/* 
 * File:   QMessageQueue.h
 * Author: purinda
 *
 * Created on 30 June 2012, 4:12 PM
 */

#ifndef QMESSAGEQUEUE_H
#define	QMESSAGEQUEUE_H

#include <QtCore>
#include <QtDebug>
#include <QList>
#include <inttypes.h>
#include "CMessage.h"
#include "requestmapper.h"
#include "QNrf24.hpp"

class QMessageQueue : public QObject{
    Q_OBJECT
	
public:
	
	explicit QMessageQueue ();	
	virtual ~QMessageQueue ();
	
	void init(QThread &qthread);
	
private:
	QThread *__threadReference;
	
	// Two static queues to handle in/out bound message queues and 
	// notification queue for immediate  
	QHash<long, CMessage> __message_queue_inbound;
	QHash<long, CMessage> __message_queue_outbound;
	

public slots:
	void slotProcessMessages();
	void slotPendingMessage(CMessage);
	void slotQueueMessage(CMessage _msg);
	
signals:
	void signalFinishedProcessing();
	void signalMessageQueueChanged();
	void signalNotificationQueueChanged();	
	/**
	 * 
     * @param 
     */
	void signalTransmit(CMessage);
	
	/**
	 * 
     * @param 
     */
	void signalSend2Exchange(CMessage);
	
};

#endif	/* QMESSAGEQUEUE_H */
