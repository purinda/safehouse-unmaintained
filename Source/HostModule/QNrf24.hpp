/*
 * File:   Nrf24.hpp
 * Author: purinda
 *
 * Created on 10 April 2012, 11:36 PM
 */

#ifndef QNRF24_HPP
#define	QNRF24_HPP

#include <QtCore>
#include <QtDebug>
#include <inttypes.h>

#include <nRF24L01.h>
#include <RF24.h>

#include "PacketTypes.h"
#include "QThreadX.h"
#include "HostConfig.h"
#include "requestmapper.h"
#include "CMessage.h"

class QNrf24: public QObject{
    Q_OBJECT

public:
	enum RadioType {
		RADIO_TYPE_LISTENER=0,
		RADIO_TYPE_TRANSMITTER=1
	};
	
	QNrf24(RadioType);		
	void init(QThread &qthread);
	void setFromAddress(uint64_t);
	void setToAddress(uint64_t);	
	bool send(void*);
	
	// Low level MIRF access, take this out later
	// Set up nRF24L01 radio on SPI bus plus pins 9 & 10
	RF24* radio;

	uint64_t my_address_;
	uint64_t receiver_address_;
	
public slots:
	void listen();
	void receiveMessage(CMessage);

signals:
	void pendingPacket(CMessage);
	
private:
	QThread *__threadReference;	
	
	uint8_t __payload[PAYLOAD];	
	std::string __spidev;
	uint8_t __ce;
	uint8_t __csn;
	RadioType __role;
};

#endif	/* QNRF24_HPP */

