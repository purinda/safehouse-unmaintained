/*
 * File:   Constants.h
 * Author: purinda
 *
 * Created on 19 July 2012, 2:08 PM
 */

#ifndef CONSTANTS_H
#define	CONSTANTS_H

#include <inttypes.h>
#include <iostream>
#include <QtCore>

namespace Safehouse
{
	// For CRule
	enum ConditionType{
		ConditionEqual,
		ConditionGreaterThan,
		ConditionLessThan,
		ConditionGreaterThanOrEqual,
		ConditionLessThanOrEqual
	};

	enum RuleType{
		RuleAbility,
		RuleSchedule
	};
	
	enum ActionType{
		ActionSMS,
		ActionTweet,
		ActionEmail,
		ActionAbility
	};
	
}


#ifdef	__cplusplus
extern "C"
{
#endif

	static const char * HTTP_GET = "GET";	
	static const char * HTTP_POST = "POST";
	// Public variables
	static const short PAYLOAD = 16;
	static const uint8_t ADDRESSWIDTH = 5;

	
	static const char * DB_HOST = "192.168.1.50";
	static const char * DB_NAME = "safehouse";
	static const char * DB_USER = "root";
	static const char * DB_PSWD = "Chat5w00d";
	
	// Some config settings
	static const uint8_t SPIDEV1_CE_PIN = 48;
	static const uint8_t SPIDEV1_CSN_PIN = 60;	
	
	static const uint8_t SPIDEV2_CE_PIN = 115;
	static const uint8_t SPIDEV2_CSN_PIN = 117;	
	
	static const bool SYSTEM_LOG_ENABLED = true;
	
#ifdef	__cplusplus
}

namespace RuleTypes{
	static const QString RULE_TYPE_SCHEDULE = "schedule";
	static const QString RULE_TYPE_ABILITY = "ability";
}

namespace DateTimeFormats{
	static const QString TIME_FORMAT_DISPLAY = "hh:mm AP";
	static const QString TIME_FORMAT_DATABASE = "hh:mm";
	static const QString DATETIME_FORMAT_JQPLOT = "yyyy-MM-dd h:mmAP";	
}


#endif

#endif	/* CONSTANTS_H */

