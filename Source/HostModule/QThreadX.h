/* 
 * File:   QThreadX.h
 * Author: purinda
 *
 * Created on 4 July 2012, 9:57 PM
 */

#ifndef QTHREADX_H
#define	QTHREADX_H

class QThreadX : public QThread
{
public:
    static void msleep(unsigned long msecs)
    {
        QThread::msleep(msecs);
    }
	
    void _exec_()
    {
            exec();
    }

};

#endif	/* QTHREADX_H */

