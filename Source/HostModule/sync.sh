#!/bin/bash

function release
{
        scp -rC dist/Release/GNU_Arm-Linux-x86/HostModule etc purinda@beagle://home/purinda/Workspace/Safehouse/
        exit 1
}

function all
{
	scp -rC dist/Debug/GNU_Arm-Linux-x86/HostModule etc purinda@beagle://home/purinda/Workspace/Safehouse/
	exit 1
}

function app
{
	scp -C dist/Debug/GNU_Arm-Linux-x86/HostModule purinda@beagle://home/purinda/Workspace/Safehouse/
	exit 1
}

if [ "$1" == "all" ] 
then
	all
fi

if [ "$1" == "release" ] 
then
	release
fi


