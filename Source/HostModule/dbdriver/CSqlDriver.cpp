/* 
 * File:   SqlDriver.cpp
 * Author: purinda
 * 
 * Created on 5 August 2012, 10:27 PM
 * Inspired by http://www.developer.nokia.com/Community/Wiki/Creating_an_SQLite_database_in_Qt
 */

#include "CSqlDriver.h"

	
// resources locker
QMutex CSqlDriver::handlerLock;
QSqlDatabase CSqlDriver::__db;

CSqlDriver::CSqlDriver()
{
//	this->connect();
}

void CSqlDriver::connect()
{
	__isConnected = false;

	// Find QSLite driver
	__db = QSqlDatabase::addDatabase("QSQLITE");
	
	#ifdef Q_OS_LINUX
	// NOTE: We have to store database file into user home folder in Linux
	QString path("//media//purinda@zeus//Projects//Safehouse//safehouse.db.sqlite");
	path = QDir::toNativeSeparators(path);
	__db.setDatabaseName(path);
	#else
	// NOTE: File exists in the application private folder, in Symbian Qt implementation
	db.setDatabaseName("safehouse.db.sqlite");
	#endif

	// Open databasee
	bool ok = __db.open();
	
	if (!ok)
		RequestMapper::appendLog("Error connecting to Database");
	else 
	{
		RequestMapper::appendLog("Connected to Database");

		// SQLite PRAGMA options set for performance
		QSqlQuery(__db).exec("PRAGMA page_size = 4096;");    
		QSqlQuery(__db).exec("PRAGMA cache_size = 32768;");    
		QSqlQuery(__db).exec("PRAGMA temp_store = MEMORY;");	
		QSqlQuery(__db).exec("PRAGMA journal_mode = OFF;");
		QSqlQuery(__db).exec("PRAGMA locking_mode = EXCLUSIVE;");
		QSqlQuery(__db).exec("PRAGMA synchronous = OFF;");
		
		__isConnected = true;
	}
	
}

bool CSqlDriver::isConnected()
{
	return __isConnected;
}

QSqlDatabase CSqlDriver::getHandle()
{
	if (__db.lastError().type() != QSqlError::NoError)
	{
		RequestMapper::appendLog("Error connecting to Database");
		connect();
	}
	
	return this->__db;
}


void CSqlDriver::logQueryError(QSqlQuery __q)
{
	// log only if an error occurs
	if (__q.lastError().isValid())
	{
		RequestMapper::appendLog("Database Query Error: " +  QString::number(__q.lastError().number(),10) + " " + __q.lastError().text() );
		RequestMapper::appendLog("Database Query : " +  __q.executedQuery());
	}
	
}