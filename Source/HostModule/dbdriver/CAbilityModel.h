/* 
 * File:   CAbilityModel.h
 * Author: purinda
 *
 * Created on 5 August 2012, 11:27 PM
 */

#ifndef CABILITYMODEL_H
#define	CABILITYMODEL_H

#include <QtCore>
#include "CSqlDriver.h"
#include <QSqlQuery>
#include <QSqlError>
#include "../CAbility.h"
#include <QSqlResult>

class CAbilityModel
{
public:
	CAbilityModel();
	
	static void saveValue(int abl_id, float value);
	static QMap<int, CAbility*> getAbilities(QByteArray);
	static QMap<uint, float> getHistoryForVariable(int abl_id, int interval, int since);
	static QMap<uint, int> getHistoryForDiscrete(int abl_id, int since);
	static float getLastValue(int abl_id);
	static QSqlQuery getAbility(int abl_id);
	
private:

};

#endif	/* CABILITYMODEL_H */

