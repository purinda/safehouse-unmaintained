/* 
 * File:   CModuleModel.cpp
 * Author: purinda
 * 
 * Created on 5 August 2012, 11:27 PM
 */

#include "CModuleModel.h"
#include "requestmapper.h"
#include <QSqlError>
#include <QSqlResult>

CModuleModel::CModuleModel()
{
	
}

CModuleList CModuleModel::getModules()
{	
	CModuleList __modules;
	
	QSqlQuery query(CSqlDriver::getInstance().getHandle());
	query.exec("SELECT mod_id, name, description, location, att_id, is_active, is_deleted FROM module WHERE is_active = 1 AND is_deleted = 0 ORDER BY gravity ASC");
	
	int index = 0;
	
	while (query.next()) 
	{
		CModule __module;
		bool success = false;
	
		__module.setIndex(index);
		__module.setAddress(query.value(0).toByteArray());
		__module.setName(query.value(1).toString());
		__module.setDescription(query.value(2).toString());
		__module.setType(query.value(4).toInt(&success));
		// load all associated abilities of the module
		__module.loadAbilities();
		
		__modules.append( __module );
		
		index++;
	}
	// log if any errors occur
	CSqlDriver::logQueryError(query);
	
	return __modules;
}



