/* 
 * File:   CRuleActionModel.h
 * Author: purinda
 *
 * Created on 29 August 2012, 10:57 PM
 */

#ifndef CRULEACTIONMODEL_H
#define	CRULEACTIONMODEL_H

#include <QtCore>
#include "CSqlDriver.h"
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlResult>
#include "../Constants.h"


class CRuleActionModel
{
public:
	CRuleActionModel();
	int saveRule(int rul_id, int abl_id, Safehouse::ConditionType comparator, int tsk_id, Safehouse::RuleType ruleType, QVariant value);
	QSqlQuery getRules(int);
	
	int saveAction(int act_id, int abl_id, int tsk_id, Safehouse::ActionType, QVariant value);
	QSqlQuery getActions(int);

	int saveTask(int tsk_id, QString name, QString description);
	QSqlQuery getTasks();
	QSqlQuery getTask(int tsk_id);
	void deleteTask(int tsk_id);

private:

};

#endif	/* CRULEACTIONMODEL_H */
