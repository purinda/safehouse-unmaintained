/* 
 * File:   SqlDriver.h
 * Author: purinda
 *
 * Created on 5 August 2012, 10:27 PM
 */

#ifndef CSQLDRIVER_H
#define	CSQLDRIVER_H

#include <QtSql/QSqlDatabase>
#include "Constants.h"
#include "requestmapper.h"
#include <QSqlError>
#include <QSqlQuery>

class CSqlDriver
{
public:
	static CSqlDriver& getInstance()
	{
		QMutexLocker lock (&handlerLock);
		static CSqlDriver instance;		// Guaranteed to be destroyed.
										// Instantiated on first use.
		return instance;
	}
	
	QSqlDatabase getHandle();
	void connect();
	bool isConnected();
	
	static void logQueryError(QSqlQuery);

private:
	CSqlDriver();
	CSqlDriver(CSqlDriver const&);			// Don't Implement
	void operator=(CSqlDriver const&);		// Don't implement	
	
	bool __isConnected;
	static QSqlDatabase __db;
	
	// resources locker
	static QMutex handlerLock;
};

#endif	/* CSQLDRIVER_H */

