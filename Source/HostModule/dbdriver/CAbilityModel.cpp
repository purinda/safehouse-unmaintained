/* 
 * File:   CAbilityModel.cpp
 * Author: purinda
 * 
 * Created on 5 August 2012, 11:27 PM
 */

#include "CAbilityModel.h"


CAbilityModel::CAbilityModel()
{
	
}

void CAbilityModel::saveValue(int abl_id, float value)
{	
	QSqlQuery query(CSqlDriver::getInstance().getHandle());
	query.prepare("INSERT INTO ability_history(abl_id, value)"
				  "VALUES(:abl_id, :value)");
	
	query.bindValue(":abl_id", abl_id);
	query.bindValue(":value", value);
	query.exec();	
	
	// log if any errors occur
	CSqlDriver::logQueryError(query);
}

/**
 * Default interval is 300sec or 5min intervals.
 * TODO: For some reason data results from the below query are not ordered 
 * in ASC order eventhough the query asks for ordered results.
 * 
 * @param abl_id
 * @param interval
 * @return 
 */
QMap<uint, float> CAbilityModel::getHistoryForVariable(int abl_id, int interval, int since)
{
/*
SELECT 
UNIX_TIMESTAMP(last_updated) AS last_updated, 
ah.abl_id, 
count(ah.abl_id) AS samples, 
AVG(ah.value) AS avg 
FROM ability_history ah 
WHERE ah.abl_id = 5 AND UNIX_TIMESTAMP(last_updated) >= (UNIX_TIMESTAMP(NOW()) - 86400)
GROUP BY round(UNIX_TIMESTAMP(last_updated) DIV 900) 
ORDER BY last_updated ASC 
*/
	
	QSqlQuery query(CSqlDriver::getInstance().getHandle());
	query.prepare(
"SELECT "
"strftime('%s', last_updated) AS last_updated, "
"ah.abl_id, "
"count(ah.abl_id) AS samples, "
"AVG(ah.value) AS avg "
"FROM ability_history ah "
"WHERE ah.abl_id = :abl_id AND CAST(strftime('%s', last_updated) AS INTEGER) >= CAST(strftime('%s', 'now') - :since AS INTEGER) "
"GROUP BY round(strftime('%s', last_updated) / :interval) "
"ORDER BY last_updated ASC "
);
	
	query.bindValue(":abl_id", abl_id);
	query.bindValue(":interval", interval);
	query.bindValue(":since", since);
	query.exec();	
	
	// log if any errors occur
	CSqlDriver::logQueryError(query);
	
	QMap<uint, float> __return;
	
	while (query.next())
	{	
		float __value = query.value(3).toFloat();
		__return.insert(query.value(0).toInt(), __value);
	}
	
	return __return;
}

QMap<uint, int> CAbilityModel::getHistoryForDiscrete(int abl_id, int since)
{
/*
SELECT 
UNIX_TIMESTAMP(last_updated) AS last_updated,
ah.abl_id, 
ah.value
FROM ability_history ah 
WHERE ah.abl_id = 1 AND UNIX_TIMESTAMP(last_updated) >= (UNIX_TIMESTAMP(NOW()) - 86400)
ORDER BY last_updated ASC 
*/
	
	QSqlQuery query(CSqlDriver::getInstance().getHandle());
	query.prepare(
"SELECT "
"strftime('%s', last_updated) AS last_updated, "
"ah.abl_id, "
"count(ah.abl_id) AS samples, "
"ah.value AS value "
"FROM ability_history ah "
"WHERE ah.abl_id = :abl_id AND CAST(strftime('%s', last_updated) AS INTEGER) >= CAST(strftime('%s', 'now') - :since AS INTEGER) "
"GROUP BY round(strftime('%s', last_updated) / :interval) "
"ORDER BY last_updated ASC "
);
	
	query.bindValue(":abl_id", abl_id);
	query.bindValue(":since", since);
	query.exec();	
	
	// log if any errors occur
	CSqlDriver::logQueryError(query);
	
	QMap<uint, int> __return;
	
	while (query.next())
	{	
		__return.insert(query.value(0).toInt(), query.value(2).toInt());
	}
	
	return __return;
}


float CAbilityModel::getLastValue(int abl_id)
{
	QSqlQuery query(CSqlDriver::getInstance().getHandle());
	query.prepare("SELECT "
"last_updated, "
"ah.abl_id, "
"ah.value "
"FROM ability_history ah "
"WHERE ah.abl_id = :abl_id "
"ORDER BY last_updated DESC "
"LIMIT 1"
);	
	
	query.bindValue(":abl_id", abl_id);
	query.exec();	
	
	// log if any errors occur
	CSqlDriver::logQueryError(query);
	query.next();
	
	return query.value(2).toFloat();
}

QMap<int, CAbility*> CAbilityModel::getAbilities(QByteArray __module_address)
{	
	QMap<int, CAbility*> __abilities;
	
	QSqlQuery query(CSqlDriver::getInstance().getHandle());
	query.exec("SELECT abl_id, att_id, name, description FROM ability WHERE mod_id = '" + QString(__module_address) +"' AND is_active = 1 AND is_deleted = 0 ORDER BY gravity ASC");
//	Dump to see what actually executed
//	RequestMapper::appendLog(query.executedQuery());
	int index = 0;
	
	while (query.next()) 
	{
		CAbility* __ability = new CAbility;
		bool success = false;
		
		__ability->setIndex(index);
		__ability->setId(query.value(0).toInt());
		__ability->setType(query.value(1).toInt());
		__ability->setName(query.value(2).toString());
		__ability->setDescription(query.value(3).toString());
		
		__abilities[index] = __ability;
		
		index++;
	}
	// log if any errors occur
	CSqlDriver::logQueryError(query);
	
	return __abilities;
}

QSqlQuery CAbilityModel::getAbility(int abl_id)
{	
	QMap<int, CAbility*> __abilities;
	
	QSqlQuery query(CSqlDriver::getInstance().getHandle());
	query.exec("SELECT abl_id, att_id, name, description FROM ability WHERE abl_id = '" + QString::number(abl_id, 10) +"' AND is_active = 1 AND is_deleted = 0 ORDER BY gravity ASC");
//	Dump to see what actually executed
//	RequestMapper::appendLog(query.executedQuery());

	// log if any errors occur
	CSqlDriver::logQueryError(query);
	
	return query;
}
