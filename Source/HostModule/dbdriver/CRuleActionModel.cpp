/* 
 * File:   CRuleActionModel.cpp
 * Author: purinda
 * 
 * Created on 29 August 2012, 10:57 PM
 */

#include "CRuleActionModel.h"

CRuleActionModel::CRuleActionModel()
{
	
	
}


int CRuleActionModel::saveRule(int rul_id, int abl_id, Safehouse::ConditionType comparator, int tsk_id, Safehouse::RuleType ruleType, QVariant value)
{	
	QSqlQuery query(CSqlDriver::getInstance().getHandle());
	
	if (rul_id == NULL || rul_id == 0)
	{
		query.prepare("INSERT INTO rule(abl_id, tsk_id, comparator, type, value) "
					  "VALUES(:abl_id, :tsk_id, :comparator, :rule_type, :value) ");
	}
	else
	{
		query.prepare("UPDATE rule "
					  "SET tsk_id = :tsk_id, name = :name, description = :desc "
					  "WHERE rul_id = :rul_id");
	}
	
	if (abl_id == NULL || abl_id == 0)
		query.bindValue(":abl_id", "NULL");
	else
		query.bindValue(":abl_id", abl_id);
		
	query.bindValue(":rul_id", rul_id);
	query.bindValue(":tsk_id", tsk_id);
	
	switch (comparator)
	{
		case Safehouse::ConditionEqual:
			query.bindValue(":comparator", "=");
			break;
		case Safehouse::ConditionGreaterThan:
			query.bindValue(":comparator", ">");
			break;
		case Safehouse::ConditionGreaterThanOrEqual:
			query.bindValue(":comparator", ">=");
			break;
		case Safehouse::ConditionLessThan:
			query.bindValue(":comparator", "<");
			break;
		case Safehouse::ConditionLessThanOrEqual:
			query.bindValue(":comparator", "<=");
			break;
			
	}

	switch (ruleType)
	{
		case Safehouse::RuleAbility:
			query.bindValue(":rule_type", "ability");
			query.bindValue(":value", value);
			break;
		case Safehouse::RuleSchedule:
			query.bindValue(":rule_type", "schedule");
			// set time values
			QTime __v = QTime::fromString(value.toString(), DateTimeFormats::TIME_FORMAT_DATABASE);
			query.bindValue(":value", __v.toString(DateTimeFormats::TIME_FORMAT_DATABASE));
			break;
	}
	
	query.exec();
	
	// log if any errors occur
	CSqlDriver::logQueryError(query);
	
	return query.lastInsertId().toInt();
}


QSqlQuery CRuleActionModel::getRules(int __tsk_id)
{	

	QSqlQuery query(CSqlDriver::getInstance().getHandle());
	query.prepare("SELECT "
	"type, "
	"abl_id, "
	"comparator, "
	"value "
	"FROM rule "
	"WHERE `tsk_id` = :tsk_id");
	query.bindValue(":tsk_id", __tsk_id);
	query.exec();
	
	// log if any errors occur
	CSqlDriver::logQueryError(query);
	
	
	return query;
}


int CRuleActionModel::saveAction(int act_id, int abl_id, int tsk_id, Safehouse::ActionType __type, QVariant value)
{	
	QSqlQuery query(CSqlDriver::getInstance().getHandle());
	
	if (act_id == NULL)
	{
		query.prepare("INSERT INTO action(abl_id, tsk_id, type, value) "
					  "VALUES(:abl_id, :tsk_id, :type, :value)");
	}
	else
	{
		query.prepare("UPDATE action "
					  "SET tsk_id = :tsk_id, abl_id = :abl_id, type = :type, value = :value "
					  "WHERE act_id = :act_id");
	}
	
	QString type = "";
	
	switch(__type)
	{
		case Safehouse::ActionAbility:
			type = "ability";
			break;
		case Safehouse::ActionSMS:
			type = "sms";
			break;
		case Safehouse::ActionTweet:
			type = "tweet";
			break;
		case Safehouse::ActionEmail:
			type = "email";
			break;
	}
	
	if (abl_id == NULL || abl_id == 0)
		query.bindValue(":abl_id", "NULL");
	else
		query.bindValue(":abl_id", abl_id);
	
	query.bindValue(":act_id", act_id);
	query.bindValue(":type", type);
	query.bindValue(":tsk_id", tsk_id);
	query.bindValue(":value", value);
	query.exec();
	
	// log if any errors occur
	CSqlDriver::logQueryError(query);
	
	return query.lastInsertId().toInt();
}


QSqlQuery CRuleActionModel::getActions(int __tsk_id)
{	
	QSqlQuery query(CSqlDriver::getInstance().getHandle());
	query.prepare("SELECT "
	"type, "
	"abl_id, "
	"value "
	"FROM action "
	"WHERE `tsk_id` = :tsk_id");
	query.bindValue(":tsk_id", __tsk_id);
	query.exec();
	
	// log if any errors occur
	CSqlDriver::logQueryError(query);
	
	return query;
}


int CRuleActionModel::saveTask(int tsk_id, QString name, QString description)
{	
	QSqlQuery query(CSqlDriver::getInstance().getHandle());
	
	if (tsk_id == NULL)
	{
		query.prepare("INSERT INTO task(name, description) "
					  "VALUES(:name, :desc) ");
	}
	else
	{
		query.prepare("UPDATE task "
					  "SET name = :name, description = :desc "
					  "WHERE tsk_id = :tsk_id");		
	}
	
	query.bindValue(":tsk_id", tsk_id);
	query.bindValue(":name", name);
	query.bindValue(":desc", description);
	query.exec();
	
	// get the new task id
	tsk_id = query.lastInsertId().toInt();
	
	// log if any errors occur
	CSqlDriver::logQueryError(query);
	
	return tsk_id;
}


QSqlQuery CRuleActionModel::getTask(int tsk_id)
{	

	QSqlQuery query(CSqlDriver::getInstance().getHandle());
	query.prepare("SELECT "
    "name, "
    "description "
	"FROM task WHERE tsk_id = :tsk_id");
	query.bindValue(":tsk_id", tsk_id);
	query.exec();
	
	// log if any errors occur
	CSqlDriver::logQueryError(query);
	
	
	return query;
}

void CRuleActionModel::deleteTask(int tsk_id)
{	

	QSqlQuery query(CSqlDriver::getInstance().getHandle());
	query.prepare("DELETE FROM task WHERE tsk_id = :tsk_id");
	query.bindValue(":tsk_id", tsk_id);
	query.exec();
	
	query.prepare("DELETE FROM rule WHERE tsk_id = :tsk_id");
	query.bindValue(":tsk_id", tsk_id);
	query.exec();
	
	query.prepare("DELETE FROM action WHERE tsk_id = :tsk_id");
	query.bindValue(":tsk_id", tsk_id);
	query.exec();
	
	// log if any errors occur
	CSqlDriver::logQueryError(query);
	
}

QSqlQuery CRuleActionModel::getTasks()
{	

	QSqlQuery query(CSqlDriver::getInstance().getHandle());
	query.prepare("SELECT "
	"tsk_id, "
    "name, "
    "description "
	"FROM task");
	query.exec();
	
	// log if any errors occur
	CSqlDriver::logQueryError(query);
	
	
	return query;
}

