/* 
 * File:   CModuleModel.h
 * Author: purinda
 *
 * Created on 5 August 2012, 11:27 PM
 */

#ifndef CMODULEMODEL_H
#define	CMODULEMODEL_H

#include "CSqlDriver.h"
#include <QSqlQuery>
#include "requestmapper.h"
#include <QSqlError>
#include <QSqlResult>
#include "../CModule.h"

class CModuleModel
{
public:
	CModuleModel();
	
	static CModuleList getModules();
	
private:

};

#endif	/* CMODULEMODEL_H */

