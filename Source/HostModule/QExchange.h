/* 
 * File:   QExchange.h
 * Author: purinda
 *
 * Created on 1 July 2012, 10:53 PM
 */

#ifndef QEXCHANGE_H
#define	QEXCHANGE_H

#include <QtCore>
#include <QtDebug>
#include <inttypes.h>

#include "CMessage.h"
#include "CModule.h"

#include "CAbility.h"
#include "../rules/CRules.h"
#include "requestmapper.h"

class QExchange : public QObject{
    Q_OBJECT
	
public:
	QExchange();
	virtual ~QExchange();
	void init(QThread &qthread);
	void loadFromDatabase();
	void loadTasks();
	
	// resources locker
	QMutex lockTasks;	
	QMutex lockModules;
		
	static const int CYCLE_WIDTH = 60000;
	static const int TASK_CYCLE_WIDTH = 5000;
		
private:
	QVariant getAbilityValue(int __ability_id);
	
	QThread *__threadReference;
	long __elapsedCycles;
	
	QHash<int, CModule> modules;
	QList<CTask*> __tasks;
	
	void incrementCycle(bool);
	QDateTime __lastUpdated;
	
public slots:
	void slotSendStatReq2Queue();
	void slotSendAbility2Queue(CAbilityData);
	void slotIncomingMessage(CMessage);
	void slotSetAbility (CAbilityData);
	void slotTasksPollingEvent();
	void slotStatusPollingEvent();
	void slotProcessTasks();
	void slotLoadTasks();
	
	
signals:
	void signalQueueMessage(CMessage);	
	void signalSendModuleList(CModuleList);	
};

#endif	/* QEXCHANGE_H */

