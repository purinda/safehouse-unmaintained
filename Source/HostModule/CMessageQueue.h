/* 
 * File:   CMessageQueue.h
 * Author: purinda
 *
 * Created on 30 June 2012, 4:12 PM
 */

#ifndef CMESSAGEQUEUE_H
#define	CMESSAGEQUEUE_H

#include <queue>
#include <stdlib.h>
#include <inttypes.h>
#include <iostream>
#include "CMessage.h"

using namespace std;

class CMessageQueue
{
	
public:
	
	CMessageQueue ();	
	virtual ~CMessageQueue ();
	
	void queue(uint16_t _id, uint8_t _msg);
	message pop();
	
private:

	// Two static queues to handle in/out bound message queues and 
	// notification queue for immediate  
	static queue<message> __in_out_queue;
	static queue<message> __notification_queue;
	
	
};

#endif	/* CMESSAGEQUEUE_H */
