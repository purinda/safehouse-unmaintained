/*
 * File:   Nrf24.cpp
 * Author: purinda
 *
 * Created on 10 April 2012, 11:40 PM
 */

#include "QNrf24.hpp"



QNrf24::QNrf24(RadioType __type)
{
	__role = __type;
	
	switch (__type)
	{
		case RADIO_TYPE_LISTENER:
			// Setting PA as listener
			__spidev = "/dev/spidev2.0";
			__ce = SPIDEV2_CE_PIN;
			__csn = SPIDEV2_CSN_PIN;				
			break;
		case RADIO_TYPE_TRANSMITTER:
			// Normal PAless device as sender
			__spidev = "/dev/spidev1.0";
			__ce = SPIDEV1_CE_PIN;
			__csn = SPIDEV1_CSN_PIN;
			break;
	}
}

void QNrf24::init(QThread &qthread)
{
	this->__threadReference = &qthread;
		
	radio = new RF24(__ce, __csn);
	radio->begin(__spidev.c_str(), 16000000);
	// optionally, increase the delay between retries & # of retries
	radio->setRetries(15, 15);
	// optionally, reduce the payload size.  seems to
	// improve reliability
	
	radio->setPayloadSize(PAYLOAD);		
	radio->setDataRate(RF24_250KBPS);
	
	// Go into listen mode if initialized as the listener
	if (__role == RADIO_TYPE_LISTENER)
	{
		connect(&qthread, SIGNAL(started()),
				this, SLOT(listen()));		
	}
}

void QNrf24::listen()
{		
	// no need to go into a non-returning loop when you're the sender of messages
	if (__role != RADIO_TYPE_LISTENER)
		return;
	
	// Setup wireless address
	uint64_t* _host_address = (uint64_t*)CONFIG_HOST_ADDR;
	this->setFromAddress(*_host_address);		
	
	radio->startListening();
	
	// never return from loop
	while(1)
	{
		// check continuously for pending datapackets in NRF device
		if (radio->available())
		{
			// Buffer to store message received
			CMessage __receivedMessage;			
			// emit the received packet to listing objects
			radio->read(&__payload, PAYLOAD);
			PacketDummy *__message = (PacketDummy*)&__payload;

			switch(__message->packetType)
			{
				case PT_COMMAND_STATUS_PACKET:
				{
					PacketCSP *__packet = (PacketCSP *)&__payload;			

					__receivedMessage.to = QByteArray((char*)&__packet->address);
					__receivedMessage.packetType = __packet->packetType;
					__receivedMessage.commandType = __packet->commandType;
					__receivedMessage.id = __packet->id;
					__receivedMessage.value = __packet->value;
					// very important to flag the message as completed and processed as
					// we send a copy of the received message to message queue/
					__receivedMessage.processed = true;				
					
					emit pendingPacket(__receivedMessage);
				}
			}

//			Debug (Interrogate received packet)
			char *ptr = (char *)__message;
			uint8_t i = 0;
			for (i=0; i<PAYLOAD; i++)
			{
				RequestMapper::appendLog("\n\rChar at " + QString::number(i,10) + "=" + QString::number(ptr[i], 10));	
			}
		}
		
		// rapidly reduces CPU time used by the process
		QThreadX::msleep(10);
	}
}

void QNrf24::receiveMessage(CMessage __message)
{
	QMutex mutex;
	mutex.lock();
	
	PacketCP* tmp = new PacketCP;
	tmp->packetType = __message.packetType;
	tmp->commandType = __message.commandType;
	tmp->value = __message.value;
	tmp->id = __message.id;
	
	// Address the outgoing packet
	uint64_t to = *(uint64_t *)__message.to.data();
	this->setToAddress(to);
	this->send(tmp);

//	slow down process of sending messages
	QThreadX::msleep(100);

	
	mutex.unlock();
}

void QNrf24::setToAddress(uint64_t addr)
{
	radio->stopListening();	
	radio->openWritingPipe(addr);
	this->receiver_address_ = addr;
	radio->startListening();
}

void QNrf24::setFromAddress(uint64_t addr)
{
	radio->stopListening();	
	radio->openReadingPipe(1, addr);	
	this->my_address_ = addr;
	radio->startListening();
}

bool QNrf24::send(void* msg)
{
    radio->stopListening();	
	radio->write(msg, PAYLOAD);
    radio->startListening();

	return true;
}
