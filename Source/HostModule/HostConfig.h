/* 
 * File:   HostConfig.h
 * Author: purinda
 *
 * Created on 4 July 2012, 11:05 PM
 */

#ifndef HOSTCONFIG_H
#define	HOSTCONFIG_H

#ifdef	__cplusplus
extern "C"
{
#endif

// Host & Client Addresses
// Default Host Address
#define CONFIG_CLIENT_ADDR	"clie5";
#define CONFIG_HOST_ADDR	"serv1";


#ifdef	__cplusplus
}
#endif

#endif	/* HOSTCONFIG_H */

