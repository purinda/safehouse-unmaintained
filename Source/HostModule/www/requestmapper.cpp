/**
  @file
  @author Stefan Frings
*/

#include "requestmapper.h"
#include "static.h"
#include "staticfilecontroller.h"
#include "controller/home.h"
#include "controller/viewmodule.h"
#include "controller/tasks.h"

QStringList* RequestMapper::log = new QStringList();
// copy of modules in www end
CModuleList RequestMapper::modules;
QMutex RequestMapper::lockModules;
QMutex RequestMapper::lockLogging;

RequestMapper::RequestMapper(QObject* parent)
    :HttpRequestHandler(parent) 
{

}

void RequestMapper::appendLog(QString logmessage)
{
//	if (!SYSTEM_LOG_ENABLED)
//		return;
//	
//	RequestMapper::log->append(logmessage);
}

void RequestMapper::service(HttpRequest& request, HttpResponse& response) 
{
    QByteArray path=request.getPath();
//	RequestMapper::appendLog("Path=" + QString(path));
	
	// Home page
	if (path == "/") {
		HomeController().service(request, response);
	}
	// View module
	else if (path.startsWith("/viewModule")) {
		ViewModuleController().service(request, response);
	}
	// Rules & Triggers
	else if (path.startsWith("/tasks")) {
		TasksController().service(request, response);
		if (!request.getParameter("task").isEmpty())
		{
			emit signalReloadTasks();
		}
	}
	// Debugging
	else if (path.startsWith("/log")) {
		response.write(QString("<body><html>" + this->getLogs("<br>") + "</html></body>").toLatin1(), true);
	}	
	// All other pathes are mapped to the static file controller.
	else {
		Static::staticFileController->service(request, response);
	}
}

QString RequestMapper::getLogs(QString delimeter)
{
	QMutexLocker lock(&lockLogging);
	return RequestMapper::log->join(delimeter);
}

void RequestMapper::slotReceiveModuleList(CModuleList __modules)
{
	QMutexLocker lock(&RequestMapper::lockModules);
	RequestMapper::modules = __modules;
}
