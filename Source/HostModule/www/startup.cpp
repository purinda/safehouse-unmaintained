/**
  @file
  @author Stefan Frings
*/

#include <QtCore>
#include <qdebug.h>
#include <QDir>

#include "static.h"
#include "startup.h"

/** Name of this application */
#define APPNAME "HostController"

/** Publisher of this application */
#define ORGANISATION "Infrasoft"

/** Short description of this application */
#define DESCRIPTION "Safehouse host controller management service"

void Startup::start() {
    // Initialize the core application
    QCoreApplication* app = application();
    app->setApplicationName(APPNAME);
    app->setOrganizationName(ORGANISATION);
	
	// Instantiate a new Message Queue object
	requestMapper = new RequestMapper(app);
	
	// New thread to the radio TRANSMITTER
	QThread radioTransmitterThread;	
	radioTransmitter = new QNrf24(QNrf24::RADIO_TYPE_TRANSMITTER);
	radioTransmitter->init(radioTransmitterThread);
	
	// New thread to the radio LISTENER 
	QThread radioListenerThread;	
	radioListener = new QNrf24(QNrf24::RADIO_TYPE_LISTENER);
	radioListener->init(radioListenerThread);
	
	QThread queueThread;
	messageQ = new QMessageQueue();
	messageQ->init(queueThread);

	// Exchange object
	QThread exchangeThread;
	exchange = new QExchange();
	exchange->init(exchangeThread);
		
	/* Move to threads */
	
	// Setup message passing signal
	qRegisterMetaType<CMessage>("CMessage");
	QObject::connect (radioListener, SIGNAL(pendingPacket(CMessage)), 
					  messageQ, SLOT(slotPendingMessage(CMessage)), Qt::QueuedConnection);
	
	QObject::connect (messageQ, SIGNAL(signalTransmit(CMessage)), 
					  radioTransmitter, SLOT(receiveMessage(CMessage)), Qt::QueuedConnection);
	
	QObject::connect(exchange, SIGNAL(signalQueueMessage(CMessage)), 
					 messageQ, SLOT(slotQueueMessage(CMessage)), Qt::QueuedConnection);
	
	QObject::connect(messageQ, SIGNAL(signalSend2Exchange(CMessage)), 
					 exchange, SLOT(slotIncomingMessage(CMessage)), Qt::QueuedConnection);
	
	qRegisterMetaType<CModuleList>("CModuleList");
	QObject::connect(exchange, SIGNAL(signalSendModuleList(CModuleList)), 
					 requestMapper, SLOT(slotReceiveModuleList(CModuleList)), Qt::QueuedConnection);
	
	QObject::connect(requestMapper, SIGNAL(signalReloadTasks()), 
					 exchange, SLOT(slotLoadTasks()), Qt::QueuedConnection);
//	
	__timer1 = new QTimer(0);
	__timer2 = new QTimer(0);
	QObject::connect(__timer1, SIGNAL(timeout()),
					 exchange, SLOT(slotStatusPollingEvent()), Qt::QueuedConnection);
	
	QObject::connect(__timer2, SIGNAL(timeout()),
					 exchange, SLOT(slotTasksPollingEvent()), Qt::QueuedConnection);
	
	__timer1->setInterval(QExchange::CYCLE_WIDTH);	
	__timer1->start(QExchange::CYCLE_WIDTH);
	
	__timer2->setInterval(QExchange::TASK_CYCLE_WIDTH);	
	__timer2->start(QExchange::TASK_CYCLE_WIDTH);
	
	// Move to separate threads
	messageQ->moveToThread(&queueThread);	
	exchange->moveToThread(&exchangeThread);
	radioTransmitter->moveToThread(&radioTransmitterThread);
	radioListener->moveToThread(&radioListenerThread);
	__timer1->moveToThread(&exchangeThread);			
	__timer2->moveToThread(&exchangeThread);			


	// Start threads
	radioListenerThread.start();
	radioTransmitterThread.start();	
	queueThread.start();	 	
	exchangeThread.start();	
	
	/*********************************************/		
	
	QString configFileName=Static::getConfigDir()+"/"+QCoreApplication::applicationName()+".ini";

    // Configure logging into files
    QSettings* mainLogSettings=new QSettings(configFileName,QSettings::IniFormat,app);
    mainLogSettings->beginGroup("mainLogFile");
    QSettings* debugLogSettings=new QSettings(configFileName,QSettings::IniFormat,app);
    debugLogSettings->beginGroup("debugLogFile");
    Logger* logger=new DualFileLogger(mainLogSettings,debugLogSettings,10000,app);
    logger->installMsgHandler();

	// Configure session store
    QSettings* sessionSettings=new QSettings(configFileName,QSettings::IniFormat,app);
    sessionSettings->beginGroup("sessions");
    Static::sessionStore=new HttpSessionStore(sessionSettings,app);

    // Configure static file controller
    QSettings* fileSettings=new QSettings(configFileName,QSettings::IniFormat,app);
    fileSettings->beginGroup("docroot");
    Static::staticFileController=new StaticFileController(fileSettings,app);

    // Configure and start the TCP listener
    qDebug("ServiceHelper: Starting service");
    QSettings* listenerSettings=new QSettings(configFileName,QSettings::IniFormat,app);
    listenerSettings->beginGroup("listener");
	
    HttpListener* newRequestListener = new HttpListener(listenerSettings, requestMapper, app);
	
    qDebug("ServiceHelper: Service has started");
	
}


void Startup::stop() {
    qDebug("ServiceHelper: Service has been stopped");
    // QCoreApplication destroys all objects that have been created in start().
}


Startup::Startup(int argc, char *argv[])
    : QtService<QCoreApplication>(argc, argv, APPNAME)
{
    setServiceDescription(DESCRIPTION);
    //setStartupType(AutoStartup);
}



