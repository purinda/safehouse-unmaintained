/**
  @file
  @author Stefan Frings
*/

#ifndef REQUESTMAPPER_H
#define REQUESTMAPPER_H

#include "httprequesthandler.h"
#include "CModule.h"
#include <QtCore>

/**
  The request mapper dispatches incoming HTTP requests to controller classes
  depending on the requested path.
*/

class RequestMapper : public HttpRequestHandler {
    Q_OBJECT
    Q_DISABLE_COPY(RequestMapper)
	
public:

    /**
      Constructor.
      @param parent Parent object
    */
    RequestMapper(QObject* parent=0);

    /**
      Dispatch a request to a controller.
      @param request The received HTTP request
      @param response Must be used to return the response
    */
    void service(HttpRequest& request, HttpResponse& response);
	static void appendLog(QString logmessage);
	QString getLogs(QString delimeter);
	/**
	 * Used as a web based realtime logging system. 
	 */
	static QStringList* log;

	/*
	 * Copy of modules in the system for WWW end.
	 * Data will be shared using signal slots between
	 * exchange and ui end;
	 */
	static CModuleList modules;
	static QMutex lockModules;
	static QMutex lockLogging;
	
public slots:
	void slotReceiveModuleList(CModuleList);

signals:
	void signalReloadTasks();
	
};

#endif // REQUESTMAPPER_H
