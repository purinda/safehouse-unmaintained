/**
  @file
  @author Stefan Frings
*/

#ifndef STARTUP_H
#define STARTUP_H

#include <QtCore/QCoreApplication>
#include "qtservice.h"
#include "dualfilelogger.h"
#include "httplistener.h"
#include "requestmapper.h"
#include "staticfilecontroller.h"
#include "CMessage.h"

#include "QMessageQueue.h"
#include "CMessage.h"
#include "QExchange.h"
#include "QNrf24.hpp"

/**
  Helper class to install and run the application as a windows
  service.
*/
class Startup : public QtService<QCoreApplication>
{
public:

    /** Constructor */
    Startup(int argc, char *argv[]);
	
	RequestMapper* requestMapper;
	QExchange* exchange;
	QMessageQueue* messageQ;
	QNrf24* radioTransmitter;
	QNrf24* radioListener;

protected:

    /** Start the service */
    void start();

    /**
      Stop the service gracefully.
      @warning This method is not called when the program terminates
      abnormally, e.g. after a fatal error, or when killed from outside.
    */
    void stop();
private:
	QTimer* __timer1;
	QTimer* __timer2;
};

#endif // STARTUP_H
