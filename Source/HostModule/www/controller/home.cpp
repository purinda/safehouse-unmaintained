/**
  @file
  @author Stefan Frings
*/

#include "home.h"
#include "static.h"
#include "logger.h"
#include "CModule.h"
#include "requestmapper.h"


HomeController::HomeController(){}

void HomeController::service(HttpRequest& request, HttpResponse& response)
{	
	// lock shared resources
	QMutexLocker lock(&RequestMapper::lockModules);	
	
	ctemplate::TemplateDictionary root("ROOT");
	// Where all the module specific details should be stored
	ctemplate::TemplateDictionary* modules;
	
	root.SetValue("PAGE_TITLE", "Safehouse");
	
	// set up list of modules active at a given time
	foreach (CModule val, RequestMapper::modules.items())
	{
		modules = root.AddSectionDictionary("MODULES");
		modules->SetIntValue("ID", val.getIndex());
		modules->SetValue("TYPE", "a");
		modules->SetValue("NAME", val.getName().toStdString());
	}
	
	std::string htmlBody;
	ctemplate::ExpandTemplate(Static::getHtDocsDir().toStdString() + "/index.html", ctemplate::DO_NOT_STRIP, &root, &htmlBody);

    response.setHeader("Content-Type", "text/html; charset=ISO-8859-1");
    response.write(Helper::stringToQByeArray(htmlBody), true);
}


