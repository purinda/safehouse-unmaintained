#include "tasks.h"
#include "static.h"
#include "requestmapper.h"
#include "../QExchange.h"

TasksController::TasksController(){}

void TasksController::service(HttpRequest& request, HttpResponse& response)
{
	// lock shared resources
	QMutexLocker lock(&RequestMapper::lockModules);
	
	// process POST requests
	if (request.getMethod().contains(HTTP_POST))
	{
		// post variable with new task properties
		if (!request.getParameter("task").isEmpty())
		{
			saveTask(request, response);
			return;
		}		
		
		// request to delete task
		if (!request.getParameter("delete").isEmpty())
		{
			deleteTask(request, response);
			return;
		}
	}
	
	// Page content loading
	ctemplate::TemplateDictionary root("ROOT");	
	root.SetValue("PAGE_TITLE", "Tasks");
	
	// Where all the module specific details should be stored
	ctemplate::TemplateDictionary* modules;
		
	// set up list of modules active at a given time
	foreach (CModule _module, RequestMapper::modules.items())
	{
		modules = root.AddSectionDictionary("MODULE");
		modules->SetIntValue("M_ID", _module.getIndex());
		modules->SetValue("M_TYPE", "a");
		modules->SetValue("M_NAME", _module.getName().toStdString());

		
		ctemplate::TemplateDictionary* tplability;	
		// Where all the abilities of a specific device is stored
		foreach (CAbility* _ability, _module.abilities)
		{
			tplability = modules->AddSectionDictionary("ABILITY");
			tplability->SetIntValue("A_INDEX", _ability->getIndex());
			tplability->SetIntValue("A_ID", _ability->getId());

			tplability->SetValue("A_NAME", _ability->getName().toStdString());
			tplability->SetValue("A_DESCRIPTION", _ability->getDescription().toStdString());
			tplability->SetValue("A_HTML", _ability->getComponentAsHtml().toStdString());
		}
	}
	
	// Load all tasks for viewing
	QSqlQuery __rs = (CRuleActionModel().getTasks());
	// dictionary for tasks
	ctemplate::TemplateDictionary* tasks;	
	while(__rs.next())
	{
		CTask __task;
		// Load task including its rules and actions
		__task.loadDbData(__rs.value(0).toInt());
		tasks = root.AddSectionDictionary("TASK");
		tasks->SetIntValue("TSK_ID", __task.getId());
		tasks->SetValue("NAME", __task.getName().toStdString());
		tasks->SetValue("DESCRIPTION", __task.getDescription().toStdString());
		tasks->SetValue("WHEN", __task.rulesList.getExplanation().toStdString());
		tasks->SetValue("THEN", __task.actionsList.getExplanation().toStdString());
	}
		
	std::string htmlBody;
	ctemplate::ExpandTemplate(Static::getHtDocsDir().toStdString() + "/tasks.html", ctemplate::DO_NOT_STRIP, &root, &htmlBody);

    response.setHeader("Content-Type", "text/html; charset=ISO-8859-1");
    response.write(Helper::stringToQByeArray(htmlBody), true);
}


void TasksController::deleteTask(HttpRequest& request, HttpResponse& response)
{
	bool ok = false;
	int __task_id = request.getParameter("delete").toInt(&ok, 10);
	
	CRuleActionModel().deleteTask(__task_id);
}


void TasksController::saveTask(HttpRequest& request, HttpResponse& response)
{
	// Parse jason request
	QJson::Parser parser;
	bool ok;

	//{"rules":{"0":{"type":"schedule","operator":"<","value":"07:38 PM"}},"actions":{"0":{}}}:
	QByteArray task = request.getParameter("task").toLower();
	QVariantMap result = parser.parse (task, &ok).toMap();

	// New task to store rules and actions
	CTask* __newTask = new CTask();

	// process rules
	QVariantMap rulesObj = result["rules"].toMap();
	foreach (QVariant ruleObj, rulesObj)
	{
		QMap<QString, QVariant> rule = ruleObj.toMap();
		// create the new rule
		CRule* __newRule = new CRule();

		if (!rule["operator"].isNull())
			__newRule->setCondition(rule["operator"].toString());

		if (!rule["ability"].isNull())
			__newRule->setAbility(rule["ability"].toInt());

		if (!rule["type"].isNull())
			__newRule->setType(rule["type"].toString());

		if (!rule["value"].isNull())
			__newRule->setThresholdValue(rule["value"]);			

		__newTask->addRule(*__newRule);
	}

	// process actions
	QVariantMap actionsObj = result["actions"].toMap();
	foreach (QVariant actionObj, actionsObj)
	{
		QMap<QString, QVariant> action = actionObj.toMap();
		// create the new rule
		CAction* __newAction = new CAction();

		if (!action["ability"].isNull())
			__newAction->setAbility(action["ability"].toInt());

		if (!action["type"].isNull())
			__newAction->setType(action["type"].toString());

		if (!action["value"].isNull())
			__newAction->setValue(action["value"]);			

		__newTask->addAction(*__newAction);			
	}

	// get task name if user has specified
	if (!result["name"].isNull())
		__newTask->setName(result["name"].toString());
	else
		__newTask->setName("Test");
	
	
	__newTask->setDescription("No Description");
	__newTask->save();		

	
	response.write("true", true);	
}