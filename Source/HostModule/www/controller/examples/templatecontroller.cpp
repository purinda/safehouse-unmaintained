/**
  @file
  @author Stefan Frings
*/

#include "templatecontroller.h"
#include "static.h"

TemplateController::TemplateController(){}

void TemplateController::service(HttpRequest& request, HttpResponse& response) {

    response.setHeader("Content-Type", "text/html; charset=ISO-8859-1");

    response.write("",true);
}
