/**
  @file
  @author Stefan Frings
*/

#include "viewmodule.h"
#include "static.h"
#include "requestmapper.h"

ViewModuleController::ViewModuleController(){}

void ViewModuleController::service(HttpRequest& request, HttpResponse& response)
{
	// lock shared resources
	QMutexLocker lock(&RequestMapper::lockModules);	
	
	ctemplate::TemplateDictionary module("MODULE");
	bool __success = false;
	
	// retrieve passed in parameters
	QString __id(request.getParameter(Helper::stringToQByeArray("id")).constData());
	int id = __id.toInt(&__success, 10);
	
	// if the module id doesnt exist 
	if (!RequestMapper::modules.items().contains(id))
	{
		response.redirect("http://www.example.com/");
		return;
	}
	// capture POST values if any when changing abilities
	if (request.getMethod().contains(HTTP_POST))
	{
		// Variable changes in abilities
		if (request.getParameter("id").isEmpty())
			return;
		if (request.getParameter("a_index").isEmpty())
			return;
		if (request.getParameter("value").isEmpty())
			return;
		
		id = request.getParameter("id").toInt(&__success, 10);
		int aid = request.getParameter("a_index").toInt(&__success, 10);
		float val = request.getParameter("value").toFloat(&__success);
		// Something has went wrong
		if (!__success)
		{
			response.write("false", true);
			return;
		}
		
		// Set the new value retrived from Ajax post
		RequestMapper::modules.items()[id].abilities[aid]->setValue(val, FALSE, TRUE);	
		
		response.write("true", true);
		return;
	}
	
	module.SetValue("NAME", RequestMapper::modules.items()[id].getName().toStdString());
	module.SetValue("ID", __id.toStdString());

	// min time for the graph to scale x axis start position (currently it draws an interval of one day). 
	// once the settings are implemented these has to be dynamically fed.
	QString graphMinDateTime = QDateTime::fromTime_t(QDateTime::currentDateTime().toTime_t() - 86400).toString(Qt::ISODate).replace("T", " ", Qt::CaseInsensitive);
	
	ctemplate::TemplateDictionary* ability;	
	// Where all the abilities of a specific device is stored
	foreach (CAbility* val, RequestMapper::modules.items()[id].abilities)
	{
		ability = module.AddSectionDictionary("ABILITY");
		ability->SetIntValue("ID", val->getIndex());
			
		ability->SetValue("NAME", val->getName().toStdString());
		ability->SetValue("DESCRIPTION", val->getDescription().toStdString());
		ability->SetValue("HTML", val->getComponentAsHtml().toStdString());
	}
	
	ctemplate::TemplateDictionary* abilityHistory;	
	foreach (CAbility* val, RequestMapper::modules.items()[id].abilities)
	{
		// Ignore discrete value based abilities such as switches from displaying in history
		if (val->getType() == CT_SWITCH)
			continue;
			
		abilityHistory = module.AddSectionDictionary("ABILITY_HISTORY");	
		
		abilityHistory->SetValue("NAME", val->getName().toStdString());
		abilityHistory->SetValue("name", val->getName().toLower().toStdString());
		// Plot points every 15min intervals for last 24 hours for desktop with 1hr intervals
		// Plot points every 15min intervals for last 24 hours for mobile with 3 hr intervals
		if (request.getBrowserType() == HttpRequest::TargetBrowserDesktop)
		{
			abilityHistory->SetValue("HISTORY", val->getHistory(900, 86400).toStdString());
			abilityHistory->SetValue("MIN_DATE", graphMinDateTime.toStdString());
			abilityHistory->SetValue("INTERVAL", "2 hours");
		}
		else
		{
			abilityHistory->SetValue("HISTORY", val->getHistory(3600, 86400).toStdString());
			abilityHistory->SetValue("MIN_DATE", graphMinDateTime.toStdString());
			abilityHistory->SetValue("INTERVAL", "6 hours");
		}
		
		// Page can identify switch data vs variable data
		if (val->getType() == CT_SWITCH)
		{
			abilityHistory->SetValue("DISCRETE_DATA", "true");
		}
		else
		{
			abilityHistory->SetValue("DISCRETE_DATA", "false");		
		}		
	}	
	
	
	
	std::string htmlBody;
	ctemplate::ExpandTemplate(Static::getHtDocsDir().toStdString() + "/viewModule.html", ctemplate::DO_NOT_STRIP, &module, &htmlBody);
	
    response.setHeader("Content-Type", "text/html; charset=ISO-8859-1");
    response.write(Helper::stringToQByeArray(htmlBody), true);
}
