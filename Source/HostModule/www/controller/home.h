/**
  @file
  @author Stefan Frings
*/

#ifndef HOME_H
#define HOME_H

#include "httprequest.h"
#include "httpresponse.h"
#include "httprequesthandler.h"
#include "ctemplate/template.h"
#include "helper.h"

/**
  This controller generates a website using the template engine.
  It generates a Latin1 (ISO-8859-1) encoded website from a UTF-8 encoded template file.
*/

class HomeController : public HttpRequestHandler {
    Q_OBJECT
        Q_DISABLE_COPY(HomeController);
    public:

        /** Constructor */
        HomeController();

        /** Generates the response */
        void service(HttpRequest& request, HttpResponse& response);
    };

#endif // HOME_H
