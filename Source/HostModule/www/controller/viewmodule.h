/**
  @file
  @author Stefan Frings
*/

#ifndef VIEWMODULECONTROLLER_H
#define VIEWMODULECONTROLLER_H

#include "httprequest.h"
#include "httpresponse.h"
#include "httprequesthandler.h"
#include "ctemplate/template.h"
#include "helper.h"
#include "../Constants.h"
#include "PacketTypes.h"

/**
  This controller generates a website using the template engine.
  It generates a Latin1 (ISO-8859-1) encoded website from a UTF-8 encoded template file.
*/

class ViewModuleController : public HttpRequestHandler {
    Q_OBJECT
        Q_DISABLE_COPY(ViewModuleController);
    public:

        /** Constructor */
        ViewModuleController();

        /** Generates the response */
        void service(HttpRequest& request, HttpResponse& response);
    };

#endif // VIEWMODULECONTROLLER_H
