/**
  @file
  @author Stefan Frings
*/

#ifndef TASKS_H
#define TASKS_H

#include "httprequest.h"
#include "httpresponse.h"
#include "httprequesthandler.h"
#include "ctemplate/template.h"
#include "helper.h"
#include "../Constants.h"
#include "../qjson/include/QJson/Parser"
#include "../qjson/include/QJson/QObjectHelper"
#include "../qjson/include/QJson/Serializer"
#include "../../rules/CRules.h"

/**
  This controller generates a website using the template engine.
  It generates a Latin1 (ISO-8859-1) encoded website from a UTF-8 encoded template file.
*/

class TasksController : public HttpRequestHandler {
    Q_OBJECT
        Q_DISABLE_COPY(TasksController);
    public:

        /** Constructor */
        TasksController();
		
        /** Generates the response */
        void service(HttpRequest& request, HttpResponse& response);
		void saveTask(HttpRequest& request, HttpResponse& response);
		void deleteTask(HttpRequest& request, HttpResponse& response);
    };

#endif // TASKS_H
