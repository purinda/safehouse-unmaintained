
#include "helper.h"

QByteArray Helper::stringToQByeArray(std::string in)
{
	QByteArray body(in.c_str(), in.length());
	return body;
}

std::string Helper::qstringToString(QString in)
{
	// Either this if you use UTF-8 anywhere
	return in.toUtf8().constData();
}

int Helper::getRandomInt(int limit)
{
	// Where the main process should start
	// to generate random message identifiers
	QTime time = QTime::currentTime();
	qsrand((uint)time.msec());

	return (qrand() % (limit));
}

int Helper::is_big_endian(void)
{
	union {
		uint32_t i;
		char c[4];
	} bint = {0x01020304};

	return bint.c[0] == 1; 
}	
