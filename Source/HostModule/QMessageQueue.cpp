/* 
 * File:   QMessageQueue.cpp
 * Author: purinda
 * 
 * Created on 30 June 2012, 4:12 PM
 */



#include <sys/socket.h>

#include "QMessageQueue.h"
#include "QExchange.h"

QMessageQueue::QMessageQueue()	
{

}

QMessageQueue::~QMessageQueue()
{
}

void QMessageQueue::init(QThread &qthread)
{	
	this->__threadReference = &qthread;
	
	// When thread start() called, run process()
	connect(&qthread, SIGNAL(started()),
			this, SLOT(slotProcessMessages()));
	
	// When new items get appended to the queue, run process()
	connect(this, SIGNAL(signalMessageQueueChanged()),
			this, SLOT(slotProcessMessages()));
}

void QMessageQueue::slotPendingMessage(CMessage __message)
{
	emit signalSend2Exchange(__message);
}

void QMessageQueue::slotProcessMessages()
{
	RequestMapper::appendLog("Messages in the Queue = " + QString::number(this->__message_queue_outbound.count(), 10)); 
	
	for (int i = 0; i < this->__message_queue_outbound.count(); i++)
	{
		if (!this->__message_queue_outbound[i].processed)
		{
			// where the messages are fed to NRF24L01 module
			RequestMapper::appendLog("(emit sendToRadio) Emiting message packet from Queue"); 

			// Mark message as processed
			this->__message_queue_outbound[i].processed = true;
			
			emit signalTransmit(this->__message_queue_outbound[i]);
			// problem is here, when this thread is in sleep the returning messages wont get read.
		}
	}
	// once every message is processed clear the queue.
	__message_queue_outbound.clear();
	

	// Tell others that processing message queue finished
	emit signalFinishedProcessing();
}

void QMessageQueue::slotQueueMessage(CMessage _msg)
{
	RequestMapper::appendLog( "(slot receiveMessage) Message Queue Received, CMessage from Exchange" ); 

	this->__message_queue_outbound.insert (this->__message_queue_outbound.count(), _msg);	
	emit signalMessageQueueChanged();

}
