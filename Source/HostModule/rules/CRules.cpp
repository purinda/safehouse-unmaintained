/* 
 * File:   Rules.cpp
 * Author: purinda
 * 
 * Created on 26 August 2012, 8:45 PM
 */

#include "CRules.h"
#include "requestmapper.h"

CRule::CRule()
{
	this->__rul_id = NULL;
	this->__abl_id = NULL;
	this->__tsk_id = NULL;
}

/**
 * Call this function before setting the threshold value
 * 
 * @param type
 */
void CRule::setType(QString type)
{
	if (type.compare("schedule") == 0)
	{
		this->__type = Safehouse::RuleSchedule;
	}
	else if (type.compare("ability") == 0)
	{
		this->__type = Safehouse::RuleAbility;
	}
}

void CRule::setTaskId(int tsk_id)
{
	this->__tsk_id = tsk_id;
}

void CRule::setCondition(QString condition)
{
	this->__condition = CRule::getConditionType(condition);
}

Safehouse::RuleType CRule::getType()
{
	return this->__type;
}

void CRule::setObservingValue(QVariant val)
{
	__observingValue = val;
}

void CRule::setAbility(int abl_id)
{
	__abl_id = abl_id;
}

int CRule::getAbilityId()
{
	return __abl_id;
}

/**
 * Call this function after setting the Rule type
 * 
 * @param val
 */
void CRule::setThresholdValue(QVariant val)
{
	__thresholdValue = val;
}

bool CRule::isTrue()
{
	if (__type == Safehouse::RuleSchedule)
		return this->compareDateTime();
	else
		return this->compareNumber();
	
}

bool CRule::compareNumber()
{
	bool __ret = false;
	
	switch(this->__condition)
	{
		case Safehouse::ConditionEqual:
			if (__observingValue == __thresholdValue)
				__ret = true;
			break;
		case Safehouse::ConditionGreaterThan:
			if (__observingValue.toDouble() > __thresholdValue.toDouble())
				__ret = true;
			break;
		case Safehouse::ConditionLessThan:
			if (__observingValue.toDouble() < __thresholdValue.toDouble())
				__ret = true;
			break;
		case Safehouse::ConditionGreaterThanOrEqual:
			if (__observingValue.toDouble() >= __thresholdValue.toDouble())
				__ret = true;
		case Safehouse::ConditionLessThanOrEqual:
			if (__observingValue.toDouble() <= __thresholdValue.toDouble())
				__ret = true;
	}	
	
	return __ret;
}

bool CRule::compareDateTime()
{
	bool __ret = false;
	
	switch(this->__condition)
	{
		case Safehouse::ConditionEqual:
			if (__observingValue.toTime() == __thresholdValue.toTime())
				__ret = true;
			break;
		case Safehouse::ConditionGreaterThan:
			if (__observingValue.toTime() > __thresholdValue.toTime())
				__ret = true;
			break;
		case Safehouse::ConditionLessThan:
			if (__observingValue.toTime() < __thresholdValue.toTime())
				__ret = true;
			break;
		case Safehouse::ConditionGreaterThanOrEqual:
			if (__observingValue.toTime() >= __thresholdValue.toTime())
				__ret = true;
		case Safehouse::ConditionLessThanOrEqual:
			if (__observingValue.toTime() <= __thresholdValue.toTime())
				__ret = true;
	}
	
	return __ret;
}

QString CRule::getExplanation()
{
//	std::string ability_name = __ability->getName().toStdString() + " ( " + QString::number(this->__abl_id, 10).toStdString() + ")";
	std::string ability_name = "";
	std::string condition = "";
	std::string value = "";
	
	switch (this->__condition)
	{
		case Safehouse::ConditionEqual:	
			condition = getConditionString(this->__condition).toStdString() + " to ";
			break;
		default:
			condition = getConditionString(this->__condition).toStdString() + " ";
	}
	
	
	if (this->__type == Safehouse::RuleSchedule)
	{
		ability_name = "time";
		value = this->__thresholdValue.toTime().toString(DateTimeFormats::TIME_FORMAT_DISPLAY).toStdString();
	}
	else
	{
		CAbility* __ability = new CAbility();
		__ability->loadDbData(this->__abl_id);		
		
		ability_name = __ability->getName().toStdString();
		value = this->__thresholdValue.toString().toStdString();
	}
	
	return QString::fromStdString(ability_name + " is " + condition + value);
}


/**
 * This function has to be improved to query the attribute table 
 * and set conditions.
 * 
 * @param condition
 * @return 
 */
Safehouse::ConditionType CRule::getConditionType(QString condition)
{
	if (condition.compare(">") == 0)
		return Safehouse::ConditionGreaterThan;
	
	if (condition.compare("<") == 0)
		return Safehouse::ConditionLessThan;
	
	if (condition.compare("=") == 0)
		return Safehouse::ConditionEqual;
	
	if (condition.compare(">=") == 0)
		return Safehouse::ConditionGreaterThanOrEqual;
	
	if (condition.compare("<=") == 0)
		return Safehouse::ConditionLessThanOrEqual;
	
	return Safehouse::ConditionEqual;
}

/**
 * Get rule condition as a string, used for translation purposes.
 * 
 * @param condition
 * @return 
 */
QString CRule::getConditionString(Safehouse::ConditionType condition)
{
	switch (condition)
	{
		case Safehouse::ConditionEqual:
			return "equal";
			break;
		case Safehouse::ConditionGreaterThan:
			return "greater than";
			break;
		case Safehouse::ConditionGreaterThanOrEqual:
			return "greater than or equal";
			break;
		case Safehouse::ConditionLessThan:
			return "less than";
			break;
		case Safehouse::ConditionLessThanOrEqual:
			return "less than or equal";
			break;
	}
}



/**
 * This function has to be improved to query the attribute table 
 * and set conditions.
 * 
 * @param condition
 * @return 
 */
Safehouse::RuleType CRule::getRuleType(QString type)
{
	if (type.compare("schedule") == 0)
	{
		return Safehouse::RuleSchedule;
	}
	else if (type.compare("ability") == 0)
	{
		return Safehouse::RuleAbility;
	}
	
}


int CRule::save()
{
	this->__rul_id = CRuleActionModel().saveRule(this->__rul_id, this->__abl_id, 
											 this->__condition, this->__tsk_id,
											 this->__type, this->__thresholdValue);
	
	return this->__rul_id;
}

/******************************************************************************
 *******************************************************************************/

CRules::CRules()
{
	
}


void CRules::addRule(CRule rule)
{
	rules.insert(rules.count(), rule);
}


bool CRules::isTrue()
{
	bool __return = true;
	
	foreach(CRule rule, rules)
	{
//		// set observing value
//		if (rule.getType() == Safehouse::RuleAbility)
//			rule.setObservingValue(QExchange::getAbilityValue(rule.getAbilityId()));
//		else
//			rule.setObservingValue(QTime::currentTime());
//		
		// if one of the conditions are not true the whole
		// rule (chained conditions) should fail
		if (!rule.isTrue())
			__return = false;
	}
	
	return __return;
}

QString CRules::getExplanation()
{
	QString _expl = "";
	QString _concat = " and <br />";
	int i = 0;
	foreach(CRule _r, rules)
	{
		_expl += _r.getExplanation();
		if (i++ < rules.count()-1)
			_expl += _concat;
	}
	
	return _expl;
}

void CRules::loadDbData(int __tsk_id)
{	
	QSqlQuery results = CRuleActionModel().getRules(__tsk_id);
	
	while(results.next())
	{
//		RequestMapper::appendLog("RULE DEBUG");	
		CRule* __newRule = new CRule();
		__newRule->setCondition(results.value(2).toString());
		__newRule->setAbility(results.value(1).toInt());
		__newRule->setType(results.value(0).toString());
		
		// check whether rule is schedule based or value based
		if (results.value(0).toString() == RuleTypes::RULE_TYPE_SCHEDULE)
		{
			__newRule->setThresholdValue(QTime::fromString(results.value(3).toString(), DateTimeFormats::TIME_FORMAT_DATABASE));
//			RequestMapper::appendLog("Time value = " + results.value(3).toString()); 	
//			RequestMapper::appendLog("Time value = " + QVariant(QTime::fromString(results.value(3).toString(), DateTimeFormats::TIME_FORMAT_DATABASE)).toString()); 	
		}
		else
		{
			__newRule->setThresholdValue(results.value(3));
		}
		this->addRule(*__newRule);	
	}
	
}


/*****************************************************************************
 ************************Action Implementation*******************************/

Safehouse::ActionType CAction::getActionType(QString type)
{
	if (type.compare("sms", Qt::CaseInsensitive) == 0)
		return Safehouse::ActionSMS;
	
	if (type.compare("tweet", Qt::CaseInsensitive) == 0)
		return Safehouse::ActionTweet;
	
	if (type.compare("email", Qt::CaseInsensitive) == 0)
		return Safehouse::ActionEmail;
	
	if (type.compare("ability", Qt::CaseInsensitive) == 0)
		return Safehouse::ActionAbility;
	
	return Safehouse::ActionAbility;
}


CAction::CAction()
{
	this->__act_id = NULL;
	this->__abl_id = NULL;
	this->__tsk_id = NULL;	
}

void CAction::setAbility(int ability_id)
{
	this->__abl_id = ability_id;
}

int CAction::getAbility()
{
	return this->__abl_id;
}

QVariant CAction::getValue()
{
	return this->__value;
}

void CAction::setValue(QVariant val)
{
	this->__value = val;
}

void CAction::setType(QString type)
{
	this->__type = CAction::getActionType(type);
}


void CAction::setTaskId(int tsk_id)
{
	this->__tsk_id = tsk_id;
}

QString CAction::getExplanation()
{
	CAbility* __ability = new CAbility();
	__ability->loadDbData(this->__abl_id);

	std::string ability_name = "";
	std::string value = "";
	
	switch (this->__type)
	{
		case Safehouse::ActionTweet:
			ability_name = " send an Tweet saying ";
			value = "'" + this->__value.toString().toStdString() + "'";
			break;
		case Safehouse::ActionSMS:
			ability_name = " send an SMS saying ";
			value = "'" + this->__value.toString().toStdString() + "'";
			break;
		case Safehouse::ActionEmail:
			ability_name = " send an EMail saying ";
			value = "'" + this->__value.toString().toStdString() + "'";
			break;
		case Safehouse::ActionAbility:
			ability_name = __ability->getName().toStdString() + " will be set to ";
			value = this->__value.toString().toStdString();
			break;
	}
	
	
	return QString::fromStdString(ability_name + value);		
}

int CAction::save()
{
	this->__act_id = CRuleActionModel().saveAction(this->__act_id, this->__abl_id, 
												this->__tsk_id, this->__type,
												this->__value);
	
	return this->__act_id;
}


/*****************************************************************************
 ************************Actions Implementation*******************************/


CActions::CActions()
{
	
}

void CActions::addAction(CAction action)
{
	this->actions.insert(this->actions.count(), action);
}


void CActions::loadDbData(int __tsk_id)
{	
	QSqlQuery results = CRuleActionModel().getActions(__tsk_id);
	
	while(results.next())
	{
		CAction* __newAction = new CAction;
		__newAction->setType(results.value(0).toString());
		__newAction->setAbility(results.value(1).toInt());
		__newAction->setValue(results.value(2));

		this->addAction(*__newAction);
	}
	
//	RequestMapper::appendLog( QString::fromStdString("Task id for Actions: ") + QString::number( __tsk_id, 10)) ;
}

QString CActions::getExplanation()
{
	QString _expl = "";
	QString _concat = " and <br />";
	int i = 0;
	
	foreach(CAction _a, this->actions)
	{
		_expl += _a.getExplanation();
		if (i++ < this->actions.count()-1)
			_expl += _concat;
	}
	
	return _expl;
}
	

/**********************Tasks Class***************************/

CTask::CTask()
{
	this->__tsk_id = NULL;
}

void CTask::addAction(CAction action)
{
	actionsList.addAction(action);
}

void CTask::addRule(CRule rule)
{
	rulesList.addRule(rule);
}


int CTask::getId()
{
	return this->__tsk_id;
}

QString CTask::getName()
{
	return this->__name;
}

QString CTask::getDescription()
{
	return this->__description;
}

void CTask::setName(QString name)
{
	this->__name = name;
}

void CTask::setDescription(QString des)
{
	this->__description = des;
}

void CTask::loadDbData(int tsk_id)
{
	QSqlQuery __rs = CRuleActionModel().getTask(tsk_id);
	
	while(__rs.next())
	{
		this->__name = __rs.value(0).toString();
		this->__description = __rs.value(1).toString();
	}

	rulesList.loadDbData(tsk_id);		
	actionsList.loadDbData(tsk_id);		
	
	this->__tsk_id = tsk_id;
}

void CTask::process()
{
	/**
	 * Following code block traverse through modules and their abilities to retrive
	 * ability values and set them as threshold values.
     */
	for (int z=0; z < rulesList.rules.count(); z++)
	{
		if (rulesList.rules[z].getType() == Safehouse::RuleAbility)
		{
			// find the threshold value
			for (int i=0; i < this->__moduleList.items().count(); i++)
			{
				for (int j=0; j < this->__moduleList.items()[i].abilities.count(); j++)
				{
					if (this->__moduleList.items()[i].abilities[j]->getId() == rulesList.rules[z].getAbilityId())
					{
						QVariant __tv (this->__moduleList.items()[i].abilities[j]->getValue());
						this->rulesList.rules[z].setObservingValue(__tv);
						RequestMapper::appendLog("Observing value = " + __tv.toString()); 
	//					break;
					}
				}
			}
		}
		else if (rulesList.rules[z].getType() == Safehouse::RuleSchedule)
		{
			this->rulesList.rules[z].setObservingValue(QTime::currentTime());
		}
	}
	
	// Rules demo code
	if (rulesList.isTrue())
	{
		foreach (CAction __a, this->actionsList.actions)
		{
			CAbilityData tmp;
			tmp.ability_id = __a.getAbility();
			tmp.emitSignal = TRUE;
			tmp.isLogged = FALSE;
			tmp.value = __a.getValue().toFloat();
			emit signalTriggerAction(tmp);
		}

		RequestMapper::appendLog("Rule: is Triggered"); 
	}
	else
		RequestMapper::appendLog("Rule: is NOT Triggered"); 
}

void CTask::setModuleList(CModuleList __ml)
{
	this->__moduleList = __ml;
}

void CTask::save()
{
	this->__tsk_id = CRuleActionModel().saveTask(this->__tsk_id, this->__name, this->__description);
	
	// save rules
	foreach (CRule __r, this->rulesList.rules)
	{
		__r.setTaskId(this->__tsk_id);
		__r.save();
	}

	// save actions
	foreach (CAction __a, this->actionsList.actions)
	{
		__a.setTaskId(this->__tsk_id);
		__a.save();
	}
		
}