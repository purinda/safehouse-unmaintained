/* 
 * File:   Rules.h
 * Author: purinda
 *
 * Created on 26 August 2012, 8:45 PM
 */

#ifndef CRULES_H
#define	CRULES_H

#include <QHash>
#include <QSqlQuery>
#include "../dbdriver/CRuleActionModel.h"
#include "../Constants.h"
#include "../CMessage.h"

class CRule
{
public:
	CRule();

	void setObservingValue(QVariant);
	void setThresholdValue(QVariant);
	void setType(QString type);
	void setCondition(QString condition);
	Safehouse::RuleType getType();
	void setAbility(int);
	int getAbilityId();
	void setTaskId(int);
	bool isTrue();
	QString getExplanation();
	int save();
	
	// Static functions
	static Safehouse::ConditionType getConditionType(QString condition);
	static QString getConditionString(Safehouse::ConditionType condition);
	static Safehouse::RuleType getRuleType(QString type);
	
private:
	int __rul_id;
	int __abl_id;
	int __tsk_id;
	Safehouse::RuleType __type;
	QVariant __observingValue;
	QVariant __thresholdValue;
	Safehouse::ConditionType __condition;

	bool compareDateTime();
	bool compareNumber();
};


class CAction
{
public:
	CAction();
	void setAbility(int);
	int getAbility();
	void setValue(QVariant);
	QVariant getValue();
	void setType(QString type);	
	QString getExplanation();
	void setTaskId(int);	
	int save();

	// Static functions
	Safehouse::ActionType getActionType(QString type);
	
private:	
	int __act_id;		
	int __abl_id;
	int __tsk_id;
	Safehouse::ActionType __type;
	QVariant __value;	
};


class CRules
{
public:
	CRules();
	void addRule(CRule);
	bool isTrue();
	void loadDbData(int);
	QString getExplanation();
	
	QHash<int, CRule> rules;
};

class CActions
{
public:
	CActions();
	void loadDbData(int);
	void addAction(CAction);
	QString getExplanation();
	
	QHash<int, CAction> actions;		
};


/**********************Actions and Rules Group Class***************************/

class CTask: public QObject{
    Q_OBJECT
public:
	CTask();
	void loadDbData(int);
	void addRule(CRule);
	void addAction(CAction);
	void process();
	void save();
	
	int getId();
	QString getName();
	QString getDescription();
	void setName(QString);
	void setDescription(QString);	

	void setModuleList(CModuleList __ml);

	// public vars
	CRules rulesList;
	CActions actionsList;	
	
signals:
	void signalTriggerAction(CAbilityData);

private:
	int __tsk_id;
	QString __name;
	QString __description;	
	
	CModuleList __moduleList;
};




#endif	/* CRULES_H */

