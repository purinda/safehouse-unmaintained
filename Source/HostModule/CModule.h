/* 
 * File:   QModule.h
 * Author: purinda
 *
 * Created on 15 July 2012, 12:02 AM
 */

#ifndef CMODULE_H
#define	CMODULE_H

#include <QtCore>
#include <QtDebug>
#include <inttypes.h>
#include "helper.h"
#include "CAbility.h"

/**
 * Explain this class
 * @param 
 */
class CModule {
	
public:
	CModule();
	
	void setDescription(QString);
	QString getDescription();

	void setName(QString);
	QString getName();
	
	void setIndex(int index);
	int getIndex();
	
	void setAddress(QByteArray );
	QByteArray getAdress();
	
	void setType(int type);
	int getType();
	
//	void setHash();
//	QByteArray getHash();
		
	void loadAbilities();
	
	virtual ~CModule();
	
	QMap<int, CAbility*> abilities;	
	
private:
	// unique index given at initialization time
	int index;
	
	// physical address of the module
	QByteArray __address;
	// logically mapped address stored in app/db
	int __type;
	QString __name;
	QString __description;

	QByteArray __abilities_hash;
};

/**
 * Explain this class
 * @param 
 */
class CModuleList{
public:
	void append(CModule);
	void remove(int index);
	void setItems(QHash<int, CModule>);
	QHash<int, CModule> items();
private:
	QHash<int, CModule>	modules;
};


#endif	/* CMODULE_H */

