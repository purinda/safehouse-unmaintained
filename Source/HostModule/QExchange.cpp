/* 
 * File:   QExchange.cpp
 * Author: purinda
 * 
 * Created on 1 July 2012, 10:53 PM
 */

#include "QExchange.h"
#include "dbdriver/CModuleModel.h"

QExchange::QExchange()
{
	// try to load data
	loadFromDatabase();
}

QExchange::~QExchange()
{
}

void QExchange::init(QThread &qthread)
{
	this->__threadReference = &qthread;
	
	// When thread start() called, run process()
	connect(&qthread, SIGNAL(started()),
			this, SLOT(slotStatusPollingEvent()));
	
	incrementCycle(true);
}

void QExchange::loadFromDatabase()
{
	// Load modules from Database
	modules = CModuleModel::getModules().items();

	qRegisterMetaType<CAbilityData>("CAbilityData");
//	Connect ability change (setValue) events to process() function
	foreach (CModule module, modules)
	{
		foreach (CAbility* ability, module.abilities)
		{
			connect(modules[module.getIndex()].abilities[ability->getIndex()], SIGNAL(signalAbilityChanged(CAbilityData)),
					this, SLOT(slotSendAbility2Queue(CAbilityData)));
		}
	}

	slotLoadTasks();
	
//	 Keep trac of date times last queried the database
	__lastUpdated = QDateTime::currentDateTime();

	// Set modules to web ui
	CModuleList __moduleList;
	__moduleList.setItems(this->modules);	
	emit signalSendModuleList(__moduleList);
}

void QExchange::slotLoadTasks()
{
	__tasks.clear();

	// Set modules to web ui
	CModuleList __moduleList;
	__moduleList.setItems(this->modules);

	// Load rules & actions
	QSqlQuery __rs = CRuleActionModel().getTasks();

	while(__rs.next())
	{
		CTask* __task = new CTask();
		// pass in Rule group id and Action group id
		__task->loadDbData(__rs.value(0).toInt());
		// set modulelist to tasks
		__task->setModuleList(__moduleList);		
		__tasks.append(__task);
//
		disconnect(__task, SIGNAL(signalTriggerAction(CAbilityData)),
				this, SLOT(slotSetAbility(CAbilityData)));
//		
		connect(__task, SIGNAL(signalTriggerAction(CAbilityData)),
				this, SLOT(slotSetAbility(CAbilityData)));			
	}
	
	RequestMapper::appendLog("Reload tasks");
}

QVariant QExchange::getAbilityValue(int __ability_id)
{
	
	foreach (CModule __module, this->modules)
	{
		foreach (CAbility *__ability, __module.abilities)
		{
			if (__ability->getId() == __ability_id)
			{
				return QVariant(__ability->getValue());
				break;
			}
		}
	}	
}


void QExchange::slotSetAbility(CAbilityData __data)
{	
	RequestMapper::appendLog("Ability Triggered");
	
	foreach (CModule __module, this->modules)
	{
		foreach (CAbility *__ability, __module.abilities)
		{
			if (__ability->getId() == __data.ability_id)
			{
				this->modules[__module.getIndex()].abilities[__ability->getIndex()]->setValue(__data.value, __data.isLogged, __data.emitSignal);
				break;
			}
		}
	}	

}

void QExchange::slotSendAbility2Queue(CAbilityData __data)
{
	// Do nothing
	if (this->modules.empty())
		return;

	RequestMapper::appendLog("Ability process = " + QString::number(__data.ability_id, 10));	
	// Where the main process should start
	// to generate random message identifiers
	QTime time = QTime::currentTime();
	qsrand((uint)time.msec());
	
	// Save hashes for next iteration (validation)
	// to figure out which modules got changed.
	// SetHash is called in Page Controllers
	foreach(CModule module, this->modules)
	{
//		RequestMapper::appendLog("(Module hash) Module=" + QString::number(module.getIndex(), 10)+ ", Hash=" + QString(module.getHash()));

		// Loop through abilities in the module
		foreach(CAbility* ability, module.abilities)
		{
			if (ability->getType() >= 10)
				continue;
			
			if (ability->getId() != __data.ability_id)
				continue;

			CMessage __msg;		
			// get a random packet identifier larger than 10000
			__msg.id = (qrand() % (32768));
			__msg.to = module.getAdress();
			// always command packets? 
			__msg.packetType = PT_COMMAND_PACKET;
			__msg.commandType = ability->getType();
			__msg.value = ability->getValue();
			__msg.processed = false;
			
			// send to the outbound queue
			emit (signalQueueMessage(__msg));
			RequestMapper::appendLog("(emit queueMessage) Change ability " + QString::number(ability->getId()));				
			break;
		}			

	}
	
}

/*
 * TODO: Looks like its possible to take out the hash matching mechanism as 
 * signal slot functionality works from ability library.
 */
void QExchange::slotSendStatReq2Queue()
{
	// Do nothing
	if (this->modules.empty())
		return;

	// Where the main process should start
	// to generate random message identifiers
	QTime time = QTime::currentTime();
	qsrand((uint)time.msec());
	
	// Save hashes for next iteration (validation)
	// to figure out which modules got changed.
	// SetHash is called in Page Controllers
	foreach(CModule module, this->modules)
	{
		// Loop through abilities in the module
		foreach(CAbility* ability, module.abilities)
		{
			if (ability->getType() < 10)
				continue;

			CMessage __msg;
		
			// get a random packet identifier larger than 10000
			__msg.id = (qrand() % (32768));
			__msg.to = module.getAdress();
			// always command packets? 
			__msg.packetType = PT_COMMAND_PACKET;
			__msg.commandType = ability->getType();
			__msg.value = ability->getValue();
			__msg.processed = false;

			// send to the outbound queue
			emit (signalQueueMessage(__msg));

			RequestMapper::appendLog("(emit queueMessage) Status change request");				
		}			

	}
	
}

void QExchange::slotIncomingMessage(CMessage __msg)
{
	bool modulesChanged = false;
	
	switch(__msg.packetType)
	{
		case PT_COMMAND_STATUS_PACKET:
		{
			RequestMapper::appendLog(QString("(slot getPendingPacket) Packet received to MessageQueue, ID=" + 
									QString::number(__msg.id, 10) + 
									", pt=" + QString::number(__msg.packetType, 10) +
									", ct=" + QString::number(__msg.commandType, 10) +
									", value=" + QString::number(__msg.value, 'g', 2)));			

			// Set values of completed messages back to module abilities
			foreach(CModule module, this->modules)
			{
				if (__msg.to != module.getAdress())
					continue;

				RequestMapper::appendLog("Processing Module ID = " + QString(__msg.to));
			
				// Loop through abilities in the module if there are no notifications
				foreach(CAbility* ability, module.abilities)
				{
					// Only changeable abilities like switch, variable_switch will be posted to the queue
					if (ability->getType() != __msg.commandType)
						continue;

					this->modules[module.getIndex()].abilities[ability->getIndex()]->setValue(__msg.value, TRUE);
					modulesChanged = true;
				}
			}
		}
	}
	
	
//	If module ability has been changed then copy changed values to WWW interface
	if (modulesChanged)
	{
		// Send modules to web ui
		CModuleList __moduleList;
		__moduleList.setItems(this->modules);
		
		// if motion is detected run tasks (they are higher priority)
		// (not sure whether this is the best way to implement instant notifications)
		// Refer to ticket 25
		if (__msg.commandType == CT_PRESENCE)
			slotTasksPollingEvent();
		
		emit signalSendModuleList(__moduleList);
	}
	
}


void QExchange::slotProcessTasks()
{
	// Send modules to web ui
	CModuleList __moduleList;
	__moduleList.setItems(this->modules);	
	
	// Run tasks
	foreach (CTask* __task, this->__tasks)
	{
		__task->setModuleList(__moduleList);						
		__task->process();
	}
}

/**
 * All the tasks that needs to be scheduled should go here.
 *  - Currently timeoutTrigger() is connected to a 30sec interval timer.
 */
void QExchange::slotStatusPollingEvent()
{
	incrementCycle(false);	
	RequestMapper::appendLog("Status Timer in Exchange running..."); 

	// Poll sensor board at defined intervals
	slotSendStatReq2Queue();
}

void QExchange::slotTasksPollingEvent()
{
	QMutexLocker lock(&lockTasks);
	RequestMapper::appendLog("Tasks Timer in Exchange running..."); 
	
	// Process rules after getting status changes (it make sense does it?).
	slotProcessTasks();	
}

void QExchange::incrementCycle(bool reset = false)
{
	if (reset)
		__elapsedCycles = 0;
	
	__elapsedCycles++;
	
}