/* 
 * File:   CMessage.h
 * Author: purinda
 *
 * Created on 1 July 2012, 12:11 AM
 */

#ifndef CMESSAGE_H
#define	CMESSAGE_H

#include <inttypes.h>
#include "PacketTypes.h"
#include "Constants.h"

class CMessage
{
public:
	CMessage();
	virtual ~CMessage();

	QByteArray to;
//	int abl_id;
	uint16_t id;
	uint8_t packetType;
	uint8_t commandType;
	float value;
	bool processed;
	
private:

};


class CAbilityData
{
public:
	CAbilityData();
	virtual ~CAbilityData();

	int ability_id;
	double value;
	bool isLogged;
	bool emitSignal;
	bool ackRequired;
	
private:

};



#endif	/* CMESSAGE_H */

