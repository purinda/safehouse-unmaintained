/* 
 * File:   QModule.cpp
 * Author: purinda
 * 
 * Created on 15 July 2012, 12:02 AM
 */

#include "CModule.h"
#include "QExchange.h"
#include "dbdriver/CAbilityModel.h"
#include "controller/tasks.h"

CModule::CModule()
{

}

void CModule::loadAbilities()
{
	this->abilities = CAbilityModel::getAbilities(__address);	
}

void CModule::setDescription(QString description)
{
	this->__description = description;
}

QString CModule::getDescription()
{
	return this->__description;
}

void CModule::setName(QString name)
{
	this->__name = name;
}


QString CModule::getName()
{
	return this->__name;
}

int CModule::getIndex()
{
	return this->index;
}

void CModule::setIndex(int i)
{
	index = i;
}

void CModule::setType(int type)
{
	this->__type = type;
}


int CModule::getType()
{
	return this->__type;
}

//void CModule::setHash()
//{
//	QCryptographicHash hash(QCryptographicHash::Sha1);
//	
//	foreach(CAbility* ability, abilities.values())
//	{
//		QByteArray tmp;
//		tmp.setNum(ability->getValue(), 'g', 0);
//		hash.addData(tmp);
//	}
//	__abilities_hash = hash.result();
//}
//
//QByteArray CModule::getHash()
//{
//	return __abilities_hash;
//}
//

void CModule::setAddress(QByteArray address)
{
	__address = address;
}

QByteArray CModule::getAdress()
{
	return __address;
}


CModule::~CModule()
{
}


/*******************************************************************************
 *********************************CModuleList***********************************
 *******************************************************************************/
void CModuleList::append(CModule __item)
{
	this->modules.insert(this->modules.count(), __item);
}

void CModuleList::remove(int index)
{
	this->modules.remove(index);
}

QHash<int, CModule> CModuleList::items()
{
	return this->modules;
}

void CModuleList::setItems(QHash<int, CModule> __items)
{
	this->modules = __items;
}
