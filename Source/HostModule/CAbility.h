/* 
 * File:   CAbility.h
 * Author: purinda
 *
 * Created on 17 July 2012, 6:22 PM
 */

#ifndef CABILITY_H
#define	CABILITY_H

#include <QtCore>
#include <inttypes.h>
#include <QString>

#include "CMessage.h"

class CAbility : public QObject{
    Q_OBJECT
	
public:
	enum HistoryDataRanges{
		Day,
		Week,
		Month
	};
	
	CAbility();
	virtual ~CAbility();

	void loadDbData(int abl_id);
	
	void setIndex(int);
	int getIndex();

	void setId(int);
	int getId();
	
	void setType(uint8_t);
	uint8_t getType();
	
	float getValue();
	void setValue(float, bool log = false, bool emitSignal = FALSE);
	
	QString getDescription();
	void setDescription(QString);
	
	void setName(QString __val);
	QString getName();
	
	QString getComponentAsHtml();
	QString getHistory(int, int);
	
	// Static functions
	
signals:
	void signalAbilityChanged(CAbilityData);
		
private:
	int __index;										// used for in system referencing
	int __abl_id;										// used in database for uniquely identify records
	
	uint8_t __abilityType;
	float __value;
	QString __unit;
	QString __html;
	QString __name;
	QString __description;

	static const QString CONTROL_FLIP_SWITCH_ON;
	static const QString CONTROL_FLIP_SWITCH_OFF;
};

#endif	/* CABILITY_H */

