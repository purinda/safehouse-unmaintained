/*
 * File:   main.cpp
 * Author: purinda
 *
 * Created on 1 July 2012, 1:18 AM
 */

#include <QtCore>
#include <qdebug.h>

#include "QMessageQueue.h"
#include "CMessage.h"
#include "QExchange.h"
#include "QNrf24.hpp"
#include "www/startup.h"

int main(int argc, char *argv[])
{	
	// www service

	Startup startup(argc, argv);
	return startup.exec();
}