/* 
 * File:   CAbility.cpp
 * Author: purinda
 * 
 * Created on 17 July 2012, 6:22 PM
 */

#include <stdint.h>
#include "CAbility.h"
#include "PacketTypes.h"
#include "helper.h"
#include "QExchange.h"
#include "dbdriver/CAbilityModel.h"
												  
const QString CAbility::CONTROL_FLIP_SWITCH_OFF = "<select name=\"slider\" id=\"ability-control\" data-role=\"slider\" data-mini=\"true\"><option selected=\"selected\" value=\"off\">Off</option><option value=\"on\">On</option></select>";
const QString CAbility::CONTROL_FLIP_SWITCH_ON = "<select name=\"slider\" id=\"ability-control\" data-role=\"slider\" data-mini=\"true\"><option value=\"off\">Off</option><option selected=\"selected\" value=\"on\">On</option></select>";

CAbility::CAbility()
{
	__value = 0;
	__html = "";
	__name = "";
	__abilityType = 0;
	__description = "";
}

void CAbility::setIndex(int i)
{
	__index = i;
}

int CAbility::getIndex()
{
	return this->__index;
}

void CAbility::setId(int i)
{
	__abl_id = i;
	// get last recorded value
	this->__value = CAbilityModel::getLastValue(this->__abl_id);
}

int CAbility::getId()
{

    return this->__abl_id;
}

void CAbility::loadDbData(int abl_id)
{
	QSqlQuery query = CAbilityModel::getAbility(abl_id);
	
	while (query.next()) 
	{	
		this->setId(query.value(0).toInt());
		this->setType(query.value(1).toInt());
		this->setName(query.value(2).toString());
		this->setDescription(query.value(3).toString());
	}	
}

void CAbility::setType(uint8_t __ability)
{
	__abilityType = __ability;
	
	/*
	 * Set default 
	 *	- html
	 *  - name
	 *  - description values
	 * these can be overwritten by calling setName, set.. functions.
	 */
	switch (__ability)
	{
		case CT_HUMIDITY:
		{
			__unit = " rh";
			__name = "Humidity";
			__description = "Humidity recorded within last 30 seconds";
			break;
		}
		case CT_TEMPERATURE:
		{
			__unit = " C";
			__name = "Temperature";
			__description = "Temperature recorded within last 30 seconds";
			break;
		}
		case CT_CURRENT:
		{
			__unit = " A";
			__name = "Current Usage";
			__description = "Average current consumption";
			break;
		}	
		case CT_SWITCH:
		{
			__html = "<select name=\"slider\" id=\"ability-control\" data-role=\"slider\"><option value=\"off\">Off</option><option selected=\"selected\" value=\"on\">On</option></select>";
			__name = "Switch";
			__description = "Click on the flip button to switch on/off";
			break;
		}
		case CT_PRESENCE:
		{
			__html = " Motion";
			__name = "Motion";
			__description = "PIR sensor status";
			break;
		}
		default:
			__html = "";
			__unit = "";
			__name = "No ability specified";
			__description = "No description";
	}	
}

uint8_t CAbility::getType()
{
	return __abilityType;
}

float CAbility::getValue()
{
	return __value;
}

QString CAbility::getHistory(int interval, int since)
{
	QString __return;

	if (__abilityType == CT_SWITCH)
	{
		QMap<uint, int> __result = CAbilityModel::getHistoryForDiscrete(this->__abl_id, since);
		
		__return.append("[");
		for(int i=0; i<__result.count(); i++) 
		{
			QString strRepresentation = (__result.values()[i] >= 1)?"'On'": "'Off'";
			// we don't want to show off state in the chart as its intutive to see the times where it was on.
			// and when current usage is plotted we can it only under the curve where the device was on
			QString value  = (__result.values()[i] > 0)?QString::number(__result.values()[i], 10): "null";
			
			__return.append("[");
			__return.append("'" + QDateTime::fromTime_t(__result.keys()[i]).toString(DateTimeFormats::DATETIME_FORMAT_JQPLOT) + "', " + value + ", " + strRepresentation);
			__return.append("]");

			// dont terminate the json with an comma at the end 
			// [['Cup Holder Pinion Bob', 7], ['Generic Fog Lamp', 9],]
			if (i != __result.count()-1)
				__return.append(",");
		}
		__return.append("]");
		
		return __return;
	}
	
	// Rest of the abilities which are polled periodically can be plotted using 
	// avg values.
	
	QMap<uint, float> __result = CAbilityModel::getHistoryForVariable(this->__abl_id, interval, since);
	
	__return.append("[");
	for(int i=0; i<__result.count(); i++) 
	{
		__return.append("[");
		__return.append("'" + QDateTime::fromTime_t(__result.keys()[i]).toString(DateTimeFormats::DATETIME_FORMAT_JQPLOT) + "', " + QString::number(__result.values()[i], 'g', 2));
		__return.append("]");
	
		// dont terminate the json with an comma at the end 
		// [['Cup Holder Pinion Bob', 7], ['Generic Fog Lamp', 9],]
		if (i != __result.count()-1)
			__return.append(",");
	}
	__return.append("]");
	
	
	return __return;
}


void CAbility::setValue(float __val, bool log, bool emitSignal)
{
	__value = __val;
	
	// Log value changes in the database
	if (log)
		CAbilityModel::saveValue(this->__abl_id, __val);
	
	// tell all listeners that the ability is changed, send the id.
	if (emitSignal)
	{
		CAbilityData __signallingData;
		__signallingData.ability_id = this->getId();
		__signallingData.value = this->getValue();
		emit (this->signalAbilityChanged(__signallingData));
	}
}


QString CAbility::getComponentAsHtml()
{
	if (__abilityType == CT_SWITCH && __value > 0)
		__html = CONTROL_FLIP_SWITCH_ON;
	
	if (__abilityType == CT_SWITCH && __value <= 0)
		__html = CONTROL_FLIP_SWITCH_OFF;
	
	if (__abilityType == CT_HUMIDITY || 
		__abilityType == CT_TEMPERATURE  || 
		__abilityType == CT_CURRENT ||
		__abilityType == CT_PRESENCE)
	{
		if (__value < 0)
			__value = 0;
		
		__html.setNum(__value, 'g', 2);
		__html += __unit;
	}

	
	return __html;
}


void CAbility::setName(QString __val)
{
	__name = __val;
}


QString CAbility::getName()
{
	return __name;
}

void CAbility::setDescription(QString description)
{
	__description = description;
}

QString CAbility::getDescription()
{
	return __description;
}

CAbility::~CAbility()
{
}

