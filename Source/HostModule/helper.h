/* 
 * File:   helper.h
 * Author: purinda
 *
 * Created on 8 July 2012, 9:46 PM
 */

#ifndef HELPER_H
#define	HELPER_H

#include <qbytearray.h>
#include <stdint.h>
#include <qstring.h>
#include <QTime>

class Helper{
	public:
		static QByteArray stringToQByeArray(std::string in);
		static std::string qstringToString(QString in);
		static int getRandomInt(int limit=32768);
		static int is_big_endian();
};
	
#endif	/* HELPER_H */

