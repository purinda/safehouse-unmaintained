/* 
 * File:   config.hpp
 * Author: purinda
 *
 * Created on 6 May 2012, 12:06 AM
 */

#ifndef CONFIG_HPP
#define	CONFIG_HPP

#include <inttypes.h>
#include <../Common/PacketTypes.h>

/**********************************************
 ************ Configure I/O Pins **************
 **********************************************/

/*
 * Client 1 (Freetronics Eleven) Configuration
 */
const uint8_t CONFIG_CMTYPE = DT_APP_CTRL_DHT;
// Host & Client Addresses
#define CONFIG_CLIENT_ADDR	"test1";
#define CONFIG_HOST_ADDR	"serv1";

// NRF24L01 SPI Driver Configuration
static const uint8_t CONFIG_NRF_CE = 4;
static const uint8_t CONFIG_NRF_CSN = 3;

// Module specific I/O pins
const uint8_t CONFIG_PIN_DEVICE1 = 9; // PWM
const uint8_t CONFIG_STATUS_LED  = 8;
const uint8_t CONFIG_PIR_PIN  = 7;
const uint8_t CONFIG_DHTPIN = 2; // Di.2
// Every 30 seconds
const unsigned long CONFIG_BLINK_FREQ = 2;

/*
 * Client 2 (Very first brown board) Configuration
 */
//const uint8_t CONFIG_CMTYPE = DT_APP_CTRL_DHT;
//
//// Host & Client Addresses
//#define CONFIG_CLIENT_ADDR	"clie2";
//#define CONFIG_HOST_ADDR	"serv1";
//// NRF24L01 SPI Driver Configuration
//static const uint8_t CONFIG_NRF_CE = 1;
//static const uint8_t CONFIG_NRF_CSN = 0;
//
//// Module specific I/O pins
//const uint8_t CONFIG_PIN_DEVICE1 = 9; // PWM
//const uint8_t CONFIG_STATUS_LED  = 7;
//const uint8_t CONFIG_DHTPIN = 4; // Di.2
//// Every 30 seconds
//const unsigned long CONFIG_BLINK_FREQ = 2;

/*
 * Client 5 (very first pro board) Configuration
 */
//const uint8_t CONFIG_CMTYPE = DT_DHT;
//
//// Host & Client Addresses
//// Default Host Address
//#define CONFIG_CLIENT_ADDR	"clie5";
//#define CONFIG_HOST_ADDR	"serv1";
//
//// NRF24L01 SPI Driver Configuration
//static const uint8_t CONFIG_NRF_CE = 8;
//static const uint8_t CONFIG_NRF_CSN = 7;
//
//// Module specific I/O pins
//const uint8_t CONFIG_PIN_DEVICE1 = 9; // PWM
//const uint8_t CONFIG_STATUS_LED  = 10;
//const uint8_t CONFIG_DHTPIN = 2; // Di.2
//
//// Every 30 seconds
//const unsigned long CONFIG_BLINK_FREQ = 30;

// Common to all modules
const uint16_t CONFIG_UART_BAUD  = 57600;

#endif	/* CONFIG_HPP */

