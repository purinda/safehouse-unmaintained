/*
 * File:   StatusIndicator.hpp
 * Author: purinda
 *
 * Created on 11 April 2012, 12:37 AM
 */

#ifndef STATUSINDICATOR_HPP
#define	STATUSINDICATOR_HPP

#include <../Common/Scheduler.hpp>
#include "config.hpp"

class StatusIndicator :
	public ITask
{
	public:
		StatusIndicator(int);

		void initialize();
		void run(Scheduler*);

		static const short STATUS_FREQ = 1000;

	private:
		int pin_;
		int seconds_pased_;
		boolean state_;
};


#endif	/* STATUSINDICATOR_HPP */

