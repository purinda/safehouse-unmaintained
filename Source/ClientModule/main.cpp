/*
 * File:   MsgParser.hpp
 * Author: purinda
 *
 * Created on 14 April 2012, 7:43 PM
 */

#include <../Common/Scheduler.hpp>
#include <../Common/Nrf24.hpp>
#include <../Common/Helper.hpp>
#include "StatusIndicator.hpp"
#include "Observer.hpp"
#include "../Common/Messaging.hpp"
#include "../Common/Debugger.h"
#include "Sensors/ACS712.hpp"
#include "Sensors/DHT.hpp"
#include "Sensors/PIR.hpp"

// Function declarations
void setup();
void loop();

// Initialize Task Scheduler
Scheduler scheduler;

// Sub tasks
StatusIndicator blinker(CONFIG_STATUS_LED);
Observer observer;

int is_big_endian(void)
{
    union {
        uint32_t i;
        char c[4];
    } bint = {0x01020304};

    return bint.c[0] == 1; 
}

void setup()
{
	// debuggin over Serial library
	Serial.begin(CONFIG_UART_BAUD);
	printf_begin();
	scheduler.initialize();

	// Setup wireless driver
	uint64_t* _host_address = (uint64_t*)CONFIG_HOST_ADDR;
	uint64_t* _client_address = (uint64_t*)CONFIG_CLIENT_ADDR;
	Nrf24::getInstance().initialize(CONFIG_NRF_CE, CONFIG_NRF_CSN);
	Nrf24::getInstance().setFromAddress(*_client_address);
	Nrf24::getInstance().setToAddress(*_host_address);

	// Initialize modules
	blinker.initialize();
	observer.initialize();
	DHT::getInstance().initialize();
	PIR::getInstance().initialize();
	ACS712::getInstance().initialize();

	scheduler.queue(&blinker);
	scheduler.queue(&observer);
	scheduler.queue(&DHT::getInstance());
	scheduler.queue(&PIR::getInstance());
	scheduler.queue(&ACS712::getInstance());
}

void loop()
{
	scheduler.processMessages();	
}
