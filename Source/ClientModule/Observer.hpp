/*
 * File:   Controller.hpp
 * Author: purinda
 *
 * Created on 14 April 2012, 9:14 PM
 */

#ifndef OBSERVER_HPP
#define	OBSERVER_HPP

#include "config.hpp"
#include "../Common/Scheduler.hpp"
#include <inttypes.h>
#include "../Common/Messaging.hpp"
#include "Sensors/ACS712.hpp"
#include "Sensors/DHT.hpp"
#include "Sensors/PIR.hpp"


class Observer :
	public ITask
{
	public:
		void initialize();
		void run(Scheduler*);
		void broadcastInfo(uint8_t*);
		void processCommand(uint8_t*);
		void processStatusChanges();
		
		Observer();
		virtual ~Observer();

	private:
		
		bool isMotionDetected;
		int secondsPassed;
		int motionChangedAt;
		
		static const uint8_t OBSERVE_FREQ = 1; // 1ms
};

#endif	/* OBSERVER_HPP */

