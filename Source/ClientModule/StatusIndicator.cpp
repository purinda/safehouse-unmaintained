/*
 * File:   StatusIndicator.cpp
 * Author: purinda
 *
 * Created on 11 April 2012, 12:37 AM
 */

#include "StatusIndicator.hpp"


StatusIndicator::StatusIndicator(int pin)
{
	pin_ = pin;
	// Power on when started
	state_ = true;
	seconds_pased_ = 0;
}

void StatusIndicator::initialize()
{
	pinMode( pin_, OUTPUT);
}

void StatusIndicator::run(Scheduler* scheduler)
{
	scheduler->schedule(this, STATUS_FREQ);
	// just to reset the variable
	if (seconds_pased_ >= 4096)
		seconds_pased_ = 0;	
	
	if ((seconds_pased_ % CONFIG_BLINK_FREQ)==0)
		digitalWrite(pin_, true);
	else
		digitalWrite(pin_, false);
	
	// count seconds
	seconds_pased_++;	
}
