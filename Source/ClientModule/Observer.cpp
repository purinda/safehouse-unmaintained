/*
 * File:   Controller.cpp
 * Author: purinda
 *
 * Created on 14 April 2012, 9:14 PM
 */

#include "Observer.hpp"

Observer::Observer()
{

}

Observer::~Observer()
{
}

void Observer::initialize()
{
	// Set IO pins
	pinMode(CONFIG_PIN_DEVICE1, OUTPUT);
	
	PacketDCP __packet;	
	// Send one beacon signal with attached device information
	broadcastInfo((uint8_t*)&__packet);
	// debug info
	Nrf24::getInstance().radio->printDetails();
}

void Observer::run(Scheduler* scheduler)
{
	scheduler->schedule(this, OBSERVE_FREQ);

	// keep track of seconds passed
	secondsPassed = millis() / 1000;	
	// just to reset the variable
	if (secondsPassed >= 4096)
		secondsPassed = 0;
	
	uint8_t _packet[Nrf24::PAYLOAD] = {0};
	// if there is data ready
	if ( Nrf24::getInstance().radio->available() )
	{
		// Dump the payloads until we've gotten everything
		bool done = false;
		while (!done)
		{
			// Fetch the payload, and see if this was the last one.
			done = Nrf24::getInstance().radio->read( &_packet, Nrf24::PAYLOAD );
			// Delay just a little bit to let the other unit
			// make the transition to receiver
			delay(20);
		}
		
//		Debug code to interrogate the received packet
//		uint8_t i = 0;
//		for (i=0; i< Nrf24::PAYLOAD; i++)
//		{
//			printf("\n\rChar at %d = %d", i, _packet[i]);	
//		}
		PacketCP* packetCP = (PacketCP*)&_packet;
		PacketCSP* packetCSP = (PacketCSP*)&_packet;
		printf("\n\r---------------------");	
		printf("\n\rCP Packet\n\r______________\n\rAddress=%s, Pt=%d, Ct=%d, Id=%u, Value=", ((char*)&packetCSP->address), packetCP->packetType, packetCP->commandType, packetCP->id);	
		// print value
		Serial.println(packetCP->value);
		

		PacketDummy* _referer = (PacketDummy*)_packet;
		// After matching CONTROL TYPE Signal a reply will be sent
		switch(_referer->packetType)
		{
			// Info returned for Command Packets
			case PT_DEVICE_CHECKING_PACKET:
				broadcastInfo(_packet);				
				break;
			// Device information 
			case PT_COMMAND_PACKET:
				this->processCommand(_packet);
				break;
		}

	}
	
	// Check continously for status changes
	processStatusChanges();

}

void Observer::processStatusChanges()
{
	if (CONFIG_CMTYPE != DT_BULB_PIR || CONFIG_CMTYPE != DT_PIR)
		return;
	
	uint64_t* _client_address = (uint64_t*)CONFIG_CLIENT_ADDR;
	
	// Process presence changes
	if ((PIR::getInstance().motionDetected == true) && (isMotionDetected != PIR::getInstance().motionDetected))
	{
		printf("\n\rPIR Motion detected");
		// Send-out presence notifications
		Messaging::reply(CT_PRESENCE, 1, *_client_address, 0);
		motionChangedAt = secondsPassed;
	}
	
	if ((PIR::getInstance().motionDetected == false) && (isMotionDetected != PIR::getInstance().motionDetected))
	{
		printf("\n\rPIR Motion ended");
		// Send-out presence notifications
		Messaging::reply(CT_PRESENCE, 0, *_client_address, 0);
		motionChangedAt = secondsPassed;
	}
	// NOT SURE THIS IS REALLY REQUIRED SINCE HOST QUERY ABILITIES EVERY NOW AND THEN 
	// ANYWAY...
	// notify host every 30 seconds if the time of the last motion changed.
	if ((motionChangedAt <= secondsPassed) && 
		((secondsPassed - motionChangedAt) % 30 == 0))
	{
		int pirStatus = (PIR::getInstance().motionDetected == true) ? 1 : 0;
		Messaging::reply(CT_PRESENCE, pirStatus, *_client_address, 0);
	}
		
	isMotionDetected = PIR::getInstance().motionDetected;
}

// Process Command Packets
void Observer::processCommand(uint8_t* packet)
{
	PacketCP* received_command = (PacketCP*)packet;
	uint64_t* _client_address = (uint64_t*)CONFIG_CLIENT_ADDR;

	// After matching CONTROL TYPE Signal a reply will be sent
	switch(received_command->commandType)
	{
		case CT_SWITCH:
			digitalWrite(CONFIG_PIN_DEVICE1, received_command->value);
			Messaging::reply(CT_SWITCH, received_command->value, *_client_address, received_command->id);
			break;
		case CT_VARIABLE_SWITCH:
			analogWrite(CONFIG_PIN_DEVICE1, received_command->value);
			Messaging::reply(CT_VARIABLE_SWITCH, received_command->value, *_client_address, received_command->id);
			break;
		case CT_HUMIDITY:
			Messaging::reply(CT_HUMIDITY, DHT::getInstance().humidity, *_client_address, received_command->id);
			break;
		case CT_TEMPERATURE:
			Messaging::reply(CT_TEMPERATURE, DHT::getInstance().temperature, *_client_address, received_command->id);
			break;
		case CT_CURRENT:
			Messaging::reply(CT_CURRENT, ACS712::getInstance().current, *_client_address, received_command->id);
			ACS712::getInstance().current = 0;
			break;			
	}
}

/*
 * Broadcast function to send self infor back to
 * Host Controller or other devices listening to DDP
 * packets.
 */
void Observer::broadcastInfo(uint8_t* packet)
{
	PacketDCP* __packet = (PacketDCP*)packet;
	uint64_t* _client_address = (uint64_t*)CONFIG_CLIENT_ADDR;
	
	PacketDDP beaconPacket;
	beaconPacket.packetType = PT_DEVICE_DATA_PACKET;
	beaconPacket.id = __packet->id;
	beaconPacket.deviceType = CONFIG_CMTYPE;
	beaconPacket.address = *_client_address;

	Messaging::beacon(&beaconPacket);
}


