/* 
 * File:   PIR.hpp
 * Author: purinda
 *
 * Created on 27 October 2012, 7:40 PM
 */

#ifndef PIR_HPP
#define	PIR_HPP

#include "config.hpp"
#include <Arduino.h>
#include "../../Common/Scheduler.hpp"
#include "inttypes.h"

class PIR : public ITask
{
	public:
	// Singleton pattern implemented as per
	// http://stackoverflow.com/questions/1008019/c-singleton-design-pattern
	static PIR& getInstance()
	{
		static PIR instance;
		return instance;
	}

	void initialize();
	void run(Scheduler*);

	//the time when the sensor outputs a low impulse	
	boolean motionDetected;		

private:

	PIR();
	PIR(PIR const&);
	void operator=(PIR const&);
	
	bool scan();
	
	long unsigned int lowIn;         
	boolean lockLow;
	boolean takeLowTime;  
	
	// Constants
	const static uint16_t POLL_FREQ = 100; // 1/10s
};

#endif	/* PIR_HPP */

