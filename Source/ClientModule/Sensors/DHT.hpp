/*
 * File:   Sensor.hpp
 * Author: purinda
 *
 * Created on 14 April 2012, 8:52 PM
 */

#ifndef DHT_HPP
#define	DHT_HPP

#include "config.hpp"
#include <Arduino.h>
#include "../../Common/Scheduler.hpp"
#include "inttypes.h"

// how many timing transitions we need to keep track of. 2 * number bits + extra
#define MAXTIMINGS 85


class DHT: public ITask
{
	public:
	// Singleton pattern implemented as per
	// http://stackoverflow.com/questions/1008019/c-singleton-design-pattern
	static DHT& getInstance()
	{
		static DHT instance;
		return instance;
	}

	void initialize();
	void run(Scheduler*);

	// environment sensing
	float temperature;
	float humidity;


	private:
		
		DHT();
		DHT(DHT const&);
		void operator=(DHT const&);
		
		uint8_t data[6];
		unsigned long _lastreadtime;
		unsigned long _iterations;
		boolean firstreading;
		
		void readDHT11();
		boolean read(void);
//		float readTemperature(bool S);
//		float readHumidity(void);

		// Constants
		const static uint16_t POLL_FREQ = 100; // 1/10s
};

#endif	/* DHT_HPP */

