/* 
 * File:   ACS712.cpp
 * Author: purinda
 * 
 * Created on 27 October 2012, 7:41 PM
 */

#include "ACS712.hpp"

ACS712::ACS712()
{
}

ACS712::ACS712(const ACS712& orig)
{
}

void ACS712::initialize()
{
}

void ACS712::run(Scheduler* scheduler)
{
	scheduler->schedule(this, POLL_FREQ);
	
	readCurrentUsage();
}

void ACS712::readCurrentUsage()
{

	int i = analogRead(0);
	if (i >= 500)
		current = current + (0.049 * i - 25) / 1000;
	
}
