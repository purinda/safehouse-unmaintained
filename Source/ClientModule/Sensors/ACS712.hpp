/* 
 * File:   ACS712.hpp
 * Author: purinda
 *
 * Created on 27 October 2012, 7:41 PM
 */

#ifndef ACS712_HPP
#define	ACS712_HPP

#include "config.hpp"
#include <Arduino.h>
#include "../../Common/Scheduler.hpp"
#include "inttypes.h"


class ACS712 : public ITask
{
	public:
	// Singleton pattern implemented as per
	// http://stackoverflow.com/questions/1008019/c-singleton-design-pattern
	static ACS712& getInstance()
	{
		static ACS712 instance;
		return instance;
	}

	void initialize();
	void run(Scheduler*);		
		
	// for current sensing
	float current;

private: 
	
	ACS712();
	ACS712(ACS712 const&);
	void operator=(ACS712 const&);
	
	void readCurrentUsage();	
	
	// Constants
	const static uint16_t POLL_FREQ = 1000; // 1/10s	
};

#endif	/* ACS712_HPP */

