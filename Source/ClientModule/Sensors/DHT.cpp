/*
 * File:   Sensor.cpp
 * Author: purinda
 *
 * Created on 14 April 2012, 8:52 PM
 */

#include "DHT.hpp"

DHT::DHT()
{
}

void DHT::initialize()
{
	// set up the pins!
	pinMode(CONFIG_DHTPIN, INPUT);
	digitalWrite(CONFIG_DHTPIN, HIGH);
	
	_lastreadtime = 0;
	_iterations = 0;
	firstreading = true;
}

void DHT::run(Scheduler* scheduler)
{
	scheduler->schedule(this, POLL_FREQ);

	// Poll sensor every 5 seconds
	if (_iterations++ % 50 == 0)
	{
		this->readDHT11();
	}
	else
	{
		// pull the pin high and wait 250 milliseconds
		// (with 100ms * 3 = 300ms)
		digitalWrite(CONFIG_DHTPIN, HIGH);
	}
}

/*	Following two functions are used to read values from
	DHT11 sensor.
*/
void DHT::readDHT11()
{
	if (read())
	{
		humidity = data[0];
		temperature = data[2];
	}
}

boolean DHT::read(void)
{
	uint8_t laststate = HIGH;
	uint8_t counter = 0;
	uint8_t j = 0, i;
	unsigned long currenttime;

	// pull the pin high and wait 250 milliseconds
	//  digitalWrite(CONST_DHTPIN, HIGH);
	//  delay(2500);

	currenttime = millis();
	if (currenttime < _lastreadtime)
	{
		// ie there was a rollover
		_lastreadtime = 0;
	}

	if (!firstreading && ((currenttime - _lastreadtime) < 2000))
	{
		return true; // return last correct measurement
		//delay(2000 - (currenttime - _lastreadtime));
	}
	firstreading = false;
	_lastreadtime = millis();

	data[0] = data[1] = data[2] = data[3] = data[4] = 0;

	// now pull it low for ~20 milliseconds
	pinMode(CONFIG_DHTPIN, OUTPUT);
	digitalWrite(CONFIG_DHTPIN, LOW);
	delay(20);
	cli();
	digitalWrite(CONFIG_DHTPIN, HIGH);
	delayMicroseconds(40);
	pinMode(CONFIG_DHTPIN, INPUT);

	// read in timings
	for ( i=0; i< MAXTIMINGS; i++)
	{
		counter = 0;
		while (digitalRead(CONFIG_DHTPIN) == laststate)
		{
			counter++;
			delayMicroseconds(1);
			if (counter == 255)
			{
				break;
			}
		}
		laststate = digitalRead(CONFIG_DHTPIN);

		if (counter == 255)
		{
			break;
		}

		// ignore first 3 transitions
		if ((i >= 4) && (i%2 == 0))
		{
			// shove each bit into the storage bytes
			data[j/8] <<= 1;
			if (counter > 6)
			{
				data[j/8] |= 1;
			}
			j++;
		}

	}

	sei();

	// check we read 40 bits and that the checksum matches
	if ((j >= 40) && (data[4] == ((data[0] + data[1] + data[2] + data[3]) & 0xFF)) )
	{
		return true;
	}

	return false;

}


//
////boolean S == Scale.  True == Farenheit; False == Celcius
//float DHT::readTemperature(bool S)
//{
//  float f;
//
//  if (read())
//  {
//      f = data[2];
//      if(S)
//      	f = convertCtoF(f);
//	  return f;
//  }
//
//  return NAN;
//}


//float DHT::readHumidity(void)
//{
//  float f;
//  if (read())
//  {
//	f = data[0];
//    return f;
//  }
//
//  return NAN;
//}
