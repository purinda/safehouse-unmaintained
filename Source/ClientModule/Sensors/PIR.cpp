/* 
 * File:   PIR.cpp
 * Author: purinda
 * 
 * Created on 27 October 2012, 7:40 PM
 */

#include "PIR.hpp"

PIR::PIR()
{
}

PIR::PIR(const PIR& orig)
{
}

void PIR::initialize()
{
	// PIR sensor settings
	pinMode(CONFIG_PIR_PIN, INPUT);
	digitalWrite(CONFIG_PIR_PIN, LOW);	
	
	// set up the pins!
	lockLow = true;	
}

void PIR::run(Scheduler* scheduler)
{
	scheduler->schedule(this, POLL_FREQ);
	
	this->scan();
}

bool PIR::scan()
{
	//the amount of milliseconds the sensor has to be low 
	//before we assume all motion has stopped
	long unsigned int pause = 5000;  

	if(digitalRead(CONFIG_PIR_PIN) == HIGH){
		if(lockLow){  
			//makes sure we wait for a transition to LOW before any further output is made:
			lockLow = false;
			
			motionDetected = true;
//			delay(50);
		}         
		takeLowTime = true;
	}

	if(digitalRead(CONFIG_PIR_PIN) == LOW){       
		if(takeLowTime)
		{
			lowIn = millis();          //save the time of the transition from high to LOW
			takeLowTime = false;       //make sure this is only done at the start of a LOW phase
		}
		//if the sensor is low for more than the given pause, 
		//we assume that no more motion is going to happen
		if(!lockLow && millis() - lowIn > pause){  
			//makes sure this block of code is only executed again after 
			//a new motion sequence has been detected
			lockLow = true;      
			
			motionDetected = false;
//			delay(50);
		}
	}
}

