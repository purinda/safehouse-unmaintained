using System;
using System.Threading;
using System.Text;
using System.IO;
using System.IO.Ports;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using Microsoft.SPOT.Cryptography;
using GHIElectronics.NETMF.FEZ;

namespace Safehouse
{
    public class Interpreter 
    {
        private static SerialPort UART = null;
        static int RXCount=0;

		static OutputPort OutPutPin;

		/*
        // split command section from raw communication signal
        public static string GetCommand(string inp)
        {
            string command = inp.Split(Config.SEPARATOR)[0];
            return command;
        }

        // split data section from raw communication signal
        public static string GetData(string inp)
        {
            string data = inp.Split(Config.SEPARATOR)[1];
            return data;
        }
		*/


		/*
        // Encryption or Decryption function - XTEA
        public static string Crypt(string data, EncryptDecrypt c) //data = The data we want to encrypt
        {
            // 16-byte 128-bit static key
            byte[] XTEA_key = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 
                                           9, 10, 11, 12, 13, 14, 15, 16 };
            Key_TinyEncryptionAlgorithm xtea =
                     new Key_TinyEncryptionAlgorithm(XTEA_key);

            //convert to byte array
            byte[] original_data = UTF8Encoding.UTF8.GetBytes(data);
            byte[] crypted_bytes = {};

            //Encrypt the data
            if (c == EncryptDecrypt.Encrypt)
                crypted_bytes = xtea.Encrypt(original_data, 0, original_data.Length, null);
            if (c == EncryptDecrypt.Decrypt)
                crypted_bytes = xtea.Decrypt(original_data, 0, original_data.Length, null);

            return new string(Encoding.UTF8.GetChars(crypted_bytes));
        }
		*/

        public static void InitializeUART()
        {    
            UART = new SerialPort("COM1", 115200, Parity.Even, 8, StopBits.One);
            UART.Open();
            //Binding to datareceive event
            UART.DataReceived += new SerialDataReceivedEventHandler(ReadXBee);
        }

		public static void ResetOutputPins()
		{
			//Initialize OUTPorts to 0
			foreach (int Pin in Config.DeviceSettings.IOPins)
			{
				OutputPort outPin = new OutputPort((Cpu.Pin)Pin, false);
			}
		}

        private static void SetLCDStatus(string input)
        {
            FEZ_Shields.KeypadLCD.Clear();
            FEZ_Shields.KeypadLCD.CursorHome();
            FEZ_Shields.KeypadLCD.Print(RXCount.ToString());
            FEZ_Shields.KeypadLCD.SetCursor(1, 0);
            FEZ_Shields.KeypadLCD.Print(input);    
        }

        private static void ReadXBee(object sender, SerialDataReceivedEventArgs e)
        {
            int read_count = 0;
            byte[] rx_data = new byte[10];
            byte[] tx_data;

            read_count = UART.Read(rx_data, 0, Config.DataBlockLength);
            if (read_count != Config.DataBlockLength)
            {
                Debug.Print("Wrong size: " + read_count.ToString());

            }
            else
            {
                RXCount++;
                string receivedDataAsString = new string(Encoding.UTF8.GetChars(rx_data));
                ExecuteData(receivedDataAsString);
                Debug.Print(receivedDataAsString);
                // setLCD text
                //SetLCDStatus(receivedDataAsString);
            }

            UART.Flush();
            UART.DiscardInBuffer();

			Thread.Sleep(100);
        }

        public static void WriteUART(string strOut)
        {
            byte[] BytOutput = Encoding.UTF8.GetBytes(strOut);

            UART.Flush();
            UART.Write(BytOutput, 0, BytOutput.Length);
        }

        private static bool ExecuteData(string data)
        {
			/*
			# 
			Output Signal 
			DDDDDDPST
			DEVICEID PIN(0-9)   SIGNAL(0..9) TYPE(0..9)
			6bytes   1byte      1byte        1byte
            
			EX: 123456011

             
			Input Signal 
			DDDDDDPST
			DEVICEID PIN(0-9)   SIGNAL(0..9) TYPE(0..9)
			6bytes   1byte      1byte        1byte
			 * 
			 * SIG BYTE:
			 * 0:OFF
			 * 1:ON
			 * 
			 * TYPE BYTE:
			 * 0:Output
			 * 1:Ack
			 * 
            
			EX: 123456111
			*/

			// try parse 
			string ReceivedDID = data.Substring(0,6);
			int PinRead = int.Parse( data.Substring(6,1) );
			int PinSignal = int.Parse( data.Substring(7, 1) );
			string PinType = data.Substring(8, 1);
			Boolean Signal = false;

			if (PinSignal == 0)
				Signal = false;
			if (PinSignal == 1)
				Signal = true;


			//Validate DID
			if (ReceivedDID.Equals(Config.DeviceSettings.DID))
			{
				Debug.Print("DID Passed!");

				foreach (int Pin in Config.DeviceSettings.IOPins)
				{
					//if Pin is in config, set the value
					if (Pin == PinRead)
					{
						//OutputPort outPin = new OutputPort((Cpu.Pin)Pin, Signal); //Dynamic PIN Status Change should be programmed

						//Example code to change pin status
						if ( OutPutPin == null )
							OutPutPin = new OutputPort((Cpu.Pin)FEZ_Pin.Digital.IO45, Signal);

						OutPutPin.Write(Signal);
						Debug.Print("PIN " + Pin + " Changed to " + Signal);
					}


				}				
			}
			
			return true;
        }

        public static void StartBlinking()
        {
			Thread led = new Thread(ToggleStatusLED);
            led.Start();
        }

        private static void ToggleStatusLED()
        {
            bool ledState = false;

            OutputPort led = new OutputPort((Cpu.Pin)FEZ_Pin.Digital.LED, ledState);

            while (true)
            {
                // Sleep for 2500 milliseconds
                Thread.Sleep(2500);

                // toggle LED state
                ledState = !ledState;
                led.Write(ledState);
            }
        }


    }
}
