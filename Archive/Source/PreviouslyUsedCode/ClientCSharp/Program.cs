﻿using System;
using System.Threading;

using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;

using GHIElectronics.NETMF.FEZ;

namespace Safehouse
{
    public class Program
    {
        public static void Main()
        {
            //Debugging stuff to write into the Keypad shield LCD
            //FEZ_Shields.KeypadLCD.Initialize();

			//Set all IO Pins(which are in Config) to 0
			Interpreter.ResetOutputPins();
         
            // Start STATUS LED Blinking, So We Know Board is Running....
            Interpreter.StartBlinking();

            // Initialize UART (XBee) Communication Port 
            Interpreter.InitializeUART();
        }

    }
}
