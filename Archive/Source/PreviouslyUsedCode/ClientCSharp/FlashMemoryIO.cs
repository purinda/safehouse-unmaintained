using System;
using System.Xml;
using System.Threading;
using System.Text;
using System.IO;
using System.IO.Ports;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using Microsoft.SPOT.Cryptography;
using GHIElectronics.NETMF.FEZ;

namespace Safehouse
{
    class FlashMemoryIO
    {
        public static void Initialize()
        {
            FEZ_Shields.FlashShield.Initialize();
            //Debug.Print("Page size is " + FEZ_Shields.FlashShield.PageSize
        }

        public static void EraseAll()
        {
            byte[] buffer = new byte[FEZ_Shields.FlashShield.PageSize];
            //Reset All Data
            for (int i = 0; i < FEZ_Shields.FlashShield.PageSize; i++)
            {
                buffer[i] = (byte)Convert.ToChar(0x0);
            }

            FEZ_Shields.FlashShield.WritePage(Config.PageUsed, buffer);
        }

        public static string Read()
        {
            byte[] buffer = new byte[FEZ_Shields.FlashShield.PageSize];

            string strRet = "";
            // start retrieving data
            FEZ_Shields.FlashShield.ReadPage(Config.PageUsed, buffer);
            for (int i = 0; i < FEZ_Shields.FlashShield.PageSize; i++)
            {
                strRet += Convert.ToChar(buffer[i]).ToString();
                if (buffer[i] == 0x0)
                    break;
            }

            return strRet;
        }

        public static void Write(string input)
        {
            byte[] buffer = new byte[FEZ_Shields.FlashShield.PageSize];

            //// store data
            for (int i = 0; i < input.Length; i++)
            {
                buffer[i] = (byte)input[i];
            }

            FEZ_Shields.FlashShield.WritePage(Config.PageUsed, buffer);       
        }

      
    }
}
