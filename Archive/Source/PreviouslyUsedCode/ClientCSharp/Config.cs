using System;
using Microsoft.SPOT;

namespace Safehouse
{
    class Config
    {
        /* ATMEL Flash Related Config */
        public const uint PageUsed = 0;

        public struct DeviceSettings
        {
			public static string DID = "332512";
			public static int[] IOPins = {2,3};
		}

        // Constants
        public const int DataBlockLength = 9;
        public const char SEPARATOR = ' ';
    }


}
