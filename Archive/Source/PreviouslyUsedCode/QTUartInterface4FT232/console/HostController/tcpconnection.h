#ifndef TCPCONNECTION_H
#define TCPCONNECTION_H

#include <QThread>
#include <QtNetwork>
#include "serialio.h"

#define LISTENPORT 1002

class TCPConnection : public QThread
{
    Q_OBJECT
public:
	SerialIO *objSerial;
	TCPConnection(QTcpSocket*, QObject *parent = 0);
	void run();
private:
	QTcpSocket *_sockConnection;
	QByteArray _data;
	void write(QByteArray);

signals:
	void signalTCPTriggered(QByteArray);

private slots:
	void slotRead();
	void slotSerialTriggered(QByteArray);
};

#endif // TCPCONNECTION_H
