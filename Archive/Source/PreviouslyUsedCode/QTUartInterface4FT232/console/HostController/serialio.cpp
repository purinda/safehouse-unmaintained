#include "serialio.h"

SerialIO::SerialIO(QObject *parent)
		: QObject(parent),_sPort(0)
{
	//Signals and Slots
	this->_serialDevice = SerialDeviceEnumerator::instance();
	connect(this->_serialDevice, SIGNAL(hasChanged(QStringList)),
			this, SLOT(slotDetectActive(QStringList)));

	this->slotDetectActive(this->_serialDevice->devicesAvailable());
}

void SerialIO::slotDetectActive(const QStringList &list)
{

	foreach (QString each, list)
	{
		this->_serialDevice->setDeviceName(each);

		if (this->_serialDevice->locationInfo() == DEVICENAME)
		{

			this->_sPort = new AbstractSerial(this);
			this->_deviceId = this->_serialDevice->name();

			if (!this->_sPort->isOpen())
			{
				qDebug() << "Port Opened" << endl;
				this->open();
			}

			//When receiving data from Serial
			connect(this->_sPort, SIGNAL(readyRead()),
					this, SLOT(slotRead()));

			qDebug() << "Device ID :" << this->_deviceId;

		}
	}

}

void SerialIO::slotRead()
{
	if (this->_sPort->isOpen())
	{
		int _noBytes2Read = this->_sPort->bytesAvailable();
		QByteArray data = this->_sPort->read(_noBytes2Read);
		emit signalSerialTriggered(data);

		qDebug() << "Rx: " << data;
	}

}

void SerialIO::slotTCPTriggered(QByteArray _tcpData)
{
	if (QString(_tcpData).contains("QUIT", Qt::CaseInsensitive))
	{
		this->slotClose();
		exit(0);
	}

	this->write(_tcpData);
}


bool SerialIO::write(QByteArray _out)
{
	if (this->_sPort->isOpen())
	{
		this->_sPort->write(_out);
		return true;
	}
	else
		return false;
}

bool SerialIO::open()
{
	this->_sPort->setDeviceName(this->_deviceId);

	bool ret = this->_sPort->open(QIODevice::ReadWrite);
	if (ret)
	{
		ret = this->_sPort->setBaudRate(AbstractSerial::BaudRate9600);
		if (ret)
qDebug() << this->_sPort->baudRate();
	}
	else
	{
//qDebug() << "An error occured while opening serial port (" << this->_deviceId << ")";
		//this->slotClose();
		//exit(1);
	}

	return ret;
}

void SerialIO::slotClose()
{
qDebug() << "UART closed";
	this->_sPort->close();
}
