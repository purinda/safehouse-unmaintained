#ifndef NETWORKIOTHREAD_H
#define NETWORKIOTHREAD_H

#include <QThread>

class NetworkIOThread : public QThread
{
    Q_OBJECT
public:
	NetworkIOThread();

protected:
	void run();

};

#endif // NETWORKIOTHREAD_H
