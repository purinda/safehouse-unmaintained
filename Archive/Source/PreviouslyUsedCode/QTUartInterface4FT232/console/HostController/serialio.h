#ifndef SERIALIO_H
#define SERIALIO_H

#include <QtCore/QObject>
#include <QString>
#include <QtCore/QDebug>
#include <QtCore/QStringList>

#include "serialdeviceenumerator.h"
#include "abstractserial.h"

#define DEVICENAME  "Arduino Uno"

class SerialDeviceEnumerator;
class SerialIO : public QObject
{
	Q_OBJECT
public:
	SerialIO(QObject *parent = 0);
	bool open();
	bool write(QByteArray );
	QString read();

private:
	QString _deviceId;
	SerialDeviceEnumerator *_serialDevice;
	AbstractSerial *_sPort;

signals:
	void signalSerialTriggered(QByteArray rec);

private slots:
	void slotDetectActive(const QStringList &list);
	void slotTCPTriggered(QByteArray);
	void slotRead();
	void slotClose();

};

#endif // ENUMERATOR_H
