#-------------------------------------------------
#
# Project created by QtCreator 2011-08-11T22:01:56
#
#-------------------------------------------------

QT       += core network xml xmlpatterns

QT       -= gui


unix:include(lib/unix/ttylocker.pri)
include(lib/qserialdeviceenumerator/qserialdeviceenumerator.pri)
include(lib/qserialdevice/qserialdevice.pri)

INCLUDEPATH += lib/qserialdeviceenumerator lib/qserialdevice
TARGET = HostController
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    serialio.cpp \
    networkio.cpp \
    tcpconnection.cpp \
    xmlparser.cpp


OTHER_FILES += \
    TEMP.txt

HEADERS += \
    serialio.h \
    networkio.h \
    tcpconnection.h \
    singleton.h \
    xmlparser.h

win32 {
	LIBS += -lsetupapi -luuid -ladvapi32
}
unix:!macx {
	LIBS += -ludev
}
