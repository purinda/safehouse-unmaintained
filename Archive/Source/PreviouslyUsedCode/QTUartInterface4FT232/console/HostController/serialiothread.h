#ifndef SERIALIOTHREAD_H
#define SERIALIOTHREAD_H

#include <QThread>
#include <QtCore/QDebug>
#include <QTimer>

class SerialIOThread : public QThread
{
	Q_OBJECT

public:
    SerialIOThread();

protected:
	void run();

signals:
	void signalClosePort( void );
};

#endif // SERIALIOTHREAD_H
