#include "xmlparser.h"
#include <QtXmlPatterns>

XMLParser::XMLParser(QObject *parent) :
    QObject(parent)
{
}

XMLParser::XMLParser(QString _xml, QString _xq)
{
	this->xmlPath = _xml;
	this->xqPath = _xq;

	this->loadFile();
	this->evaluate();
}

QString XMLParser::getResult()
{
	return this->result;
}

void XMLParser::loadFile()
{
	QFile _xmlFile;
	_xmlFile.setFileName(this->xmlPath);
	if (!_xmlFile.open(QIODevice::ReadOnly)) {
		qDebug() << "Unable to open file"
				 << _xmlFile.errorString();

		exit(1);
	}

	QTextStream _xml(&_xmlFile);
	this->xmlContent = _xml.readAll();

	QFile _xqFile;
	_xqFile.setFileName(this->xqPath);
	if (!_xqFile.open(QIODevice::ReadOnly)) {
		qDebug() << "Unable to open file"
				 << _xqFile.errorString();

		exit(1);
	}

	QTextStream _xq(&_xqFile);
	this->xqContent = _xq.readAll();
}

void XMLParser::evaluate()
{
	QFile _xqFile;
	_xqFile.setFileName(this->xmlPath);
	_xqFile.open(QIODevice::ReadOnly);

	QByteArray outArray;
	QBuffer buffer(&outArray);
	buffer.open(QIODevice::ReadWrite);

	QXmlQuery query;
	query.bindVariable("inputDocument", &_xqFile);
	query.setQuery(this->xqContent);
	if (!query.isValid())
		return;

	QXmlFormatter formatter(query, &buffer);
	if (!query.evaluateTo(&formatter))
		return;

	buffer.close();
	this->result = QString::fromUtf8(outArray.constData());

}
