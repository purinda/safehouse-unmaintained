#include <QtCore/QCoreApplication>
#include <QtCore/QStringList>
#include <QtCore/QDebug>

/* NetworkIO */
#include "networkio.h"
#include "xmlparser.h"


int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);

	NetworkIO oNetwork;

	return a.exec();
}
