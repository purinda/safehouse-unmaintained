#ifndef XMLPARSER_H
#define XMLPARSER_H

#include <QObject>

class XMLParser : public QObject
{
    Q_OBJECT
private:
	QString xmlPath;
	QString xqPath;
	QString xmlContent;
	QString xqContent;
	QString	result;

	void loadFile();
	void evaluate();

public:
    explicit XMLParser(QObject *parent = 0);
	XMLParser(QString, QString);
	QString getResult();

signals:

public slots:

};

#endif // XMLPARSER_H
