#include "networkio.h"
#include "tcpconnection.h"

NetworkIO::NetworkIO(QObject *parent) :
	QObject(parent)
{
	if (!this->_server.listen(QHostAddress::Any, LISTENPORT)) {
		qDebug () << tr("Unable to start the server: %1.")
				  << this->_server.errorString();

		this->_server.close();
		exit(1);
	}

	qDebug() << "TCP Server initiated using following configuration, "
			 << " IP and Port: "
			 <<	this->_server.serverAddress()
			 << ":" << this->_server.serverPort();

	connect(&this->_server, SIGNAL(newConnection()),
			this, SLOT(incomingConnection()));

}

void NetworkIO::incomingConnection()
{

	TCPConnection *thread = new TCPConnection(this->_server.nextPendingConnection(), this);

	connect(thread, SIGNAL(finished()),
			thread, SLOT(deleteLater()));

	thread->start();

}
