#include "tcpconnection.h"
#include "serialio.h"
#include "singleton.h"

TCPConnection::TCPConnection(QTcpSocket* _incomingSocket, QObject *parent) :
    QThread(parent)
{
	this->_sockConnection = _incomingSocket;

	//Triggers when receiving data from connected host
	connect(this->_sockConnection, SIGNAL(readyRead()),
			this, SLOT(slotRead()));

}

void TCPConnection::run()
{
	objSerial = &Singleton<SerialIO>::Instance();

	//Send data from Serial to TCP
	QObject::connect(objSerial, SIGNAL( signalSerialTriggered(QByteArray) ),
					 this, SLOT( slotSerialTriggered(QByteArray) ) );

	//Send data from TCP to Serials
	QObject::connect(this, SIGNAL( signalTCPTriggered(QByteArray) ),
					 objSerial, SLOT( slotTCPTriggered(QByteArray) ) );

	exec();
}

void TCPConnection::slotSerialTriggered(QByteArray rec)
{
	this->write(rec);
}


void TCPConnection::write(QByteArray _data)
{
	if (this->_sockConnection->isWritable())
	{
		this->_sockConnection->write(_data);
	}
}

void TCPConnection::slotRead()
{
	QByteArray __read = this->_sockConnection->readAll();
	_data.append(__read);

	// if CR found then send collected data to Serial Ifce
	if (__read.at(0) == 13)
	{
		emit signalTCPTriggered(_data);
		_data.clear();
	}
}
