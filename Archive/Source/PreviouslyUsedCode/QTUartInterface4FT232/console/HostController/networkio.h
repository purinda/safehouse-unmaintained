#ifndef NETWORKIO_H
#define NETWORKIO_H

#include <QTcpServer>
#include <QtNetwork>

class NetworkIO : public QObject
{
    Q_OBJECT

private:
	QTcpServer _server;
public:
    explicit NetworkIO(QObject *parent = 0);	

public slots:
	void incomingConnection();
};

#endif // NETWORKIO_H

