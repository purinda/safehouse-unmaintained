/*
 * AVRGCC2.c
 *
 * Created: 4/06/2011 14:50:28
 *  Author: Purinda
 */ 

#include <avr/io.h>
#include <avr/eeprom.h>
#include <avr/delay.h>

#define F_CPU 16000000  // 16 MHz
//#define F_CPU 8000000  // 8 MHz Internal Osc
#define SPEED 9600

void serial_init(unsigned int bittimer)
{
	/* Set the baud rate */
	UBRR0H = (unsigned char) (bittimer >> 8);
	UBRR0L = (unsigned char) bittimer;
	
	/* Engage! */
	UCSR0B = (1 << RXEN0) | (1 << TXEN0);
	/* set the framing to 8N1 */
	UCSR0C = (3 << UCSZ00);
	/* Set frame format: 8data, 2stop bit */
	//UCSR0C = (1<<USBS0)|(3<<UCSZ00);		
	return;
}

unsigned char serial_read(void)
{
	while( !(UCSR0A & (1 << RXC0)) )
		;
	return UDR0;
}

void serial_write(unsigned char c)
{
	while ( !(UCSR0A & (1 << UDRE0)) )
		;
	UDR0 = c;
}

void EEPROM_write(unsigned int uiAddress, unsigned char ucData)
{
	/* Wait for completion of previous write */
	while(EECR & (1<<EEPE))
	;
	/* Set up address and Data Registers */
	EEAR = uiAddress;
	EEDR = ucData;
	/* Write logical one to EEMPE */
	EECR |= (1<<EEMPE);
	/* Start eeprom write by setting EEPE */
	EECR |= (1<<EEPE);
}

unsigned char EEPROM_read(unsigned int uiAddress)
{
	/* Wait for completion of previous write */
	while(EECR & (1<<EEPE))
	;
	/* Set up address register */
	EEAR = uiAddress;
	/* Start eeprom read by writing EERE */
	EECR |= (1<<EERE);
	/* Return data from Data Register */
	return EEDR;
}


int main(void)
{

	// Set Port B pins as all outputs
	DDRB = 0b11111111;

	// Set all Port B pins as HIGH
	/*
	while (1)
	{
		PORTB = 0b11111111;
		_delay_ms(1000);	
		PORTB = 0x0;
		_delay_ms(1000);	
		
	}
	*/
	
		
	/* let the preprocessor calculate this */
	serial_init( ( F_CPU / SPEED / 16 ) - 1);
	unsigned char X = 65;
	unsigned char Y = 0;
	while (1) {
		/* read a character and echo back the next one */
		//serial_write( serial_read() + 1 );	
		serial_write( X );
		//Y = serial_read();
		if (Y == 66)
		{
			PORTB = 0b11111111;
			break;
		}

		PORTB = 0b11111111;
		_delay_ms(1000);	
		PORTB = 0x0;
		_delay_ms(1000);	

	}
		
	return 1;
}