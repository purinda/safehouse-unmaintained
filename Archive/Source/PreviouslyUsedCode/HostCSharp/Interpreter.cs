using System;
using System.Xml;
using System.Threading;
using System.Text;
using System.IO;
using System.IO.Ports;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using Microsoft.SPOT.Cryptography;
using GHIElectronics.NETMF.FEZ;

namespace Safehouse
{
    public static class Interpreter 
    {
        // Structures for Message Handling
        public struct IOPort
        {
            private int portnum;
            private int portval;

            public int Port
            {
                get
                {
                    return portnum;
                }
                set
                {
                    portnum = value;
                }
            }

            public int Value
            {
                get
                {
                    return portval;
                }
                set
                {
                    portval = value;
                }
            }


        }


        // Constants
        public const char SEPARATOR = ' ';
        public static SerialPort UART = null;
        public enum EncryptDecrypt : byte
        {
            Encrypt = 1,
            Decrypt = 0
        }

        // split command section from raw communication signal
        public static string GetCommand(string inp)
        {
            string command = inp.Split(SEPARATOR)[0];
            return command;
        }

        // split data section from raw communication signal
        public static string GetData(string inp)
        {
            string data = inp.Split(SEPARATOR)[1];
            return data;
        }

        // Encryption or Decryption function - XTEA
        public static string Crypt(string data, EncryptDecrypt c) //data = The data we want to encrypt
        {
            // 16-byte 128-bit static key
            byte[] XTEA_key = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 
                                           9, 10, 11, 12, 13, 14, 15, 16 };
            Key_TinyEncryptionAlgorithm xtea =
                     new Key_TinyEncryptionAlgorithm(XTEA_key);

            //convert to byte array
            byte[] original_data = UTF8Encoding.UTF8.GetBytes(data);
            byte[] crypted_bytes = {};

            //Encrypt the data
            if (c == EncryptDecrypt.Encrypt)
                crypted_bytes = xtea.Encrypt(original_data, 0, original_data.Length, null);
            if (c == EncryptDecrypt.Decrypt)
                crypted_bytes = xtea.Decrypt(original_data, 0, original_data.Length, null);

            return new string(Encoding.UTF8.GetChars(crypted_bytes));
        }

        public static void InitializeUART()
        {
            UART = new SerialPort("COM1", 9600, Parity.None, 8, StopBits.One);
            UART.Open();

            //UART.DataReceived += new SerialDataReceivedEventHandler(UART_DataReceived);
           
        }

        private static void ReadXBee()
        {
            int NumOfBytes2Read = 0;
           
            while (true)
            {
                NumOfBytes2Read = UART.BytesToRead;
                byte[] rx_data = new byte[NumOfBytes2Read];

                if (NumOfBytes2Read > 0)
                {
                    UART.Read(rx_data, 0, NumOfBytes2Read);

                    String receivedDataAsString = new String(Encoding.UTF8.GetChars(rx_data));
                    Debug.Print(NumOfBytes2Read + " - " + receivedDataAsString);

                }

                UART.Flush();
                Thread.Sleep(250);
            }
        }

        public static void ReadUART()
        {
            // Continuously read UART
            Thread ThreadUART = new Thread(ReadXBee);
            ThreadUART.Start();
        }

        public static void WriteUART(string strOut)
        {
            byte[] BytOutput = Encoding.UTF8.GetBytes(strOut);
            UART.Write(BytOutput, 0, BytOutput.Length);
            UART.Flush();
            UART.DiscardOutBuffer();
        }

        public static void ShowLEDIndicator()
        {
            Thread led = new Thread(StatusLEDOn);
            led.Start();
        }

        private static void StatusLEDOn()
        {
            bool ledState = false;

            OutputPort led = new OutputPort((Cpu.Pin)FEZ_Pin.Digital.LED, ledState);

            while (true)
            {
                // Sleep for 500 milliseconds
                Thread.Sleep(2500);

                // toggle LED state
                ledState = !ledState;
                led.Write(ledState);
            }
        }

    }
}
