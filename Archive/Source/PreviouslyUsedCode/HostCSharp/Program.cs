﻿using System;
using System.Text;
using System.Threading;
using GHIElectronics.NETMF.FEZ;
using Microsoft.SPOT.Hardware;
using Microsoft.SPOT;
//Refers to EthernetShield.cs Object
using Socket = GHIElectronics.NETMF.FEZ.FEZ_Shields.Ethernet.uSocket;

namespace Safehouse
{
    public class Program
    {
         //Network properties
        private static byte[] IP = { 192, 168, 3, 10 };
        private static byte[] SUBNET = { 255, 255, 255, 0 };
        //private static byte[] GATEWAY = { 192, 168, 3, 1 };
        private static byte[] GATEWAY = { 0, 0, 0, 0 };
        private static byte[] MAC = { 43, 232, 111, 12, 03, 251 };
        private static ushort PORT = 16339;
        // Network Socket
        private static FEZ_Shields.Ethernet.uSocket socket;

        private static bool ledState = false;
        public static void Main()
        {

            OutputPort led = new OutputPort((Cpu.Pin)FEZ_Pin.Digital.LED, ledState);

            while (true)
            {
                // Sleep for 500 milliseconds
                Thread.Sleep(2500);

                // toggle LED state
                ledState = !ledState;
                led.Write(ledState);
            }

            // Initialize UART 
            Interpreter.InitializeUART();

            // Keep blinking so we know CLR is running.
            Interpreter.ShowLEDIndicator();

            // Init the Ethernet shield 
            FEZ_Shields.Ethernet.Initialize(IP, SUBNET, GATEWAY, MAC);
			
            Socket socket;
            socket = new Socket(Socket.Protocol.TCP, PORT);


            byte[] buf = new byte[255];
            int n;
            System.Text.UTF8Encoding enc;
            enc = new System.Text.UTF8Encoding();

            while (true)
            {
                String line = "";
                socket.Listen();
                while (socket.GetStatus() != FEZ_Shields.Ethernet.uSocket.SR_val.SOCK_ESTABLISHED)
                { }
                while (socket.GetStatus() == FEZ_Shields.Ethernet.uSocket.SR_val.SOCK_ESTABLISHED)
                {
                    Array.Clear(buf, 0, 255);
                    n = socket.Receive(buf, socket.AvilableBytes);
                    if (n > 0)
                    {
                        try
                        {
                            char[] chars = enc.GetChars(buf);
                            line = line + new string(chars);
                        }

                        catch (Exception e) { Debug.Print("error"); }

                        if (buf[n - 1] == 0x0A || buf[n - 1] == 0x0D)
                        {
                            if (line.Length >= 4 && line.Substring(0, 4) == "QUIT")
                            {
                                goto done;
                            }
                            // ON Switches
                            Interpreter.WriteUART(line);
                            /*
                            if (line.Length >= 4 && line.Substring(0, 4) == "TEST")
                            {
                                string strXML = "123456###";
                                Interpreter.WriteUART(strXML);
                            }

                            if (line.Length >= 2 && line.Substring(0, 2) == "ON")
                            {
                                string pin = line.Substring(3, 1);
                                Interpreter.WriteUART(line.Substring());
                            }

                            // OFF Switches
                            if (line.Length >= 3 && line.Substring(0, 3) == "OFF")
                            {
                                string strXML = "<Req> <Device id=\"12345\" type=\"out\"> <IO pin=\"2\" signal=\"off\"/> </Device> </Req> ";
                                Interpreter.WriteUART(strXML);
                            }
                             */

                            line = line + "\n";
                            socket.Send(enc.GetBytes(line), line.Length - 1);
                            line = "";
                        }
                    }

                }

            done:
                socket.Disconnect();
                socket.Close();
                socket = new Socket(Socket.Protocol.TCP, PORT);
            }



        }

    }
}
