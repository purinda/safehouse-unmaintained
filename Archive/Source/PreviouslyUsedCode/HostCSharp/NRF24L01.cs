///////////////////////////////////////////////////////////////////////////////
//
//  This file holds classes that you can use
//  to communicate trough the nRF24L01 from Nordic.
//
//  It's a very basic one way implementation. You should
//  adapt to your personal requirements.
//
//  For more information on the nRF24L01 you should visit these sites:
//
//  - Nordic product page
//    http://www.nordicsemi.com/index.cfm?obj=product&act=display&pro=89
//
//  - Class written by Peter Ivanov, Olimex Ltd. for MSP430-449STK2
//    http://dev.ivanov.eu/projects/msp430-4619lcd_sample/nrf24l01_8c-source.html
//
//
//  Copyright:
//  (C)opyright 2008, Elze Kool 
//  http://www.microframework.nl
//  
//  You can use this file as you like, please include the above copyright 
//  information as I did with my sources.
//
//
//  Enjoy!
//
//////////////////////////////////////////////////////////////////////////////

using System;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;

using System.Threading;

using GHIElectronics;
using GHIElectronics.Hardware;
using GHIElectronics.System;

/// <summary>
/// Commands for NRF24L01
/// </summary>
public static class NRF24L01Commands
{
    public const byte R_REGISTER = 0x00;
    public const byte W_REGISTER = 0x20;
    public const byte R_RX_PAYLOAD = 0x61;
    public const byte W_TX_PAYLOAD = 0xA0;
    public const byte FLUSH_TX = 0xE1;
    public const byte FLUSH_RX = 0xE2;
    public const byte REUSE_TX_PL = 0xE3;
    public const byte ACTIVATE = 0x50;
    public const byte R_RX_PL_WID = 0x60;
    public const byte W_ACK_PAYLOAD = 0xA8;
    public const byte W_TX_PAYLOAD_NO_ACK = 0xB0;
    public const byte NOP = 0xFF;
}

/// <summary>
/// Mnemonics for the NRF24L01 Registers
/// </summary>
public static class NRF24L01Mnemonics
{
    public static byte MASK_RX_DR = 6;
    public static byte MASK_TX_DS = 5;
    public static byte MASK_MAX_RT = 4;
    public static byte EN_CRC = 3;
    public static byte CRCO = 2;
    public static byte PWR_UP = 1;
    public static byte PRIM_RX = 0;
    public static byte ENAA_P5 = 5;
    public static byte ENAA_P4 = 4;
    public static byte ENAA_P3 = 3;
    public static byte ENAA_P2 = 2;
    public static byte ENAA_P1 = 1;
    public static byte ENAA_P0 = 0;
    public static byte ERX_P5 = 5;
    public static byte ERX_P4 = 4;
    public static byte ERX_P3 = 3;
    public static byte ERX_P2 = 2;
    public static byte ERX_P1 = 1;
    public static byte ERX_P0 = 0;
    public static byte AW = 0;
    public static byte ARD = 4;
    public static byte ARC = 0;
    public static byte PLL_LOCK = 4;
    public static byte RF_DR = 3;
    public static byte RF_PWR = 1;
    public static byte LNA_HCURR = 0;
    public static byte RX_DR = 6;
    public static byte TX_DS = 5;
    public static byte MAX_RT = 4;
    public static byte RX_P_NO = 1;
    public static byte TX_FULL = 0;
    public static byte PLOS_CNT = 4;
    public static byte ARC_CNT = 0;
    public static byte TX_REUSE = 6;
    public static byte FIFO_FULL = 5;
    public static byte TX_EMPTY = 4;
    public static byte RX_FULL = 1;
    public static byte RX_EMPTY = 0;
}

/// <summary>
/// Registers for NRF24L01
/// Can be read with NRF24L01Commands.R_REGISTER and written by NRF24L01Commands.W_REGISTER
/// </summary>
public static class NRF24L01Registers
{
    public const byte CONFIG = 0x00;
    public const byte EN_AA = 0x01;
    public const byte EN_RXADDR = 0x02;
    public const byte SETUP_AW = 0x03;
    public const byte SETUP_RETR = 0x04;
    public const byte RF_CH = 0x05;
    public const byte RF_SETUP = 0x06;
    public const byte STATUS = 0x07;
    public const byte OBSERVE_TX = 0x08;
    public const byte CD = 0x09;
    public const byte RX_ADDR_P0 = 0x0A;
    public const byte RX_ADDR_P1 = 0x0B;
    public const byte RX_ADDR_P2 = 0x0C;
    public const byte RX_ADDR_P3 = 0x0D;
    public const byte RX_ADDR_P4 = 0x0E;
    public const byte RX_ADDR_P5 = 0x0F;
    public const byte TX_ADDR = 0x10;
    public const byte RX_PW_P0 = 0x11;
    public const byte RX_PW_P1 = 0x12;
    public const byte RX_PW_P2 = 0x13;
    public const byte RX_PW_P3 = 0x14;
    public const byte RX_PW_P4 = 0x15;
    public const byte RX_PW_P5 = 0x16;
    public const byte FIFO_STATUS = 0x17;
    public const byte DYNPD = 0x1C;
    public const byte FEATURE = 0x1D;
}


public static class NRF24L01
{
    //////////////////////////////////////////////////////////////
    //
    //       Ports used for communication with nRF24L01
    //
    //////////////////////////////////////////////////////////////

    // Connections used:
    //
    //  NRF24L01         Embedded Master
    // ---------------------------------
    //  VCC      <->     VCC
    //  GND      <->     GND
    //  CE       <->     E11 I2C_SCL            (Chip Enable)
    //  IRQ      <->     E12 I2C_SDA            (Interrupt)
    //  MISO     <->     E25 SPI_MISO           (SPI Slave Out)
    //  MOSI     <->     E24 SPI_MOSI           (SPI Slave In)
    //  CLK      <->     E27 SPI_SCK            (SPI Clock)
    //  CSN      <->     E26 SPI_SSEL           (SPI Chip Select)


    /// <summary>
    /// Chip Enable Port
    /// When High Activates TX and RX
    /// </summary>
    private static OutputPort NRF24L01_ChipEnable;

    /// <summary>
    /// Interrupt request port
    /// </summary>
    private static InterruptPort NRF24L01_IRQ;

    /// <summary>
    /// SPI port used for NRF24L01
    /// </summary>
    private static SPI SpiPort = null;


    /// <summary>
    /// Enable RX/TX by setting CE
    /// </summary>
    public static void EnableRXTX()
    {
        try {
            NRF24L01_ChipEnable.Write(true);
        } catch {
            throw new Exception("Failed to enable ChipEnable, maybe not initalized?");
        }
    }

    /// <summary>
    /// Disable RX/TX by clearing CE
    /// </summary>
    public static void DisableRXTX()
    {
        try {
            NRF24L01_ChipEnable.Write(false);
        } catch {
            throw new Exception("Failed to disable ChipEnable, maybe not initalized?");
        }
    }


    //////////////////////////////////////////////////////////////
    //
    //          Adresses used for sending and recieving
    //
    //////////////////////////////////////////////////////////////

    /// <summary>
    /// Adres for recieving data
    /// </summary>
    public static byte[] RX_Adress = new byte[] { 0x05, 0x06, 0x07, 0x08, 0x09 };

    /// <summary>
    /// Adres to send data to
    /// </summary>
    public static byte[] TX_Adress = new byte[] { 0x05, 0x06, 0x07, 0x08, 0x09 };

    /// <summary>
    /// Delegate for OnInterrupt event
    /// </summary>
    /// <param name="statuscode">Code retrieved from status register</param>
    public delegate void OnInterruptHandler(byte statuscode, ref bool StopDefaultInterrupt);

    /// <summary>
    /// Event on nRF24L01 IRQ. 
    /// Called before OnRecieve and OnTransmit. 
    /// You can set StopDefaultInterrupt to true to stop the
    /// default interrupt from processing. You should ensure that the IRQ mask is correctly
    /// reset.
    /// </summary>
    public static event OnInterruptHandler OnInterrupt;

    //////////////////////////////////////////////////////////////
    //
    //              Recieve delegate and event handler
    //
    //////////////////////////////////////////////////////////////

    /// <summary>
    /// Delegate for OnDataRecieved event
    /// </summary>
    /// <param name="data">Recieved byte</param>
    /// <param name="statuscode">Code retrieved from status register</param>
    public delegate void OnDataRecievedHandler(byte data, byte statuscode);

    /// <summary>
    /// Event handler for Data recieved.
    /// Triggered on recieval of data.
    /// </summary>
    public static event OnDataRecievedHandler OnDataRecieved;




    //////////////////////////////////////////////////////////////
    //
    //              Transmit delegate and event handler
    //
    //////////////////////////////////////////////////////////////

    /// <summary>
    /// Delegate for OnTransmit
    /// </summary>
    /// <param name="succes">True on succesfull data transmit</param>
    /// <param name="statuscode">Code retrieved from status register</param>
    /// <returns></returns>
    public delegate void OnTransmitHandler(bool succes, byte statuscode);

    /// <summary>
    /// Event handler for Data transmit.
    /// Triggered on transmitting data. 
    /// </summary>
    public static event OnTransmitHandler OnTransmit;





    /// <summary>
    /// Open Pins and SPI port for NRF24L01.
    /// </summary>
    public static void Initialize()
    {
        // Initialize SPI
        // Chip Select : Port E26 SPI_SSEL, Active Low
        // Clock : Active High, Data clocked in on rising edge, Rate 3MHz
        // SPI Port: SPI1
        SpiPort = new SPI(new SPI.Configuration(EmbeddedMaster.Pins.E26x, false, 0, 0, false, false, 2000, SPI.SPI_module.SPI1));

        // Initialize IRQ Port
        NRF24L01_IRQ = new InterruptPort(EmbeddedMaster.Pins.E12x, false, Port.ResistorMode.PullUp, Port.InterruptMode.InterruptEdgeLow);
        NRF24L01_IRQ.OnInterrupt += new GPIOInterruptEventHandler(NRF24L01_IRQ_OnInterrupt);

        // Initialize Chip Enable Port
        NRF24L01_ChipEnable = new OutputPort(EmbeddedMaster.Pins.E11x, false);
        Thread.Sleep(100);
        
        // Disable RX/TX
        NRF24L01.DisableRXTX();
    }

    /// <summary>
    /// Configure the nRF24L01 as a simple 1 byte transmitter and reciever 
    /// </summary>
    public static void Configure()
    {
        // Setup, CRC enabled, Power Up, PRX
        NRF24L01.SendCommand(NRF24L01Commands.W_REGISTER, NRF24L01Registers.CONFIG, new byte[] { (byte)((1 << NRF24L01Mnemonics.EN_CRC) | (1 << NRF24L01Mnemonics.PWR_UP) | (1 << NRF24L01Mnemonics.PRIM_RX)) });

        // Write transmit adres to RX_ADDRESS_P0 (Pipe0) (For Auto ACK)
        NRF24L01.SendCommand(NRF24L01Commands.W_REGISTER, NRF24L01Registers.RX_ADDR_P0, TX_Adress);

        // Write transmit adres to TX_ADDR register. 
        NRF24L01.SendCommand(NRF24L01Commands.W_REGISTER, NRF24L01Registers.TX_ADDR, TX_Adress);

        // Write recieve adres to RX_ADDRESS_P1 (Pipe1)
        NRF24L01.SendCommand(NRF24L01Commands.W_REGISTER, NRF24L01Registers.RX_ADDR_P1, RX_Adress);

        // Set Reciever, Pipe0 Payload size 
        NRF24L01.SendCommand(NRF24L01Commands.W_REGISTER, NRF24L01Registers.RX_PW_P0, new byte[] { 1 });

        // Set Reciever, Pipe0 Payload size 
        NRF24L01.SendCommand(NRF24L01Commands.W_REGISTER, NRF24L01Registers.RX_PW_P1, new byte[] { 1 });

        // Setup, CRC enabled, Power Up, PRX
        NRF24L01.SendCommand(NRF24L01Commands.W_REGISTER, NRF24L01Registers.CONFIG, new byte[] { (byte)((1 << NRF24L01Mnemonics.EN_CRC) | (1 << NRF24L01Mnemonics.PWR_UP) | (1 << NRF24L01Mnemonics.PRIM_RX)) });

        // Enable RX/TX
        NRF24L01.EnableRXTX();

        // Clear IRQ Masks
        NRF24L01.SendCommand(NRF24L01Commands.W_REGISTER, NRF24L01Registers.STATUS, new byte[] { 0x70 });

        // Flush RX FIFO
        NRF24L01.SendCommand(NRF24L01Commands.FLUSH_RX, 0x00, new byte[] { });
    }


    /// <summary>
    /// Interrupt handler for NRF24L01_IRQ Interrupt port
    /// </summary>
    /// <param name="port"></param>
    /// <param name="state"></param>
    /// <param name="time"></param>
    static void NRF24L01_IRQ_OnInterrupt(Cpu.Pin port, bool state, TimeSpan time)
    {
        // Get status byte
        byte[] status = NRF24L01.SendCommand(NRF24L01Commands.R_REGISTER, NRF24L01Registers.STATUS, new byte[1]);

        // Default we want to process interrupt
        bool StopDefaultInterrupt = false;

        // Trigger event
        if (OnInterrupt != null)
        {
            OnInterrupt(status[0],ref StopDefaultInterrupt);
        }

        // Check if we have to proces the interrupt
        if (StopDefaultInterrupt)
            return;

        // Check for Data in FIFO flag
        if ((status[0] & (1 << NRF24L01Mnemonics.RX_DR)) != 0)
        {
            // Disable RX/TX
            NRF24L01.DisableRXTX();

            // Read payload data
            byte[] payload = NRF24L01.SendCommand(NRF24L01Commands.R_RX_PAYLOAD, 0x00, new byte[1]);

            // Clear RX_DR bit 
            NRF24L01.SendCommand(NRF24L01Commands.W_REGISTER, NRF24L01Registers.STATUS, new byte[] { (byte)(1 << NRF24L01Mnemonics.RX_DR) });

            // Re-Enable RX/TX
            NRF24L01.EnableRXTX();

            // Trigger event
            if (OnDataRecieved != null)
            {
                OnDataRecieved(payload[1],status[0]);
            }
        }

        // Check for Max retries
        if ((status[0] & (1 << NRF24L01Mnemonics.MAX_RT)) != 0)
        {
            // Clear MAX_RT bit in status register
            NRF24L01.SendCommand(NRF24L01Commands.W_REGISTER, NRF24L01Registers.STATUS, new byte[] { (byte)(1 << NRF24L01Mnemonics.MAX_RT) });

            // Flush TX FIFO 
            NRF24L01.SendCommand(NRF24L01Commands.FLUSH_TX, 0x00, new byte[] { });

            // Trigger event
            if (OnTransmit != null)
            {
                OnTransmit(false,status[0]);
            }
        }

        // Check for Transmit buffer full
        if ((status[0] & (1 << NRF24L01Mnemonics.TX_FULL)) != 0)
        {
            // Flush TX FIFO 
            NRF24L01.SendCommand(NRF24L01Commands.FLUSH_TX, 0x00, new byte[] { });

            // Trigger event
            if (OnTransmit != null)
            {
                OnTransmit(false,status[0]);
            }
        }

        // Check for Data Send 
        if ((status[0] & (1 << NRF24L01Mnemonics.TX_DS)) != 0)
        {
            // Clear TX_DS bit in status register
            NRF24L01.SendCommand(NRF24L01Commands.W_REGISTER, NRF24L01Registers.STATUS, new byte[] { (byte)(1 << NRF24L01Mnemonics.TX_DS) });

            // Trigger event
            if (OnTransmit != null)
            {
                OnTransmit(true,status[0]);
            }
        }
    }

    /// <summary>
    /// Send 1 byte to TX_Adress
    /// </summary>
    /// <param name="b">byte to send</param>
    public static void SendByte(byte b)
    {
        // Chip enable low
        NRF24L01.DisableRXTX();

        // Setup, CRC enabled, Power Up, TRX
        NRF24L01.SendCommand(NRF24L01Commands.W_REGISTER, NRF24L01Registers.CONFIG, new byte[] { (byte)((1 << NRF24L01Mnemonics.EN_CRC) | (1 << NRF24L01Mnemonics.PWR_UP)) });

        // Send payload
        NRF24L01.SendCommand(NRF24L01Commands.W_TX_PAYLOAD, 0x00, new byte[] { b });

        // Pulse for CE -> starts the transmission.
        NRF24L01.EnableRXTX();
        
    }


    /// <summary>
    /// Simple routine to perform byte -> Hex string
    /// </summary>
    /// <param name="b">byte</param>
    /// <returns>Hex string</returns>
    public static String ByteToHex(byte b)
    {
        char[] Nibble = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
        String hex = "0x";

        hex += Nibble[(b >> 4)].ToString();
        hex += Nibble[(b & 0x0F)].ToString();

        return hex;
    }


    /// <summary>
    /// Send Command to NRF24L01
    /// </summary>
    /// <param name="Command">Command to Send (NRF24L01Commands)</param>
    /// <param name="Addres">Addres/Register to write to (NRF24L01Registers)</param>
    /// <param name="Data">Data to write</param>
    /// <returns>Byte array with sizeof(Data)+1 rows. row 0 is status register from NRF24L01</returns>
    public static byte[] SendCommand(byte Command, byte Addres, byte[] Data)
    {
        // Create SPI Buffers with Size of Data + 1 (For Command)
        byte[] WriteBuffer = new byte[Data.Length + 1];
        byte[] ReadBuffer = new byte[Data.Length + 1];

        // Add command and adres to SPI buffer
        WriteBuffer[0] = (byte)(Command | Addres);

        // Add data to SPI buffer
        Array.Copy(Data, 0, WriteBuffer, 1, Data.Length);

        // Do SPI Read/Write
        SpiPort.WriteRead(WriteBuffer, ReadBuffer);

        // Return ReadBuffer
        return ReadBuffer;
    }

}

