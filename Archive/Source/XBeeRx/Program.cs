﻿using System;
using System.Threading;
using System.Text;
using System.IO.Ports;

using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;

using GHIElectronics.NETMF.FEZ;
using GHIElectronics.NETMF.Hardware;

namespace XBeeRx
{
    public class Program
    {
        public static int read_count = 0;
        public static byte[] rx_data = new byte[10];
        public static byte[] tx_data;
        public static SerialPort UART = null;

        public static void Main()
        {
            bool ledState = false;
            OutputPort led = new OutputPort((Cpu.Pin)FEZ_Pin.Digital.LED, ledState);

            for (int i = 0; i <= 5; i++)
            {
                led.Write(!ledState);
                ledState = !ledState;
                Thread.Sleep(200);
            }


            //UART = new SerialPort("COM2", 115200);
            UART = new SerialPort("COM1", 115200, Parity.None, 8, StopBits.One);
            UART.Open();
            UART.DataReceived += new SerialDataReceivedEventHandler(UART_DataReceived);

            Thread.Sleep(Timeout.Infinite);
        }

        private static void UART_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            bool ledState = false;
            OutputPort led = new OutputPort((Cpu.Pin)FEZ_Pin.Digital.LED, ledState);

            for (int i = 0; i <= 2; i++)
            {
                led.Write(!ledState);
                ledState = !ledState;
                Thread.Sleep(500);
            }


            // read the data
            read_count = UART.Read(rx_data, 0, UART.BytesToRead);
            if (read_count != 1)
            {
                // we sent 6 so we should have 6 back
                Debug.Print("Wrong size: " + read_count.ToString());
            }
            else
            {
                Debug.Print("Perfect data!");
                String receivedDataAsString = new String(Encoding.UTF8.GetChars(rx_data));
                Debug.Print(receivedDataAsString);
            }

            // LED
            for (int i = 0; i <= 200; i++)
            {
                led.Write(!ledState);
                ledState = !ledState;
                Thread.Sleep(50);
            }

            Thread.Sleep(100);
        }
    }
}
