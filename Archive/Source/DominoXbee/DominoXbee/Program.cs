﻿using System;
using System.Threading;
using System.Text;
using System.IO.Ports;

using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;

using GHIElectronics.NETMF.FEZ;
using GHIElectronics.NETMF.Hardware;

namespace DominoXbee
{
    public class Program
    {
        public static int read_count = 0;
        public static byte[] rx_data = new byte[10];
        public static byte[] tx_data;
        public static SerialPort UART = null;
        public static void Main()
        {
            UART = new SerialPort("COM1", 115200);
            UART.Open();
            UART.DataReceived += new SerialDataReceivedEventHandler(UART_DataReceived);

            Thread.Sleep(Timeout.Infinite);
        }

        private static void UART_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            // read the data
            read_count = UART.Read(rx_data, 0, UART.BytesToRead);
            if (read_count != 6)
            {
                // we sent 6 so we should have 6 back
                Debug.Print("Wrong size: " + read_count.ToString());
            }
            else
            {
                Debug.Print("Perfect data!");
                String receivedDataAsString = new String(Encoding.UTF8.GetChars(rx_data));
                Debug.Print(receivedDataAsString);
            }
            Thread.Sleep(100);
        }
    }
}
