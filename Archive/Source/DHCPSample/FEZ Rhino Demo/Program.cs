﻿using System;
using System.Threading;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using GHIElectronics.NETMF.FEZ;
using GHIElectronics.Graphics.Simple128x64;
using GHIElectronics.NETMF.Net;
using Microsoft.SPOT.Hardware;
using GHIElectronics.NETMF.Net;
using GHIElectronics.NETMF.Net.NetworkInformation;

namespace FEZ_Rhino_Demo
{
    public class Program
    {
        public static void BlinkLED()
        {
            // Blink board LED

            bool ledState = false;

            OutputPort led = new OutputPort((Cpu.Pin)FEZ_Pin.Digital.LED, ledState);

            while (true)
            {
                // Sleep for 500 milliseconds
                Thread.Sleep(500);

                // toggle LED state
                ledState = !ledState;
                led.Write(ledState);
            }
        }
        public static void Main()
        {
            Thread LEDThread = new Thread(BlinkLED);
            LEDThread.Start();
            // Initialize the display
            FEZ_Extensions.Graphical128x64Display.Initialize(FEZ_Pin.Digital.UEXT3, FEZ_Pin.Digital.UEXT4, FEZ_Pin.Digital.UEXT10, SPI.SPI_module.SPI2);

            byte[] mac = { 43, 185, 44, 2, 206, 120 };
            WIZnet_W5100.Enable(SPI.SPI_module.SPI1, (Cpu.Pin)FEZ_Pin.Digital.IO38, (Cpu.Pin)FEZ_Pin.Digital.IO67, true); // WIZnet interface on FEZ Connect
           

            // Now draw using the graphics drivers
            GPainter paint = new GPainter();
            //drawing all happen in VRAM no on display
            paint.Clear();
            // Print some text directly on the screen
            paint.Line(0, 0, 127, 0);
            paint.Line(0, 63, 127, 63);
            paint.Line(0, 0, 0, 63);
            paint.Line(127, 0, 127, 63);
            paint.Print(5,10, "GHI Electronics LLC");
            paint.Print(5,30, "     FEZ Rhino");
            paint.Print(5,50, "Acquiring IP address");
            paint.Cirlcle(13, 13, 10);
            FEZ_Extensions.Graphical128x64Display.Flush(paint.vram);

            try
            {
                Dhcp.EnableDhcp(mac);
                Debug.Print("Network settings:");
                Debug.Print("IP Address: " + new IPAddress(NetworkInterface.IPAddress).ToString());
                Debug.Print("Subnet Mask: " + new IPAddress(NetworkInterface.SubnetMask).ToString());
                Debug.Print("Default Getway: " + new IPAddress(NetworkInterface.GatewayAddress).ToString());
                Debug.Print("DNS Server: " + new IPAddress(NetworkInterface.DnsServer).ToString());
                paint.Print(5, 50,"   "+ new IPAddress(NetworkInterface.IPAddress).ToString()+"   ");

            }
            catch
            {
                paint.Print(5, 50, " DHCP not available ");
            }
            FEZ_Extensions.Graphical128x64Display.Flush(paint.vram);


            
            Thread.Sleep(-1);
            
        }

    }
}
