﻿using System;
using IP = System.Net.IPAddress;
using System.IO;
using System.Threading;
using Microsoft.SPOT;
using GHIElectronics.NETMF.FEZ;
using GHIElectronics.NETMF.Net;
using GHIElectronics.NETMF.Hardware;
using GHIElectronics.NETMF.IO;
using GHIElectronics.NETMF.System;
using Microsoft.SPOT.Hardware;
//using Microsoft.SPOT.Hardware.SerialPort;
using Microsoft.SPOT.IO;
using Microsoft.SPOT.Net;
using Socket = GHIElectronics.NETMF.FEZ.FEZ_Shields.Ethernet.uSocket;

namespace TestEthernet
{
    public class TestEthernet
    {
        public static void Main()
        {
            IP ip_addr, ip_mask, ip_gateway;

            ip_addr = IP.Parse("192.168.3.11");
            ip_mask = IP.Parse("255.255.255.0");
            ip_gateway = IP.Parse("192.168.3.1");  
            byte[] MAC = new byte[6] {0xAA,0x00,0x04,0x0E,0x0F,0x99};
            FEZ_Shields.Ethernet.Initialize(ip_addr.GetAddressBytes(), ip_mask.GetAddressBytes(), ip_gateway.GetAddressBytes(), MAC);
            Socket s;
            s = new Socket(Socket.Protocol.TCP, 32);
            byte[] buf = new byte[255];
            int n;
            System.Text.UTF8Encoding enc;
            enc = new System.Text.UTF8Encoding();
            while (true)
            {
                String line = "";
                s.Listen();
                while (s.GetStatus() != Socket.SR_val.SOCK_ESTABLISHED) 
                { }
                while (s.GetStatus() == Socket.SR_val.SOCK_ESTABLISHED)
                {
                    Array.Clear(buf, 0, 255);
                    n = s.Receive(buf, s.AvilableBytes);
                    if (n > 0)
                    {
                        try
                        {
                            char[] chars = enc.GetChars(buf);
                            line = line + new String(chars);

                        }
                        catch (Exception e) { Debug.Print("error"); }

                        if (buf[n - 1] == 0x0A || buf[n - 1] == 0x0D)
                        {
                            if (line.Length >= 5 && line.Substring(0, 5) == "$QUIT")
                            {
                                goto done;
                            }
                            line = line + "\n";
                            s.Send(enc.GetBytes(line), line.Length - 1);
                            line = "";
                        }
                    }

                }

            done:
                s.Disconnect();
                s.Close();
                s = new Socket(FEZ_Shields.Ethernet.uSocket.Protocol.TCP, 32);
            }
        }

    }
}

