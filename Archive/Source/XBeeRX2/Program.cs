﻿using System;
using System.Threading;
using System.Text;
using System.IO.Ports;

using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;

using GHIElectronics.NETMF.FEZ;
using GHIElectronics.NETMF.Hardware;

namespace XBeeRx
{
    public class Program
    {
        public static SerialPort UART = null;

        public static void Main()
        {
            UART = new SerialPort("COM1", 9600, Parity.None, 8, StopBits.One);
            UART.Open();
            Thread CommThread = new Thread(ReadComm);
            //UART.DataReceived += new SerialDataReceivedEventHandler(UART_DataReceived);
            CommThread.Start();

            Thread.Sleep(Timeout.Infinite);
        }

        private static void ReadComm()
        {
            int NumOfBytes2Read = 0;

            while (true)
            {
                NumOfBytes2Read = UART.BytesToRead;
                byte[] rx_data = new byte[NumOfBytes2Read];

                if (NumOfBytes2Read > 0)
                {
                    UART.Read(rx_data, 0, NumOfBytes2Read);

                    String receivedDataAsString = new String(Encoding.UTF8.GetChars(rx_data));
                    Debug.Print(NumOfBytes2Read + " - " + receivedDataAsString);
                }

                //UART.Flush();
                Thread.Sleep(250);
            }
        }



    }
}
