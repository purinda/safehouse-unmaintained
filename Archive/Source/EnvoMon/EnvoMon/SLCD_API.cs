using System.Text;
using System.IO.Ports;
using Microsoft.SPOT;

/*
 *  SLCD API
 *      Coded by Chris Seto 
 *      
 * This is an API to simplify operations involving the MeLabs SLCD
 *
 * */
namespace SLCD_API
{
	class SLCD
	{
        /*
         *  public class SerialBuffer
         *  
         * This class is used to cache and send serial data in bursts.
         * */
		public class SerialBuffer
		{
			private SerialPort shandle;
			private byte[] buffer = new byte[0];

			public SerialBuffer(SerialPort serial)
			{
				shandle = serial;
			}

			public void add(byte[] inc)
			{
				buffer = concatByteArray(buffer, buffer.Length, inc, inc.Length);
			}


			/*
			 *	private byte[] concatByteArray(byte[] a, int aSize, byte[] b, int bSize)
			 *	
			 * Concat the incoming array to the end of the buffer
			 * */
			private byte[] concatByteArray(byte[] a, int aSize, byte[] b, int bSize)
			{
				byte[] buff = new byte[aSize + bSize];

				for (int i = 0; i < aSize; i++)
					buff[i] = a[i];

				for (int i = 0; i < bSize; i++)
					buff[i + aSize] = b[i];

				return buff;
			}

			/*
			 *	public void commit(void)
			 *	
			 * Write the buffer to serial
			 * */
			public void commit()
			{
				// Write to the serial port
				shandle.Write(buffer, 0, buffer.Length);

				// Clear out the buffer
				clear();
			}

			public void clear()
			{
				buffer = new byte[0];
			}
		} // END private class SerialBuffer


        /* 
         *	public class Line
         *	
         *	This class will keep track of individual line operations
         * */
        public class Line
		{
            // If true, this will cause the previous write to be cleared.
            // For basic use, leave this true.
			public bool fillLine		    = true;

            // If true, this will cause the lien to be FULLY CLEARED on every write.
            // This is a bandwidth hog, so leave it false unless you know what
            // you are doing.
            public bool fullClear           = false;

            // This is the text displayed on this line fo the LCD
            public string Text
            {
                set
                {
                    // We have to save this until the end of the function
                    string llast = value;

                    // Check if we are going to overflow the line
                    if (value.Length > 20)
                        value = value.Substring(0, 20);
                    else
                    {
                        // We only need to fill the line if the line is less than 20 chars.

                        if (fullClear)
                        {
                            int fillLen = 20 - value.Length;

                            for (int i = 1; i <= fillLen; i++)
                                value += " ";
                        }
                        else
                        {
                            // Clean up garbage left over by the last write
                            if (fillLine)
                            {
                                if (last.Length > value.Length)
                                {
                                    int fillLen = last.Length - value.Length;

                                    for (int i = 1; i <= fillLen; i++)
                                        value += " ";
                                }
                            }
                        }
                    }

                    // Seek to requested line
                    sb.add(new byte[2] { 254, line });

                    // Append the text to the end of the buffer and commit the buffer
                    sendStr(value);
                    sb.commit();

                    // This is used later to figure out the most
                    // efficient way to overwrite old data
                    last = llast;
                }
            }

            // Line seek
			private byte line;

            // SerialBuffer object to use for writes
			private SerialBuffer sb;

            // Last line write
			private string last = string.Empty;

			public Line(byte line, SerialBuffer sb)
			{
				this.line  = line;
				this.sb    = sb;
			}

			public void clear()
			{
				Text = "                    ";
			}

            private void sendStr(string str)
			{
				byte[] bytes = Encoding.UTF8.GetBytes(str);
				sb.add(bytes);
			}
		} // END public class Line

		public Line[] Lines = new Line[4];
		private SerialPort lcdHandle;
		private SerialBuffer sb;


        // Sets the backlight to a level between (none) 0-255 (full)
		public int backlight
		{
			set
			{
				sb.add(new byte[3] { 27, 42, (byte)value });
				sb.commit();
			}
		}

		/*
		 *	public SLCD SLCD(string comport)
		 *	
		 * Construct the object. Set up serial port/buffer, lines, etc.
		 * */
		public SLCD(string comPort)
		{
			// Create and open the serial port
			lcdHandle = new SerialPort(comPort, 9600);
			lcdHandle.Open();

			// Create the serial buffer
			sb = new SerialBuffer(lcdHandle);

			// Create a line object for each line
			Lines[0] = new Line((byte)128, sb);
			Lines[1] = new Line((byte)192, sb);
			Lines[2] = new Line((byte)148, sb);
			Lines[3] = new Line((byte)212, sb);
		}

		/*
		 *	public void clear(void)
		 *	
		 * Clear the entire LCD
		 * */
		public void clear()
		{
			sb.add(new byte[2] { 254, 1 });
			sb.commit();
		}
	}
}