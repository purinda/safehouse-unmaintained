﻿using System;
using System.Text;
using System.Threading;
using GHIElectronics.NETMF.FEZ;
using GHIElectronics.NETMF.Hardware;
using Microsoft.SPOT.Hardware;
using SLCD_API;
using Microsoft.SPOT;

namespace EnvoMon
{
	public class Program
	{
		// Objects 
		private static InterruptPort motion;
		private static PWM buzzer;
		private static FEZ_Shields.Ethernet.uSocket socket;
		private static SLCD LCD;
		private static OneWire owtemp = new OneWire((Cpu.Pin)FEZ_Pin.Digital.Di6);

		// Network properties
		private static byte[] ip		= { 192 ,168, 15, 4 };
		private static byte[] subnet	= { 255, 255, 255, 0 };
		private static byte[] gateway	= { 192, 168, 15, 1 };
		private static byte[] mac		= { 43, 185, 44, 2, 206, 127 };
		private static byte[] server	= { 192, 168, 15, 123 };
		private static string path		= "fezmon/";

		// Others
		private static double debounce_motion		= getTimeStamp();
		private static int tempThreshold			= 85;
		private static int checkInInterval			= 300;
		private static bool motionEnabled			= true;
		private static string motionMsg				= "Motion Detect On";
		private static string serverID				= "...";
		private static bool eNetInUse				= false;


		public static void Main()
		{
            // Initialize the system
            init();

			// Init done. Switch to operations loop
			// This loop may return in the event of a critical error
			loop();
		}

		/// <summary>
		/// Initalize the system
		/// </summary>
        private static void init()
        {
            // First thing to do is bring the LCD up
            LCD = new SLCD("COM1");
            LCD.clear();
            LCD.Lines[0].Text = "=== Envo Monitor ===";
			LCD.Lines[1].Text = "====================";
			LCD.Lines[2].Text = "====================";
			LCD.Lines[3].Text = "====================";

            // Fade the backlight up...
            for (int i = 0; i <= 255; i++)
            {
                LCD.backlight = i;
                Thread.Sleep(5);
            }


            // Init the Ethernet shield 
            FEZ_Shields.Ethernet.Initialize(ip, subnet, gateway, mac);
            socket = new FEZ_Shields.Ethernet.uSocket(FEZ_Shields.Ethernet.uSocket.Protocol.TCP, 80);
            LCD.Lines[3].Text = "Ethernet Configured";
            Thread.Sleep(100);

            // CheckIn to the server
            if (!checkIn())
                return;

            LCD.Lines[3].Text = "Server: " + serverID;

            // Init buzzer
            buzzer = new PWM((PWM.Pin)FEZ_Pin.PWM.Di5);
            buzzer.Set(0, (byte)0);


            // Create the Interrupt object and and add an interrupt event
            motion = new InterruptPort((Cpu.Pin)FEZ_Pin.Interrupt.Di7, false, Port.ResistorMode.Disabled, Port.InterruptMode.InterruptEdgeLow);
            motion.OnInterrupt += new NativeEventHandler(capMotion);
            LCD.Lines[3].Text = "Interrupt Configured";
            Thread.Sleep(100);

            // Init temp sensor
          //  owtemp = new OneWire((Cpu.Pin)FEZ_Pin.Digital.Di6);

            LCD.Lines[3].Text = "Ready!";
        }


		/// <summary>
		/// This is used to check into the server to download 
		/// the system configuration
		/// </summary>
		/// <returns></returns>
        public static bool checkIn()
		{
			string response = sendHTTPRequest(server, 80, path +"?fezcoms&checkin");
			string[] rparts = response.Split(';');

			if (rparts[0] == "OK")
			{
				LCD.Lines[3].Text = "CheckIn OK";

				serverID		= rparts[1];
				tempThreshold   = int.Parse(rparts[3]);
				checkInInterval = int.Parse(rparts[2]);

				if (rparts[4] == "true")
				{
					motionEnabled = true;
					motionMsg = "Motion Detect On";
				}
				else
				{
					motionEnabled = false;
					motionMsg = "Motion Detect Off";
				}


				Thread.Sleep(1000);
				LCD.Lines[1].Text = motionMsg;
				return true;
			}
			else
			{
				LCD.clear();
				Thread.Sleep(100);
				LCD.Lines[3].Text = "CheckIn Failed";
                Debug.Print(response);
				return false;
			}
		}

		/// <summary>
		/// This method will be used to monitor the system
		/// in an infinate loop
		/// </summary>
		public static void loop()
		{
			// Used by the CheckIn function
			int seconds   = 0;
			// Used to store which status line is being displayed
			int curStatus = 0;

			int untilTempCheckin = 0;

			while (true)
			{
				// Temp
				string tempStatus = "";
				int temp = (int)System.Math.Round(getTemp());
				if (temp >= tempThreshold)
				{
					tempStatus = " !WARN!";
					// TODO: Contact server
				}
				else
					tempStatus = " OK";

				LCD.Lines[2].Text = "Temp: "+ temp +"F"+ tempStatus;

				if (untilTempCheckin++ >= 1800)
				{
					sendHTTPRequest(server, 80, "TempTrack/?Temp="+ temp.ToString());
					untilTempCheckin = 0;
				}

				// ReCheckIn
				if (++seconds >= checkInInterval)
				{
					for (int i = 0; i <= 10; i++)
					{
						if (checkIn())
							break;
						else
						{
							if (i >= 10)
								return;
						}
					}

					seconds = 0;
				}

				// Every two seconds, switch whatever is displayed on the bottom line
				if ((seconds % 2) == 0)
				{
					if (curStatus++ > 1)
						curStatus = 0;

					switch (curStatus)
					{
						// Server ID
						case 0:
							LCD.Lines[3].Text = "Server: "+ serverID;
							break;

						// Time until next CheckIn
						case 1:
							LCD.Lines[3].Text = (int)((((checkInInterval - seconds) / 60)+0.005)*100.0)/100.0 + " Min To CheckIn";
							break;
					}
				}

				Thread.Sleep(1000);
			}
		}

		/// <summary>
		/// Use the Ethernet shield to send an HTTP request
		/// </summary>
		/// <param name="server"></param>
		/// <param name="port"></param>
		/// <param name="url"></param>
		/// <returns></returns>
		public static string sendHTTPRequest(byte[] server, ushort port, string url)
		{
			// Wait until enet is no longer in use
			while (eNetInUse)
				Thread.Sleep(10);

			eNetInUse = true;

			// Connect to the server
			socket.Connect(server, port);

			// Get a byte array for the GET request and send it
			byte[] sendB = Encoding.UTF8.GetBytes("GET /"+ url +"\r\nHTTP/1.0\r\n\r\n");
			socket.Send(sendB, sendB.Length);
			
			// Recive the HTTP response and cast the byte[] into a string
			byte[] recv = new byte[socket.AvilableBytes];
			socket.Receive(recv, recv.Length);

			// Disconnect and return the HTTP response (minus headers)
			socket.Disconnect();
			eNetInUse = false;
			return bytesToString(recv);
		}

		/// <summary>
		/// Convert a byte array to string
		/// </summary>
		/// <param name="bytes"></param>
		/// <returns></returns>
        public static string bytesToString(byte[] bytes)
		{
			string s = "";

			for (int i = 0; i < bytes.Length; ++i)
				s += (char)bytes[i];

			return s;
		}

		
		/// <summary>
		/// Get temp from sensor
		/// </summary>
		/// <returns></returns>
        public static float getTemp()
		{
			ushort temp;
			float tempf = 0;

			if (owtemp.Reset())
			{
				owtemp.WriteByte(0xCC);
				owtemp.WriteByte(0x44);

				if (owtemp.ReadByte() == 0)
				{
					owtemp.Reset();
					owtemp.WriteByte(0xCC);
					owtemp.WriteByte(0xBE);

					temp = owtemp.ReadByte();
					temp |= (ushort)(owtemp.ReadByte() << 8);

					tempf = (float)((1.80 * (temp / 16.00)) + 32.00);
				}
			}

			return tempf;
		}

		/// <summary>
		/// Handle a motion detected event
		/// </summary>
		/// <param name="port"></param>
		/// <param name="state"></param>
		/// <param name="time"></param>
		public static void capMotion(uint port, uint state, DateTime time)
		{
			// Make sure we aren't sending too many requests by debouncing
			if (((getTimeStamp() - debounce_motion) > 5) && motionEnabled)
			{
				// Buzzer on
				buzzer.Set(1000, (byte)1);
				LCD.Lines[1].Text = "Motion Detected";

				// Tell the server that something moved
				// Make sure that the request is OK'd by the server
				if (sendHTTPRequest(server, 80, path +"?motion") != "REG_OK")
				{
					LCD.Lines[1].Text = "Server Error";
					Thread.Sleep(1000);
				}

				// Set the debounce timestamp now
				debounce_motion = getTimeStamp();
				Thread.Sleep(1000);

				// Turn off the buzzer and resume normal operation
				buzzer.Set(0, (byte)0);
				LCD.Lines[1].Text = motionMsg;
			}
		}


		/// <summary>
		/// Get the current UNIX Timestamp
		/// </summary>
		/// <returns></returns>
        public static double getTimeStamp()
		{
			TimeSpan diff = DateTime.Now - new DateTime(1970, 1, 1, 0, 0, 0, 0);
			return diff.Seconds + (diff.Hours * 60 *60) + (diff.Minutes * 60);
		}
	}
}
