﻿using System;
using System.Text;
using System.Threading;
using System.IO.Ports;
using Microsoft.SPOT.Hardware;
using GHIElectronics.NETMF.FEZ;

namespace XbeeTx
{
    public class Program
    {
        public static void Main()
        {
            // create serial port for XBEE. Connect XBEE_RX to Di8 and XBEE_TX to Di7.
            SerialPort xbee = new SerialPort("COM1", 9600, Parity.None, 8, StopBits.One);
            xbee.Open();
            // create "Hello!" string as bytes
            byte[] helloBytes = Encoding.UTF8.GetBytes("Hello pati this is me :)  am I still in the range? ");
            // send string every second
            FEZ_Pin.Digital.LED;

            bool ledState = false;
            OutputPort led = new OutputPort((Cpu.Pin)FEZ_Pin.Digital.LED, ledState);

            while (true)
            {
                xbee.Flush();
                xbee.DiscardOutBuffer();
                xbee.Write(helloBytes, 0, helloBytes.Length);
                Thread.Sleep(2000);

                // indicator to see activity
                // toggle LED state
                ledState = !ledState;
                led.Write(ledState);
            }
        }
    }
}