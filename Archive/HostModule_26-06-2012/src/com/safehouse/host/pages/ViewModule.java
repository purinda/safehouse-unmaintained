/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.safehouse.host.pages;

import freemarker.template.Template;
import freemarker.template.*;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.safehouse.host.*;
import java.util.Arrays;
import com.safehouse.host.CommonHelper.*;

import com.safehouse.host.HardwareInterface.Module;
import com.safehouse.host.HardwareInterface.Module.Ability;
import java.util.*;
import javax.print.attribute.standard.DateTimeAtProcessing;
/**
 *
 * @author Purinda Gunasekara
 */
public class ViewModule extends CommonPage {
	
	HardwareInterface hal = HardwareInterface.getInstance();
	
	// Create a data-model 
	private String pageTitle = "Module View";
	private String htmlContent = "";
	private String moduleId = null;
	
	private Properties pageParameters = null;

	public ViewModule (Configuration cfg, Properties _params, String method)
	{
		this.pageParameters = _params;
		
		try {
			// Parse template (a html at this point)
			Template temp = cfg.getTemplate("www/viewModule.html");
			Map root = new HashMap();		
			// Set the id as global
			moduleId = this.getUrlModuleId();
			
			Module m = getModule(moduleId);
			root.put("id", m.id);			
			root.put("title", m.name);
			root.put("type", m.type);		
			root.put("abilities", this.getAbilities());
				
			// If POST exist
			if (method.equalsIgnoreCase("POST"))
			{
				this.processPost();
			}
			else if (method.equalsIgnoreCase("GET"))
			{
				StringWriter out = new StringWriter();		
				temp.process(root, out);
				this.htmlContent = out.toString();
			}
			
						
		} catch (TemplateException ex) {
			Logger.getLogger(Index.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(Index.class.getName()).log(Level.SEVERE, null, ex);
		}

	}
	
	private boolean processPost()
	{	
		Iterator eleKeys = this.pageParameters.keySet().iterator();
		Iterator eleValues = this.pageParameters.values().iterator();
				
		String action = null;
		String ability = null;
		String value = null;
		
		while (eleKeys.hasNext()) {
			// Get post variables
			Object eKey = eleKeys.next();
			Object eVal = eleValues.next();
			
			String postKey = eKey.toString().toUpperCase();	
			String postVal = eVal.toString().toUpperCase();	
			
			if (postKey.equalsIgnoreCase("ACTION"))
				action = postVal; 
			if (postKey.equalsIgnoreCase("ID"))
				this.moduleId = postVal;
			else if (postKey.equalsIgnoreCase("ABILITY"))
				ability = postVal; 
			else if (postKey.equalsIgnoreCase("VALUE"))
				value = postVal; 

		}
		
		// Send commands to H/W interface
		if (action.equalsIgnoreCase("CONTROL"))
		{
			// Debug
			//System.out.println(id);			
			//System.out.println(ability);			
			//System.out.println(value);		
			Iterator abilities = hal.getModuleById(this.moduleId).abilities.iterator();
			while (abilities.hasNext())
			{
				Ability ability_ = (Ability) abilities.next();
				
				if (ability_.type.equalsIgnoreCase("SWITCH"))
				{
					
					if (value.equalsIgnoreCase("TRUE"))
						ability_.control(1);
					else
						ability_.control(0);
				}
			}
		}
		
		return true;
	}
	
	private String getUrlModuleId()
	{
		Iterator eleKeys = pageParameters.keySet().iterator();
		Iterator eleValues = pageParameters.values().iterator();
		String id = null;
		
//System.out.println("DEBUG");
		
		while (eleKeys.hasNext()) {
			// Get post variables
			Object eKey = eleKeys.next();
			Object eVal = eleValues.next();

			if (eKey.toString().equalsIgnoreCase("id"))
			{					
				id = eVal.toString();
			}
		}		
//System.out.println("ID : " + Integer.toString(id));			
		return id;
	}
	
    private Module getModule(String id) 
	{
		HardwareInterface hal = HardwareInterface.getInstance();
		// Init modules
		hal.getConnectedModules();
		return hal.getModuleById(id);
	}

    public Map getAbilities() 
	{
		Map m = new HashMap();
		Module module = this.getModule(this.moduleId);
		// Retrive the list of currently connectd modules to host 
		m.put("items", module.abilities);
		return m;
	}
	

	public String getHtmlContent()
	{
		return this.htmlContent;
	}
	
}



//    public static class Context {
//        public Map getListOfModules() {
//            Map m = new HashMap();
//			
//			Man men[] = new Man[5];
//			for (int i = 0; i < 5; i++) {
//				men[i] = new Man();
//				men[i].first_name = "Name " + Integer.toString(i);
//			}
//            m.put("man", Arrays.asList(men));
//            return m;
//        }
//    }
//
//    public static class Man {
//        private String first_name = "first";
//
//        public String getFirst_name() {
//            return first_name;
//        }
//
//        public void setFirst_name(String first_name) {
//            this.first_name = first_name;
//        }
//    }
//	
	