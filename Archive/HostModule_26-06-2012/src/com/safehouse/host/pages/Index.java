/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.safehouse.host.pages;

import freemarker.template.Template;
import freemarker.template.*;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.safehouse.host.*;
import java.util.Arrays;

import com.safehouse.host.HardwareInterface.Module;
import java.util.Properties;
/**
 *
 * @author Purinda Gunasekara
 */
public class Index extends CommonPage {
	
	// Create a data-model 
	private String pageTitle = "Safehouse";
	private String htmlContent = "";

	public Index (Configuration cfg, Properties params, String method)
	{
		try {
			// Parse template (a html at this point)
			Template temp = cfg.getTemplate("www/index.html");
			Map root = new HashMap();
			root.put("title", this.pageTitle);
			root.put("modules", this.getListOfModules());
			
			StringWriter out = new StringWriter();		
			temp.process(root, out);
			this.htmlContent = out.toString();
						
		} catch (TemplateException ex) {
			Logger.getLogger(Index.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(Index.class.getName()).log(Level.SEVERE, null, ex);
		}

	}

    public Map getListOfModules() 
	{
		Map m = new HashMap();
		HardwareInterface hal = HardwareInterface.getInstance();
		// Retrive the list of currently connectd modules to host 
		m.put("items", Arrays.asList(hal.getConnectedModules()));
		return m;
	}


	public String getHtmlContent()
	{
		return this.htmlContent;
	}
	
}



//    public static class Context {
//        public Map getListOfModules() {
//            Map m = new HashMap();
//			
//			Man men[] = new Man[5];
//			for (int i = 0; i < 5; i++) {
//				men[i] = new Man();
//				men[i].first_name = "Name " + Integer.toString(i);
//			}
//            m.put("man", Arrays.asList(men));
//            return m;
//        }
//    }
//
//    public static class Man {
//        private String first_name = "first";
//
//        public String getFirst_name() {
//            return first_name;
//        }
//
//        public void setFirst_name(String first_name) {
//            this.first_name = first_name;
//        }
//    }
//	
	