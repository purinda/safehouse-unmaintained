package com.safehouse.host;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import config.CustomVariables;

/**
 *
 * @author Purinda Gunasekara
 */
public class main {

	public static void main(String[] args) 
	{
//Debug current path
//		String currentDir = new File(".").getAbsolutePath();
//		System.out.println(currentDir);
		// Extention of nanoHttpd
		WebServer httpd;
		// Initialize HAL module (this is a singleton module)
		HardwareInterface hal = HardwareInterface.getInstance();
		
		try
		{
			// Init webserver
			httpd = new WebServer(CustomVariables.PORT);
			
			System.out.println("Press any key to terminate.");
			System.in.read();
			
			hal.disconnect();
			httpd.stop();
			
			Thread.sleep(1000);
			System.exit(0);
		}
		catch (InterruptedException e){}
		catch (IOException e)
		{
			// Display any exceptions
			System.out.printf(e.getMessage());
		}
		
	}
}
