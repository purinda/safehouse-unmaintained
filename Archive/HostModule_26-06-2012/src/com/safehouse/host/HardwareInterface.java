package com.safehouse.host;

import com.safehouse.host.HardwareInterface.Module.Ability;
import config.CustomVariables;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

// RxTx related imports
import gnu.io.*;
import java.io.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;


public class HardwareInterface 
{
	private static HardwareInterface _instance = null;
	private static Module[] connectedModules;

	// serial message buffer
	private static ArrayList<String> serialBuffer;
	
	// Serial Interface I/O streams
	private SerialPort serialPort;
	private InputStream serialIn;
	private OutputStream serialOut;
	
	// For cron
	Timer timer = new Timer();
	
	private HardwareInterface() 
	{
		System.setProperty("gnu.io.rxtx.SerialPorts", "/dev/ttyACM0");
		
		// Connect to serial interface
		try {
			connect(CustomVariables.CONFIG_SERIAL_PORT);			
			// set HC setup
			write("SERVERADDRESS;");
			write("serv1;");
		
			// init all modules
			getConnectedModules();

			// 30s cron
			timer.schedule(new Crontask(), 5000, 30000);			
		
		} catch (Exception ex) {
			Logger.getLogger(HardwareInterface.class.getName()).log(Level.SEVERE, null, ex);
		}			
	}
	
	public static synchronized HardwareInterface getInstance()
	{
		if (_instance == null) 
		{
			_instance = new HardwareInterface();
		}
		
		return _instance;
	}
	
	
    private class Crontask extends TimerTask {
        public void run() {
			getStats();
        }
    }
	
	private void getStats()
	{
		try {
			HardwareInterface.getInstance().write("CLIENTADDRESS;clie1;");
			Iterator abilities = HardwareInterface.getInstance().getModuleById("CLIE1").abilities.iterator();
			while (abilities.hasNext())
			{
				Ability ability_ = (Ability) abilities.next();
				ability_.request();
				Thread.sleep(1000);
			}
			
			Thread.sleep(2000);
			
			HardwareInterface.getInstance().write("CLIENTADDRESS;clie5;");
			abilities = HardwareInterface.getInstance().getModuleById("CLIE5").abilities.iterator();
			while (abilities.hasNext())
			{
				Ability ability_ = (Ability) abilities.next();
				ability_.request();
				Thread.sleep(1000);
			}
		} catch (InterruptedException ex) {
			Logger.getLogger(HardwareInterface.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	// Referece
	// http://rxtx.qbang.org/wiki/index.php/Two_way_communcation_with_the_serial_port
	void connect ( String portName ) throws Exception
	{
		CommPortIdentifier portIdentifier = CommPortIdentifier.getPortIdentifier(portName);
		if ( portIdentifier.isCurrentlyOwned() )
		{
			System.out.println("Error: Port is currently in use");
		}
		else
		{
			CommPort commPort = portIdentifier.open(this.getClass().getName(),2000);

			if ( commPort instanceof SerialPort )
			{
				serialPort = (SerialPort) commPort;
				serialPort.setSerialPortParams(57600,SerialPort.DATABITS_8,SerialPort.STOPBITS_1,SerialPort.PARITY_NONE);

				serialIn = serialPort.getInputStream();
				serialOut = serialPort.getOutputStream();
				
				(new Thread(new SerialReader(serialIn))).start();
			}
			else
			{
				System.out.println("Error: Only serial ports are handled by this example.");
			}
		}
	}
	
	// Write to serial port
	void write(String in_)
	{
		try {
			serialOut.write( in_.getBytes() );
		} catch (IOException ex) {
			Logger.getLogger(HardwareInterface.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

    public static class SerialReader implements Runnable 
	{
        InputStream in;
        BufferedReader reader;
        
        public SerialReader ( InputStream in ) 
		{
            this.in = in;
            this.reader = new BufferedReader(new InputStreamReader(in));
        }
        
        public void run () 
		{
			String line = null;
			try{
				while ((line = reader.readLine()) != null) 
				{
					HardwareInterface.getInstance().processMessage(line);
				}
			}
			catch ( IOException e ) {
				e.printStackTrace();
			}
        }
	
    }
	
	
	/*
	 * Processes messages returned from serial interface
	 */
	public void processMessage(String msg_)
	{	
		String [] results = msg_.split("\\,|;");
		String clientAddress = null;
		int cmd = 0;
		double val = 0;
		
//		for (int i=0; i<results.length;i++)
//			System.out.println(results[i]);
//		
		if (results[0].equalsIgnoreCase("CSP"))
		{
			System.out.println("PROCESSING MESSAGES = " + msg_);
			
			try {
					clientAddress = results[1];
					cmd = Integer.parseInt(results[2]);
					val = Double.parseDouble(results[3]);

					for (int x=0; x < connectedModules[getModuleIndexById(clientAddress)].abilities.size(); x++)
					{
						if (connectedModules[getModuleIndexById(clientAddress)].abilities.get(x).type.equalsIgnoreCase("SWITCH") && cmd == 1)
								connectedModules[getModuleIndexById(clientAddress)].abilities.get(x).value = val;

						if (connectedModules[getModuleIndexById(clientAddress)].abilities.get(x).type.equalsIgnoreCase("TEMPERATURE") && cmd == 10)
								connectedModules[getModuleIndexById(clientAddress)].abilities.get(x).value = val;

						if (connectedModules[getModuleIndexById(clientAddress)].abilities.get(x).type.equalsIgnoreCase("HUMIDITY") && cmd == 11)
								connectedModules[getModuleIndexById(clientAddress)].abilities.get(x).value = val;
					}
			}
			catch(Exception e)
			{
					// ignore all exceptions

			}
							
		}
	}
	
	// Serial port disconnect
	void disconnect()
	{
		this.serialPort.close();
		System.out.println("Serial port closed.");
	}
	
	
	public Module getModuleById(String id)
	{	
		for (int i = 0; i < connectedModules.length; i++) 
		{
			if (connectedModules[i].id.equalsIgnoreCase(id))
				return connectedModules[i];
		}
		
		return null;
	}
	
	public int getModuleIndexById(String id)
	{	
		for (int i = 0; i < connectedModules.length; i++) 
		{
			if (connectedModules[i].id.equalsIgnoreCase(id))
				return i;
		}
		
		return -1;
	}

	public Module[] getConnectedModules()
	{	
		if (connectedModules != null)
			return connectedModules;
		
		connectedModules = new Module[3];
		// Hard coded set of modules used for testing. In real world
		// this has to be retrived from the host device.
		
		for (int i = 0; i < 3; i++) {
			connectedModules[i] = new Module();
		}
		
		// Example Module 1
		connectedModules[0].setModuleInfo("clie1", "Client 1", "Currently only this device is linked to H/W interface");
		connectedModules[0].setAbility("SWITCH", "Switch", "Switch on/off your device");
		connectedModules[0].setAbility("TEMPERATURE", "Temperature", "Current temperature reading inside room");
		connectedModules[0].setAbility("HUMIDITY", "Humidity", "How wet is your surrounding ;)");
		connectedModules[0].setType("APPCTRL+DHT");
		
		// Example Module 2
		connectedModules[1].setModuleInfo("clie2", "Client 2", "Self explanatory! ;)");
		connectedModules[1].setAbility("SWITCH", "Switch", "Switch on/off your device");
		connectedModules[1].setType("BULB");
		// Example Module 3
		connectedModules[2].setModuleInfo("clie5", "Weather Station", "Check current temperature and humidity");
		connectedModules[2].setAbility("TEMPERATURE", "Temperature", "Outside temperature");
		connectedModules[2].setAbility("HUMIDITY", "Humidity", "Outside humidity leve");
		connectedModules[2].setType("WEATHER");
		
		return connectedModules;
	}
	
	/*
	 * Class Module implementation
	 *  - this is used to breakdown the code to appliance controller (module) level
	 *  - returns module information other than more generic details like ID, Title
	 *   - Current status
	 *   - Abilities 
	 *   - History
	 */
	public static class Module
	{
		public String id = null;
		public String name = null;
		public String description = null;

		public char type = 'a';
		
		public List<Ability> abilities = new ArrayList<Ability>();
		
//		Following can be implemented later
//		private HashMap notifications;
//		private HashMap history;				
		
		public Module(String name)
		{
			this.name = name;
		}
		
		public Module()
		{
			
		}
		
		public void setModuleInfo(String id, String name, String des)
		{
			this.id= id;
			this.name = name;
			this.description = des;
		}
		
        public String getId() 
		{
            return id;
        }

        public void setId(String _in) 
		{
            this.id = _in;
        }		
		
		private void write(String in_)
		{
			HardwareInterface.getInstance().write(in_);
		}
		
        public String getName() 
		{
            return name;
        }

        public void setName(String _in) 
		{
            this.name = _in;
        }		
		
        public String getDescription() 
		{
            return name;
        }

        public void setDescription(String _in) 
		{
            this.description = _in;
        }		
		
        public char getType() 
		{
            return type;
        }
	
        public void setType(String _in) 
		{			
			// Each module in the home page 
			// displayed assigned color code 
			// (JQUERY Mobile theme default codes)
			char __type = 'a'; // Black
			
			if (_in.equalsIgnoreCase("BULB"))
				__type = 'e';
			if (_in.equalsIgnoreCase("APPCTRL+DHT"))
				__type = 'b';
			if (_in.equalsIgnoreCase("WEATHER"))
				__type = 'a';

            this.type = __type;
        }
			
		public void setAbility(String type, String name, String des)
		{
			// init new ability and set parameters
			Ability newAbility = new Ability();
			newAbility.setType(type);
			newAbility.setName(name);
			newAbility.setDescription(des);
			// Set id for messaging requirements
			newAbility.moduleId = this.id;
			
			this.abilities.add(newAbility);
		}
		
		public static class Ability
		{
			/*
			* Ability type has to be defined in a config file as itself must explain
			* what it suppose to do and what the UI should look like for that ability.
			* Ex:	Type		Name	Control
			*		1			Dimmer	Variable
			*		2			Switch	Binary
			*		3			.....	.....
			*/
			public String moduleId = null;
			public String type = null;
			public String name = null;
			public String description = null;
			public String html = null;
			public double value = 0.0;
					
			public Ability()
			{

			}

			public void setType(String in) 
			{ 
				this.type = in; 				
			}
			
			public String getType() { return this.type; }
			
			public void setName(String in) { this.name = in; }
			public String getName() { return this.name; }
			
			public void setHtml(String in) { this.html = in; }
			public String getHtml() 
			{
				DecimalFormat twoDForm = new DecimalFormat("#.##");

				// What JQuery mobile control to retrieve
				if (this.type.equalsIgnoreCase("SWITCH"))
				{
					if (value > 0)
						this.html = CustomVariables.CONTROL_FLIP_SWITCH_on; 
					else
						this.html = CustomVariables.CONTROL_FLIP_SWITCH_off; 
				}
				
				else if (this.type.equalsIgnoreCase("TEMPERATURE"))
					this.html = twoDForm.format(this.value) + " C";
				else if (this.type.equalsIgnoreCase("HUMIDITY"))
					this.html = twoDForm.format(this.value) + " rh";	
				
				return this.html; 
			}

			public void control(int outputValue)
			{

				if (type.equalsIgnoreCase("SWITCH"))
				{
					HardwareInterface.getInstance().write("CLIENTADDRESS;" + this.moduleId.toLowerCase() + ";");
					HardwareInterface.getInstance().write("REQUEST;");
					// on
					if (outputValue > 0)
						HardwareInterface.getInstance().write("1,1;");
					// off
					if (outputValue <= 0)
						HardwareInterface.getInstance().write("1,0;");
				}
				
				return;
							
			}
			
			public void request()
			{
				
				if (type.equalsIgnoreCase("TEMPERATURE"))
				{
					HardwareInterface.getInstance().write("REQUEST;");
					// read 
					HardwareInterface.getInstance().write("10,0;");
					
					return;
				}
				
				
				if (type.equalsIgnoreCase("HUMIDITY"))
				{
					HardwareInterface.getInstance().write("REQUEST;");
					// read 
					HardwareInterface.getInstance().write("11,0;");
					
					return;
				}
							
			}
			
			public void setDescription(String in) { this.description = in; }
			public String getDescription() { return this.description; }
		}

	}
}

