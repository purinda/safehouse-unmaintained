/*
 * 
 * TODO: Extended WebServer
 * - Testing HG setup on zeus
 */
package com.safehouse.host;

import java.io.*;
import java.util.*;
import java.util.Iterator;
import java.util.Properties;
import freemarker.template.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import freemarker.template.*;
import config.CustomVariables;
import com.safehouse.host.pages.*;

public class WebServer extends NanoHTTPD{

	// Globals
	private Configuration cfg;
	
	public WebServer(int _port) throws IOException
	{
		super(_port, new File(CustomVariables.HTDOCS).getAbsoluteFile());

		/* This is used by the FreeMarker Library							   */    
        /* You should do this ONLY ONCE in the whole application life-cycle:   */    
    
        /* Create and adjust the configuration */
        cfg = new freemarker.template.Configuration();
        cfg.setDirectoryForTemplateLoading(
                new File(CustomVariables.HTDOCS));
        cfg.setObjectWrapper(new DefaultObjectWrapper());

        /* ------------------------------------------------------------------- */   		
	}
	
	public Response serve( String uri, String method, Properties header, Properties parms, Properties files )
	{
		Response response = null;
		// Parse HTML files using Freemarker 
		if (uri.contains(CustomVariables.TEMPLATE_TYPE))
		{
			// Choose what page to server
			String documentName = CommonHelper.removeExtention( uri ).toUpperCase();
			String htmlContent = null;
			if (documentName.equalsIgnoreCase("INDEX"))
			{
				Index pIndex = new Index(cfg, parms, method);
				htmlContent = pIndex.getHtmlContent();
				// Server the parsed page
				response = new NanoHTTPD.Response( HTTP_OK, MIME_HTML, htmlContent);					
			}
			else if (documentName.equalsIgnoreCase("VIEWMODULE"))
			{
				ViewModule pViewModule = new ViewModule(cfg, parms, method);
				htmlContent = pViewModule.getHtmlContent();					
				// Serve the parsed page
				response = new NanoHTTPD.Response( HTTP_OK, MIME_HTML, htmlContent);					
			}

		}

		// Ignore all other files(css, js, images, etc) and use default content serve
		if (response == null)
			response = super.serve( uri, method, header, parms, files );
		
		return response;
	}
}



// Some example code blocks
//
// Interrogate all POST values 
//if (method.equalsIgnoreCase("POST"))
//{
//	// User requests to control appliances
//	if (parms.getProperty("SUBMIT".toLowerCase()).equalsIgnoreCase("OK"))
//	{
//		String data = CommonHelper.convertStreamToString( super.serve( uri, method, header, parms, files ).data );
//		return new NanoHTTPD.Response( HTTP_OK, MIME_HTML, data);
//	}
//}

//Iterator eleKeys = parms.keySet().iterator();
//Iterator eleValues = parms.values().iterator();
//
//while (eleKeys.hasNext()) {
//	// Get post variables
//	Object eKey = eleKeys.next();
//	Object eVal = eleValues.next();
//	*//*
//	contact all interrogated variables from form
//	dump += "<br />" +eKey.toString().toUpperCase();
//	dump += " = " + eVal.toString().toUpperCase();
//	*//*
//}
