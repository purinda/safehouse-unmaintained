package com.safehouse.host;

import java.io.File;

/**
 * User: purinda
 * Date: 26/02/12
 * Time: 9:07 PM
 * Utility class.
 */
final public class CommonHelper {

	// Suppress default constructor for noninstantiability
	private CommonHelper() {
		throw new AssertionError();
	}
	

	public static String GetClassWithoutPackage(Class className)
	{
		String clsname= className.getName();
		int mid=clsname.lastIndexOf ('.') + 1;
		String finalClsName = clsname.substring(mid);
		return (finalClsName);
	}	

	public static String convertStreamToString(java.io.InputStream is) {
		try {
			return new java.util.Scanner(is).useDelimiter("\\A").next();
		} catch (java.util.NoSuchElementException e) {
			return "";
		}
	}

	public static String removeExtention(String filePath) {
		String fileName = new File(filePath).getName();
		return fileName.substring(0, fileName.lastIndexOf('.'));
	}
	

   public static Object createObject(String className) {
      Object object = null;
      try {
          Class classDefinition = Class.forName(className);
          object = classDefinition.newInstance();
      } catch (InstantiationException e) {
          System.out.println(e);
      } catch (IllegalAccessException e) {
          System.out.println(e);
      } catch (ClassNotFoundException e) {
          System.out.println(e);
      }
      return object;
   }
   

	public class Pair<L,R> {

	private final L left;
	private final R right;

	public Pair(L left, R right) {
		this.left = left;
		this.right = right;
	}

	public L getLeft() { return left; }
	public R getRight() { return right; }

	@Override
	public int hashCode() { return left.hashCode() ^ right.hashCode(); }

	@Override
	public boolean equals(Object o) {
		if (o == null) return false;
		if (!(o instanceof Pair)) return false;
		Pair pairo = (Pair) o;
		return this.left.equals(pairo.getLeft()) &&
			this.right.equals(pairo.getRight());
	}

	}
}

