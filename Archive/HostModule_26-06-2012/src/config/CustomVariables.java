/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package config;

/**
 *
 * @author Purinda Gunasekara
 */
public class CustomVariables {
	// Constants
	public static final String HTDOCS = "/opt/app/safehouse/html/";
	public static final int PORT = 80;
	public static final String TEMPLATE_TYPE = ".html";
	public static final String CONFIG_SERIAL_PORT = "/dev/ttyACM0";
	
	// Two different default values for Flip Switch (on/off)
	public static final String CONTROL_FLIP_SWITCH_off = "<select name=\"slider\" id=\"ability-control\" data-role=\"slider\"><option selected=\"selected\" value=\"off\">Off</option><option value=\"on\">On</option></select>";
	public static final String CONTROL_FLIP_SWITCH_on = "<select name=\"slider\" id=\"ability-control\" data-role=\"slider\"><option value=\"off\">Off</option><option selected=\"selected\" value=\"on\">On</option></select>";
	
}
