/*
 * File:   Scheduler.hpp
 * Author: purinda
 *
 * Created on 10 April 2012, 11:40 PM
 *
 * Notes:
 *
 *
 */

#include <../Common/Scheduler.hpp>
#include <../Common/Nrf24.hpp>
#include "StatusIndicator.hpp"
#include "UartInterface.hpp"
#include "config.hpp"

// Initialize Task Scheduler
Scheduler scheduler;

// Sub tasks
UartInterface uartCommunication(1);
StatusIndicator blinker(CONFIG_STATUS_LED, StatusIndicator::STATUS_FREQ);

void setup()
{
	// Setup wireless driver
	uint64_t* _host_address = (uint64_t*)CONFIG_HOST_ADDR;
	Nrf24::getInstance().initialize(CONFIG_NRF_CE, CONFIG_NRF_CSN);
	Nrf24::getInstance().setFromAddress(*_host_address);
//	Nrf24::getInstance().setToAddress(CONFIG_CLIENT_ADDR);
	
	// init other modules
	scheduler.initialize();
	uartCommunication.initialize();
	blinker.initialize();

	scheduler.queue(&uartCommunication);
	scheduler.queue(&blinker);
}


void loop()
{
	scheduler.processMessages();
	
}
