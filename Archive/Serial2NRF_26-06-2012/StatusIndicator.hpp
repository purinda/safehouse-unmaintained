/*
 * File:   StatusIndicator.hpp
 * Author: purinda
 *
 * Created on 11 April 2012, 12:37 AM
 */

#ifndef STATUSINDICATOR_HPP
#define	STATUSINDICATOR_HPP

#include <../Common/Scheduler.hpp>
#include "config.hpp"

class StatusIndicator :
	public ITask
{
	public:
		StatusIndicator(int, int);

		void initialize();
		void run(Scheduler*);

		static const short STATUS_FREQ = 500;

	private:
		int pin_;
		int freq_;
		boolean state_;
};


#endif	/* STATUSINDICATOR_HPP */

