/*
 * File:   UartInterface.h
 * Author: purinda
 *
 * Created on 11 April 2012, 1:13 AM
 */

#ifndef UARTINTERFACE_HPP
#define	UARTINTERFACE_HPP

#define CHAR_TERMINAL		';'
#define CHAR_SEPARATOR		','

#define CMD_HOSTADDRESS		"HOSTADDRESS"
#define CMD_CLIENTADDRESS	"CLIENTADDRESS"
#define CMD_REQUEST			"REQUEST"
#define CMD_SEND			"SEND"
#define CMD_DIG				"DIG"
#define CMD_MEMFREE			"MEMFREE"

#define STAT_OK				"OK"
#define STAT_WAITING		"WAITING"
#define STAT_FAIL			"FAIL"

#include <../Common/Scheduler.hpp>
#include "config.hpp"

enum
{
	OK = 0,
	FAILED = 1,
	WAITING = -1
};

class UartInterface :
	public ITask
{
	public:
		UartInterface(int);

		void initialize();
		void run(Scheduler*);
		
		void getMessages();

	private:
		void processMessages(char terminal);

		short period_;
		// Used to implement simple Protocol for communicating with
		// host module (computer, beagle[board|bone], etc)
		String payload_;
		String command_;
		int8_t _status; // +/-
	
};


#endif	/* UARTINTERFACE_H */

