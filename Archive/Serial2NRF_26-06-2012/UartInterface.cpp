/*
 * File:   UartInterface.cpp
 * Author: purinda
 *
 * Created on 11 April 2012, 1:13 AM
 *
 */

#include "UartInterface.hpp"
#include <../Common/Nrf24.hpp>
#include <../Common/Helper.hpp>
#include <../Common/Messaging.hpp>
#include "../Common/Debugger.h"


UartInterface::UartInterface(int period)
{
	period_ = period;
}

void UartInterface::initialize()
{
	Serial.begin(CONFIG_UART_BAUD);	

	// DEBUG CODE (this will enable printfs to output throught Serial)
	printf_begin();
}

void UartInterface::run(Scheduler* scheduler)
{
	scheduler->schedule(this, period_);

	if (Serial.available() > 0)
	{
		char c = Serial.read();

		// Echo out
//		Serial.print(c);

		if (c != CHAR_TERMINAL)
		{
			payload_ += c;
		}

		this->processMessages(c);
	}
	
	// Check incoming messages
	this->getMessages();
	
	// DEBUG CODE	
//	if ( Nrf24::getInstance().radio->available() )
//	{
//		// Dump the payloads until we've gotten everything
//		unsigned long long got_time;
//		bool done = false;
//		while (!done)
//		{
//			// Fetch the payload, and see if this was the last one.
//			done = Nrf24::getInstance().radio->read( &got_time, sizeof(unsigned long long) );
//			// Spew it
//			printf("Got payload %lu... size of payload", got_time);
//
//			// Delay just a little bit to let the other unit
//			// make the transition to receiver
//			delay(20);
//		}
//
//		// First, stop listening so we can talk
//		Nrf24::getInstance().radio->stopListening();
//
//		// Send the final one back.
//		Nrf24::getInstance().radio->write( &got_time, sizeof(unsigned long) );
//		printf("Sent response.\n\r");
//
//		// Now, resume listening so we catch the next packets.
//		Nrf24::getInstance().radio->startListening();
//	}		
}

void UartInterface::processMessages(char terminal)
{
	// End of string
	if (terminal == CHAR_TERMINAL)
	{
		// Clean up before everything else ;)
		payload_.trim();
		Serial.println();
		// Useful for debugging
		// Serial.println("#"+payload_+"#");

		// Set OWN address of NRF (setRAddr)
		if (payload_.equalsIgnoreCase(CMD_HOSTADDRESS))
		{
			command_ = CMD_HOSTADDRESS;
			_status = WAITING;
		}
		// Set client/receiver address to NRF (setTAddr)
		else if (payload_.equalsIgnoreCase(CMD_CLIENTADDRESS))
		{
			command_ = CMD_CLIENTADDRESS;
			_status = WAITING;
		}
		// Sends data into NRF24L01+ chip
		else if (payload_.equalsIgnoreCase(CMD_SEND))
		{
			command_ = CMD_SEND;
			_status = WAITING;
		}
		// request command used to send control signals
		else if (payload_.equalsIgnoreCase(CMD_REQUEST))
		{
			command_ = CMD_REQUEST;
			_status = WAITING;
		}
		// Displays how much RAM is free
		else if (payload_.equalsIgnoreCase(CMD_MEMFREE))
		{
			// No parameters required so set it to NULL
			command_ = NULL;
			Serial.print(CMD_MEMFREE);
			Serial.print(CHAR_SEPARATOR);
			Helper::freeMem(NULL);
			Serial.print(CHAR_TERMINAL);
			_status = OK;
		}
		// dig command to request client information
		else if (payload_.equalsIgnoreCase(CMD_DIG))
		{
			command_ = NULL;
			Messaging::ddp _emptyDdp;
//			_emptyDdp.address = _emptyDdp.address;
			_emptyDdp.devType = DT_RESERVED;
			_emptyDdp.pt = PT_RESERVED;


			// Send dig command with receiver address
			Messaging::dig(Nrf24::getInstance().receiver_address_);
			_status = OK;

		}
		// What we received can be a message?
		else
		{
			if (command_ == NULL)
			{
				_status = FAILED;
			}
			else
			{
				// If the just received string is not a command
				// then use it as parameter for previously received command.
				uint8_t msgBuffer[payload_.length()];
				payload_.toCharArray((char*)msgBuffer, payload_.length()+1);

				if (command_.equalsIgnoreCase(CMD_HOSTADDRESS))
				{
					uint64_t* _host_address = (uint64_t*)msgBuffer;
					Nrf24::getInstance().setFromAddress(*_host_address);
				}
				if (command_.equalsIgnoreCase(CMD_CLIENTADDRESS))
				{
					uint64_t* _client_address = (uint64_t*)msgBuffer;
					Nrf24::getInstance().setToAddress(*_client_address);

				}
				if (command_.equalsIgnoreCase(CMD_SEND))
				{
					Nrf24::getInstance().send(msgBuffer);
				}
				if (command_.equalsIgnoreCase(CMD_REQUEST))
				{
					// Process input string
					uint8_t sep_pos = payload_.indexOf(',');
					uint8_t ct = payload_.substring(0, sep_pos).toInt();
					uint16_t instruction = payload_.substring(sep_pos+1, payload_.length()).toInt();

					// Send command
					Messaging::request(Nrf24::getInstance().receiver_address_, ct, instruction);
				}

				_status = OK;
				// Clear the COMMAND variable
				command_ = NULL;
			}
		}

		//		delay(50);
		// ACK
		switch(_status)
		{
			case OK: Serial.print(STAT_OK); break;
			case FAILED: Serial.print(STAT_FAIL); break;
			case WAITING: Serial.print(STAT_WAITING); break;
		}
		
		Serial.print(CHAR_TERMINAL);
		Serial.println();
		// Clean storage vars before next command/msg
		payload_ = NULL;
	}
}

void UartInterface::getMessages()
{
	uint8_t _packet[Nrf24::PAYLOAD] = {0};
	// To store address
	uint8_t buf[8]={0};	
	
	// if there is data ready
	if ( Nrf24::getInstance().radio->available() )
	{
		// Dump the payloads until we've gotten everything
		bool done = false;
		while (!done)
		{
			// Fetch the payload, and see if this was the last one.
			done = Nrf24::getInstance().radio->read( &_packet, Nrf24::PAYLOAD);
			// Delay just a little bit to let the other unit
			// make the transition to receiver
			delay(20);
		}
		
		Messaging::dummy* _referer = (Messaging::dummy*)_packet;
		// After matching CONTROL TYPE Signal a reply will be sent
		switch(_referer->pt)
		{
			// Info returned for Command Packets
			case PT_COMMAND_STATUS_PACKET:
			{
				Messaging::csp* _csp = (Messaging::csp*)_packet;
				printf("\n\rCSP,");
				
				memcpy (&buf, &_csp->address, Nrf24::ADDRESSWIDTH); 				
				printf("%c", buf[0]);
				printf("%c", buf[1]);
				printf("%c", buf[2]);
				printf("%c", buf[3]);
				printf("%c", buf[4]);
				printf(",%u,%u", _csp->ct, _csp->returnVal);
				printf("%c\n\r", CHAR_TERMINAL);
			}
				break;
			// Device information 
			case PT_DEVICE_DATA_PACKET:
			{
				Messaging::ddp* _ddp = (Messaging::ddp*)_packet;
				printf("\n\rDDP,");
			
				memcpy (&buf, &_ddp->address, Nrf24::ADDRESSWIDTH); 				
				printf("%c", buf[0]);
				printf("%c", buf[1]);
				printf("%c", buf[2]);
				printf("%c", buf[3]);
				printf("%c", buf[4]);
				printf(",%u", _ddp->devType);
				printf("%c\n\r", CHAR_TERMINAL);
			}
				break;
		}
	}
}