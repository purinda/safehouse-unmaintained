/*
 * File:   StatusIndicator.cpp
 * Author: purinda
 *
 * Created on 11 April 2012, 12:37 AM
 */

#include "StatusIndicator.hpp"


StatusIndicator::StatusIndicator(int pin, int period)
{
	pin_ = pin;
	// Power on when started
	state_ = true;
	freq_ = period;
}

void StatusIndicator::initialize()
{
	pinMode( pin_, OUTPUT);
}

void StatusIndicator::run(Scheduler* scheduler)
{
	scheduler->schedule(this, freq_);

	state_ = (state_ == true) ? false : true;
	digitalWrite(pin_, state_);
}
