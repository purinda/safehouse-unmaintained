/* 
 * File:   config.hpp
 * Author: purinda
 *
 * Created on 6 May 2012, 12:06 AM
 */

#ifndef CONFIG_HPP
#define	CONFIG_HPP

#include <inttypes.h>

// DEVICE TYPES
const uint8_t DT_RESERVED = 0;
const uint8_t DT_BULB = 1;
const uint8_t DT_BULB_DIMMABLE = 2;
const uint8_t DT_BULB_PIR = 3;
const uint8_t DT_APP_CTRL_DHT = 4;
const uint8_t DT_DHT = 5;

// PACKET TYPES
const uint8_t PT_RESERVED = 0;
const uint8_t PT_DEVICE_CHECKING_PACKET = 1;
const uint8_t PT_DEVICE_DATA_PACKET = 2;
const uint8_t PT_COMMAND_PACKET = 3;
const uint8_t PT_COMMAND_STATUS_PACKET = 4;

// COMMAND TYPES
const uint8_t CT_RESERVED = 0;
const uint8_t CT_SWITCH = 1;
const uint8_t CT_VARIABLE_SWITCH = 2;
const uint8_t CT_TEMPERATURE = 10;
const uint8_t CT_HUMIDITY = 11;
const uint8_t CONFIG_CMTYPE = 4;

// Default Host Address
#define CONFIG_HOST_ADDR "serv1";

/**********************************************
 ************ Configure I/O Pins **************
 **********************************************/

// NRF24L01 SPI Driver Configuration
static const uint8_t CONFIG_NRF_CE = 4;
static const uint8_t CONFIG_NRF_CSN = 3;

// Host module specific I/O
const uint8_t CONFIG_STATUS_LED  = 8;
const uint16_t CONFIG_UART_BAUD  = 57600;

#endif	/* CONFIG_HPP */

