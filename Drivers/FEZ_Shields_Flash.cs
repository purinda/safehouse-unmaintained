/*
Copyright 2010 GHI Electronics LLC
Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License. 
*/

using System;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using GHIElectronics.NETMF.Hardware;

namespace GHIElectronics.NETMF.FEZ
{
    public static partial class FEZ_Shields
    {
        public static class FlashShield
        {
            const byte Status_Register_Read = 0xD7;
            const byte Continuous_Array_Read = 0xE8;
            const byte Buffer_1_Read = 0xD1;
            const byte Buffer_1_Write = 0x84;
            const byte DB_BUF1_PAGE_ERASE_PGM = 0x83;
            const byte AT45DB161D_density = 0x2C;
            static private uint pageSize = 0;
            static private SPI Spi = null;
			
            static public void Initialize()
            {

                SPI.Configuration SPICfig = new SPI.Configuration((Cpu.Pin)FEZ_Pin.Digital.Di10, false, 0, 0, false, true, 500,SPI.SPI_module.SPI1);
                Spi = new SPI(SPICfig);
                byte status = ReadStatusRegister();
                if ((status & 0x3C) != AT45DB161D_density)
                    throw new Exception("Not Supported Device or DataFlash");
                if ((status & 0x01) == 0x01)
                {
                    pageSize = 512;
                }
                else
                {
                    pageSize = 528;
                }
            }
			
            static public uint PageSize
            {
                get
                {
                    return pageSize;
                }
            }

            static private void SendCommandAndData(byte[] command, byte[] data)
            {
                if (command != null && data != null)
                {
                    byte[] temp = new byte[command.Length + data.Length];
                    for (int i = 0; i < command.Length; i++)
                    {
                        temp[i] = command[i];
                    }
                    for (int i = 0; i < data.Length; i++)
                    {
                        temp[i + command.Length] = data[i];
                    }
                    Spi.WriteRead(temp, temp);
                    for (int i = 0; i < command.Length; i++)
                    {
                        command[i] = temp[i];
                    }
                    for (int i = 0; i < data.Length; i++)
                    {
                        data[i] = temp[i + command.Length];
                    }


                }
                else
                {
                    if (command != null)
                        Spi.WriteRead(command, command);
                    if (data != null)
                        Spi.WriteRead(data, data);
                }
            }

            static private byte ReadStatusRegister()
            {
                byte[] cmd = new byte[2] { Status_Register_Read, 0 };
                SendCommandAndData(cmd, null);
                return cmd[1];
            }
			
            static public void ReadPage(uint PageNumber, byte[] buffer)
            {
                if (buffer.Length < PageSize)
                    throw new Exception("Buffer Size is smaller than Page Size");

                uint address = PageNumber << 10;
                byte[] cmd = new byte[5];
                cmd[0] = Buffer_1_Read;// Continuous_Array_Read;
                cmd[1] = (byte)(PageNumber >> 24);
                cmd[2] = (byte)(PageNumber >> 16);
                cmd[3] = (byte)(PageNumber >> 8);
                cmd[4] = (byte)PageNumber;
                SendCommandAndData(cmd, buffer);
                for (int timeout = 0; timeout < 100; timeout++)
                    if ((ReadStatusRegister() & 0x80) > 0)
                        return;
                throw new Exception("Buffer Size is smaller than Page Size");

            }

            static public void WritePage(uint PageNumber, byte[] buffer)
            {
                if (buffer.Length < PageSize)
                    throw new Exception("Buffer Size is smaller than Page Size");

                uint address = PageNumber << 10;
                byte[] cmd = new byte[5];
                cmd[0] = Buffer_1_Write;
                cmd[1] = (byte)(PageNumber >> 24);
                cmd[2] = (byte)(PageNumber >> 16);
                cmd[3] = (byte)(PageNumber >> 8);
                cmd[4] = (byte)PageNumber;
                SendCommandAndData(cmd, buffer);
                for (int timeout = 0; timeout < 100; timeout++)
                    if ((ReadStatusRegister() & 0x80) > 0)
                        return;
                throw new Exception("Error sending data");
                               
            }
        }
    }
}