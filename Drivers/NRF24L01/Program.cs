///////////////////////////////////////////////////////////////////////////////
//
//  This is a test program for the nRF24L01 class.
//
//  When you start the program it ask you to press the left or right key
//  selecting the role: sender or reciever.
//
//  Sender:
//  - Keeps sending a counter value. On succesfull transmition it increases
//  the counter (up to 100, then it resets).
//  - On error it decreases the counter.
//
//  Reciever
//  - Displays the last recieved data byte
//
//  Copyright:
//  (C)opyright 2008, Elze Kool 
//  http://www.microframework.nl
//  
//  You can use this file as you like, please include the above copyright 
//  information as I did with my sources.
//
//
//  Enjoy!
//
//////////////////////////////////////////////////////////////////////////////

using System;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;

using System.Threading;

using GHIElectronics;
using GHIElectronics.Hardware;
using GHIElectronics.System;

namespace NRF24L01_Sender
{
    /// <summary>
    /// Main entry point
    /// </summary>
    public class Program
    {
        public static void Main()
        {
            // Create and run program
            NRF24L01TestProgram Application = new NRF24L01TestProgram();
            Application.Run();
        }

    }



    /// <summary>
    /// NRF24L01 Test Application
    /// </summary>
    public class NRF24L01TestProgram
    {
        Font ScreenFont;
        Bitmap Screen;

        InputPort ButtonUp;
        InputPort ButtonDown;
        InputPort ButtonLeft;
        InputPort ButtonRight;
        InputPort ButtonSelect;
        InputPort ButtonMenu;
        InputPort ButtonBack;

        /// <summary>
        /// Program Constructor. Initialize Program
        /// </summary>
        public NRF24L01TestProgram()
        {
            // Start GHI System Manager
            SystemManager.Start(null);

            // Direction buttons
            ButtonUp = new InputPort(EmbeddedMaster.Pins.E4x, true, Port.ResistorMode.PullUp);
            ButtonDown = new InputPort(EmbeddedMaster.Pins.E0x, true, Port.ResistorMode.PullUp);
            ButtonLeft = new InputPort(EmbeddedMaster.Pins.E23x, true, Port.ResistorMode.PullUp);
            ButtonRight = new InputPort(EmbeddedMaster.Pins.E1x, true, Port.ResistorMode.PullUp);

            // Select, Menu and Back buttons
            ButtonSelect = new InputPort(EmbeddedMaster.Pins.E30x, true, Port.ResistorMode.PullUp);
            ButtonMenu = new InputPort(EmbeddedMaster.Pins.E18x, true, Port.ResistorMode.PullUp);
            ButtonBack = new InputPort(EmbeddedMaster.Pins.E21x, true, Port.ResistorMode.PullUp);

            // Select font based on screen type (TFT/Non-TFT)
            if (Microsoft.SPOT.Presentation.SystemMetrics.ScreenWidth > 200)
            {
                // TFT
                ScreenFont = Resources.GetFont(Resources.FontResources.Verdana48b);
            }
            else
            {
                // Non TFT
                ScreenFont = Resources.GetFont(Resources.FontResources.Verdana12);
            }

            // Init screen bitmap
            Screen = new Bitmap(Microsoft.SPOT.Presentation.SystemMetrics.ScreenWidth, Microsoft.SPOT.Presentation.SystemMetrics.ScreenHeight);
            Screen.Flush();

        }


        byte Counter = 1;

        /// <summary>
        /// Run program
        /// </summary>
        public void Run()
        {
            // First key pressed determines the role
            bool Sender = false;

            Screen.Clear();
            Screen.DrawText("<- Recieve", ScreenFont, Microsoft.SPOT.Presentation.Media.Color.White, 2, 2);
            Screen.DrawText("-> Send", ScreenFont, Microsoft.SPOT.Presentation.Media.Color.White, 2, (int)(ScreenFont.Height * 1.2F));
            Screen.Flush();

            while (true)
            {
                // Left -> Reciever
                if (ButtonLeft.Read() == false)
                {
                    Sender = false;
                    break;
                }

                // Right -> Sender
                if (ButtonRight.Read() == false)
                {
                    Sender = true;
                    break;
                }

                Thread.Sleep(10);
            }


            // Start test programs
            if (Sender)
            {
                ////////////////////////////////////////////////////////
                //
                //     SENDER Test 
                //     Send to: { 0xAA, 0xBB, 0xCC, 0xDD, 0xEE }
                //
                ////////////////////////////////////////////////////////

                Screen.Clear();
                Screen.DrawText("Sender", ScreenFont, Microsoft.SPOT.Presentation.Media.Color.White, 2, 2);
                Screen.Flush();

                // Initialize ports for nRF24L01
                NRF24L01.Initialize();

                // Set adresses
                NRF24L01.RX_Adress = new byte[] { 0xEE, 0xDD, 0xCC, 0xBB, 0xAA };
                NRF24L01.TX_Adress = new byte[] { 0xAA, 0xBB, 0xCC, 0xDD, 0xEE };

                // Initialize nRF24L01
                NRF24L01.Configure();

                // Event handler for OnTransmit
                NRF24L01.OnTransmit += new NRF24L01.OnTransmitHandler(NRF24L01_OnTransmit);

                // Send first byte, next bytes are send with the OnTransmit handler
                NRF24L01.SendByte(Counter);

                // Keep updating screen
                while (true)
                {
                    Thread.Sleep(200);
                    Screen.Clear();
                    Screen.DrawText("Sender", ScreenFont, Microsoft.SPOT.Presentation.Media.Color.White, 2, 2);
                    Screen.DrawText(Counter.ToString(), ScreenFont, Microsoft.SPOT.Presentation.Media.Color.White, 2, (int)(ScreenFont.Height * 1.2F));
                    Screen.Flush();
                }
            }
            else
            {
                ////////////////////////////////////////////////////////
                //
                //     RECIEVER Test 
                //     Recieve adres: { 0xAA, 0xBB, 0xCC, 0xDD, 0xEE }
                //
                ////////////////////////////////////////////////////////

                Screen.Clear();
                Screen.DrawText("Reciever", ScreenFont, Microsoft.SPOT.Presentation.Media.Color.White, 2, 2);
                Screen.Flush();

                // Initialize ports for nRF24L01
                NRF24L01.Initialize();

                // Set adresses 
                NRF24L01.RX_Adress = new byte[] { 0xAA, 0xBB, 0xCC, 0xDD, 0xEE };
                NRF24L01.TX_Adress = new byte[] { 0xEE, 0xDD, 0xCC, 0xBB, 0xAA };

                // Initialize nRF24L01
                NRF24L01.Configure();

                // Event handler for OnDataRecieved
                NRF24L01.OnDataRecieved += new NRF24L01.OnDataRecievedHandler(NRF24L01_OnDataRecieved);

                // Keep updating screen
                while (true)
                {
                    Thread.Sleep(200);
                    Screen.Clear();
                    Screen.DrawText("Reciever", ScreenFont, Microsoft.SPOT.Presentation.Media.Color.White, 2, 2);
                    Screen.DrawText(LastData, ScreenFont, Microsoft.SPOT.Presentation.Media.Color.White, 2, (int)(ScreenFont.Height * 1.2F));
                    Screen.Flush();
                }

            }

        }

        /// <summary>
        /// Data that's written to the screen in the recieve test
        /// </summary>
        String LastData = "";

        /// <summary>
        /// Event handler for OnDataRecieved
        /// </summary>
        /// <param name="data"></param>
        /// <param name="statuscode"></param>
        void NRF24L01_OnDataRecieved(byte data, byte statuscode)
        {
            // Debug the data
            Debug.Print("Recieved data: " + data.ToString());

            // Output to screen
            LastData = data.ToString();
        }


        /// <summary>
        /// Event handler for OnTransmit
        /// </summary>
        /// <param name="succes"></param>
        /// <param name="status"></param>
        void NRF24L01_OnTransmit(bool succes, byte status)
        {
            // See if transmit was succesfull
            if (succes)
            {
                // If succes increase counter and send next byte
                if (Counter == 100) { Counter = 1; } else { Counter++; }
                NRF24L01.SendByte(Counter);
                Debug.Print("Send data: " + Counter.ToString());
                Thread.Sleep(1000);
            }
            else
            {
                // If error decrease counter and try send next byte
                if (Counter == 1) { Counter = 1; } else { Counter--; }
                NRF24L01.SendByte(Counter);
                Debug.Print("Send data failed: " + NRF24L01.ByteToHex(status));
                Thread.Sleep(1000);
            }

        }
    }
}
